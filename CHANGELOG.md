# Release 32.0.127

* Change maven artifactId

# Release 32.0.126

* Publish to maven central
* Remove p2repo

# Release 32.0.125

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.9.0

# Release 32.0.124

* Update dependency org.apache.maven.plugins:maven-compiler-plugin to v3.9.0

# Release 32.0.123

* Update dependency org.reficio:p2-maven-plugin to v2

# Release 32.0.122

* Update dependency org.assertj:assertj-core to v3.22.0

# Release 32.0.121

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.10.0

# Release 32.0.120

* No changes

# Release 32.0.119

* Remove unused methods from RelatedExpressionsSpi

# Release 32.0.118

* Correct JavaDoc errors
* Move creation of related expressions to d.m.e.a
* Turn PreferencesSpi to Preferences POJO
* Update java-contract
* Reorder jobs in .gitlab-ci.yml
* Verify maven for renovate in GitLab CI
* Add TODO for unnecessary info
* Remove whitespace .gitlab-ci.yml [skip release]
* Remove DEPENDENT_PROJECTS from .gitlab-ci.yml

# Release 32.0.117

* Adapt to java-contract 15.0.1 API
* Update dependency de.monochromata.contract:java-contract to v15
* Fix contract verification
* Verify pact from broker in CI
* Update to java-contract 12.x
* Update dependency de.monochromata.contract:java-contract to v12

# Release 32.0.116

* Update junit5 monorepo to v5.8.1

# Release 32.0.115

* Update dependency org.assertj:assertj-core to v3.21.0

# Release 32.0.114

* Update junit5 monorepo to v5.8.0

# Release 32.0.113

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.1

# Release 32.0.112

* Update dependency org.mockito:mockito-core to v3.12.4

# Release 32.0.111

* Update dependency org.mockito:mockito-core to v3.12.1

# Release 32.0.110

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.1

# Release 32.0.109

* Update dependency de.monochromata.contract:java-contract to v6.2.1

# Release 32.0.108

* Update java-contract to 6.2.0

# Release 32.0.107

* Remove Mockito mock IDs from provider IDs

# Release 32.0.106

* Add contracts for all ref strategies in CIC_DA1Re_Hy_Simple

# Release 32.0.105

* Update contracts from run as Eclipse plug-in test

# Release 32.0.104

* Update CIC_DA1RE_Hy_SimpleTest pact

# Release 32.0.103

* Fix contract test scenario
* Re-enable contract test

# Release 32.0.102

* Update dependency de.monochromata.contract:java-contract to v4

# Release 32.0.101

* Disable contract tests (instead of ignoring)
* Ignore contract test temporarily
* Require at least Java 14
* Simplify heading in README.md

# Release 32.0.100

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7

# Release 32.0.99

* Update dependency org.mockito:mockito-core to v3.11.2

# Release 32.0.98

* Update dependency org.assertj:assertj-core to v3.20.2

# Release 32.0.97

* Update dependency org.assertj:assertj-core to v3.20.1

# Release 32.0.96

* Fix JavaDoc error
* Ease contract testing by making Referent subtypes public

# Release 32.0.95

* Update dependency org.mockito:mockito-core to v3.11.1

# Release 32.0.94

* Update dependency org.mockito:mockito-core to v3.11.0

# Release 32.0.93

* Update dependency org.reficio:p2-maven-plugin to v1.7.0

# Release 32.0.92

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.0

# Release 32.0.91

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.6.1

# Release 32.0.90

* Add protected no-args constructors for contract testing

# Release 32.0.89

* Change access to protected

# Release 32.0.88

* Add no-arg constructor for Hyponymy to aid contract testing

# Release 32.0.87

* chore(deps): update junit5 monorepo to v5.7.2

# Release 32.0.86

* chore(deps): update dependency de.monochromata.contract:java-contract to v2.1.14

# Release 32.0.85

* chore(deps): update dependency org.mockito:mockito-core to v3.10.0

# Release 32.0.84

* chore(deps): update dependency org.jacoco:jacoco-maven-plugin to v0.8.7

# Release 32.0.83

* chore(deps): update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.2

# Release 32.0.82

* chore(deps): update dependency nl.jqno.equalsverifier:equalsverifier to v3.6

# Release 32.0.81

* chore(deps): update dependency de.monochromata.contract:java-contract to v2.1.12

# Release 32.0.80

* chore(deps): update dependency de.monochromata.contract:java-contract to v2.1.10

# Release 32.0.79

* chore(deps): update dependency org.reficio:p2-maven-plugin to v1.6.0

# Release 32.0.78

* chore(deps): update dependency org.mockito:mockito-core to v3.9.0

# Release 32.0.77

* chore(deps): update dependency de.monochromata.contract:java-contract to v2.1.9

# Release 32.0.76

* chore: update pacts

# Release 32.0.75

* chore(deps): update dependency de.monochromata.contract:java-contract to v2.1.8

# Release 32.0.74

* Use JDK 14
* chore: switch to JUnit 5 #93

# Release 32.0.73

* chore: add initial contract for Hyponynmy

# Release 32.0.72

* Update dependency org.apache.commons:commons-lang3 to v3.12.0

# Release 32.0.71

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.5

# Release 32.0.70

* Update dependency org.mockito:mockito-core to v3.8.0

# Release 32.0.69

* Update dependency junit:junit to v4.13.2

# Release 32.0.68

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.4

# Release 32.0.67

* Update dependency org.assertj:assertj-core to v3.19.0

# Release 32.0.66

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.2

# Release 32.0.65

* Update dependency org.mockito:mockito-core to v3.7.7

# Release 32.0.64

* Update dependency org.mockito:mockito-core to v3.7.0

# Release 32.0.63

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.1

# Release 32.0.62

* Merge remote-tracking branch 'origin/master' into renovate/equalsverifier.version
* Remove redundant property declaration
* Remove redundant property declarations

# Release 32.0.61

* Remove unused third-party dependencies

# Release 32.0.60

* Correct path to p2repo
* Correct links to (p2/m2) repo in README.md
* Skip site deployment (will be copied)
* Release to GitLab pages instead of FTP
* Link GitLab pages in README.md
* Update dependency org.mockito:mockito-core to v3.6.28

# Release 32.0.59

* Update dependency org.assertj:assertj-core to v3.18.1

# Release 32.0.58

* Update dependency org.mockito:mockito-core to v3.6.0

# Release 32.0.57

* Update dependency org.assertj:assertj-core to v3.18.0

# Release 32.0.56

* Update dependency org.mockito:mockito-core to v3.5.15

# Release 32.0.55

* Update dependency junit:junit to v4.13.1

# Release 32.0.54

* Update dependency org.mockito:mockito-core to v3.5.13

# Release 32.0.53

* Update dependency org.mockito:mockito-core to v3.5.11

# Release 32.0.52

* Update dependency org.jacoco:jacoco-maven-plugin to v0.8.6

# Release 32.0.51

* Update dependency org.assertj:assertj-core to v3.17.2

# Release 32.0.50

* Update dependency org.mockito:mockito-core to v3.5.10

# Release 32.0.49

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.1

# Release 32.0.48

* Update dependency org.mockito:mockito-core to v3.5.9

# Release 32.0.47

* Update dependency org.assertj:assertj-core to v3.17.1

# Release 32.0.46

* Update dependency org.mockito:mockito-core to v3.5.7

# Release 32.0.45

* Update dependency org.assertj:assertj-core to v3.17.0

# Release 32.0.44

* Update dependency org.mockito:mockito-core to v3.5.5

# Release 32.0.43

* Update dependency org.mockito:mockito-core to v3.5.2

# Release 32.0.42

* Update dependency org.mockito:mockito-core to v3.5.0

# Release 32.0.41

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.8.1

# Release 32.0.40

* Update dependency org.mockito:mockito-core to v3.4.6

# Release 32.0.39

* Update dependency org.mockito:mockito-core to v3.4.4

# Release 32.0.38

* Update dependency org.apache.commons:commons-lang3 to v3.11

# Release 32.0.37

* Update dependency org.mockito:mockito-core to v3.4.0

# Release 32.0.36

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.1

# Release 32.0.35

* Update maven.surefire.version to v3.0.0-M5

# Release 32.0.34

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.0

# Release 32.0.33

* Update dependency org.assertj:assertj-core to v3.16.1

# Release 32.0.32

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 32.0.31

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0

# Release 32.0.30

* Update dependency org.apache.commons:commons-lang3 to v3.10

# Release 32.0.29

* Build with Java 14 with preview features

# Release 32.0.28

* Add CHANGELOG.md to deploy-maven job artifacts

