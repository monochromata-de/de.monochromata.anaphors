package de.monochromata;

public abstract class AbstractStrategyTest<S extends Strategy> extends AbstractSuppliedStrategyTest<S> {

    private final S strategy;

    public AbstractStrategyTest(final String expectedKind, final S strategy) {
        super(expectedKind);
        this.strategy = strategy;
    }

    @Override
    public S getStrategy() {
        return strategy;
    }
}
