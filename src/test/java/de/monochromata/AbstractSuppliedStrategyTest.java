package de.monochromata;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.api.SingleTypeEqualsVerifierApi;

public abstract class AbstractSuppliedStrategyTest<S extends Strategy> {

    private final String expectedKind;

    public AbstractSuppliedStrategyTest(final String expectedKind) {
        this.expectedKind = expectedKind;
    }

    public String getExpectedKind() {
        return expectedKind;
    }

    public abstract S getStrategy();

    @Test
    public void testKind() {
        assertThat(getStrategy().getKind()).isEqualTo(getExpectedKind());
    }

    @Test
    public void testEqualsContract() {
        createEqualsVerifier().verify();
    }

    protected SingleTypeEqualsVerifierApi<? extends Strategy> createEqualsVerifier() {
        return EqualsVerifier.forClass(getStrategy().getClass()).usingGetClass();
    }
}
