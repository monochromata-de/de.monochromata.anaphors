package de.monochromata.anaphors.cog.activation;

import static java.util.Collections.sort;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.cog.activation.Activatable;
import de.monochromata.anaphors.cog.activation.LIFOFormula;
import de.monochromata.anaphors.cog.activation.LIFOFormula.LIFOActivation;

public class LIFOFormulaTest {

	private final LIFOFormula formula = new LIFOFormula();

	@Test
	public void valuesAreSortedByReverseAddition() {
		final List<LIFOActivation> values = estimateActivationValues(4);

		assertThat(values).extracting("position").containsExactly(0l, 1l, 2l, 3l);

		sort(values);
		assertThat(values).extracting("position").containsExactly(3l, 2l, 1l, 0l);
	}

	protected List<LIFOActivation> estimateActivationValues(final int numberOfValues) {
		return IntStream.range(0, numberOfValues)
				.mapToObj(i -> formula.estimateActivation(mock(Activatable.class, "activatable-" + i), 0l))
				.collect(toList());
	}

}
