package de.monochromata.anaphors.cog.activation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Random;

import org.junit.jupiter.api.Test;

public class AllActiveFormulaTest {

    @Test
    public void anActivatableHasIdenticalActivationForRandomTimestamps() {

        final Activatable activatable = mock(Activatable.class);

        final AllActiveFormula allActive = new AllActiveFormula();
        final Random random = new Random();

        final long timestamp1 = random.nextLong();
        final EstimatedActivationValue activation1 = allActive.estimateActivation(activatable, timestamp1);

        final long timestamp2 = random.nextLong();
        final EstimatedActivationValue activation2 = allActive.estimateActivation(activatable, timestamp2);

        assertThat(activation1)
                .describedAs(
                        "Activations at random points in time " + timestamp1 + " and " + timestamp2 + " are equal")
                .isEqualTo(activation2);
    }

    @Test
    public void twoActivatablesHaveIdenticalActivationForRandomTimestamps() {

        final Activatable activatable1 = mock(Activatable.class, "activatable1");
        final Activatable activatable2 = mock(Activatable.class, "activatable2");

        final AllActiveFormula allActive = new AllActiveFormula();
        final Random random = new Random();

        final long timestamp1 = random.nextLong();
        final EstimatedActivationValue activation1 = allActive.estimateActivation(activatable1, timestamp1);

        final long timestamp2 = random.nextLong();
        final EstimatedActivationValue activation2 = allActive.estimateActivation(activatable2, timestamp2);

        assertThat(activation1)
                .describedAs("Activations for " + activatable1 + "@" + timestamp1 + " and " + activatable2 + "@"
                        + timestamp2 + " are equal")
                .isEqualTo(activation2);
    }

}
