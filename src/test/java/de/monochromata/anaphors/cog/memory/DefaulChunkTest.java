package de.monochromata.anaphors.cog.memory;

import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class DefaulChunkTest {

	@Test
	public void testEqualsContract() {
		EqualsVerifier.forClass(DefaultChunk.class).usingGetClass()
				.withIgnoredFields("activationFormula", "lastActivation", "lastActivationCalculatedForTimestamp")
				.verify();
	}

}
