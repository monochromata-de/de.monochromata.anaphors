package de.monochromata.anaphors.cog.memory;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.cog.activation.ActivationFormula;
import de.monochromata.anaphors.cog.activation.LIFOFormula;

public class SortedListMemoryTest implements SortedListMemoryTesting {

	private final LIFOFormula activationFormula = new LIFOFormula();
	private final SortedListMemory<Long> memory = new SortedListMemory<>(activationFormula);

	@Override
	public ActivationFormula activationFormula() {
		return activationFormula;
	}

	@Override
	public Memory<Long> memory() {
		return memory;
	}

	@Test
	public void afterAdding4ChunksWhenRetrieving3TheyAreReturnedInReverseOrder() {
		memory.addAll(0l, createChunks(0, 4));
		assertChunksInMemory(1l, 3, new Long(3), new Long(2), new Long(1));

		memory.addAll(2l, createChunks(4, 6));
		assertChunksInMemory(3l, 3, new Long(5), new Long(4), new Long(3));
	}

	@Test
	public void equalChunksAreNotAddedTwice() {
		memory.addAll(0l, createChunks(0, 1));
		assertChunksInMemory(1l, 3, new Long(0));

		memory.addAll(2l, createChunks(0, 1));
		assertChunksInMemory(3l, 3, new Long(0));
	}

}
