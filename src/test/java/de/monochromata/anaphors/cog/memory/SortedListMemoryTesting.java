package de.monochromata.anaphors.cog.memory;

import static java.util.stream.Collectors.toList;
import static java.util.stream.LongStream.range;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import de.monochromata.anaphors.cog.activation.ActivationFormula;

public interface SortedListMemoryTesting {

	ActivationFormula activationFormula();

	Memory<Long> memory();

	default List<Chunk<Long>> createChunks(final int offset, final int count) {
		return range(offset, count).mapToObj(i -> new DefaultChunk<>(activationFormula(), i)).collect(toList());
	}

	default void assertChunksInMemory(final long timestamp, final int maxNumber, final Long... represented) {
		assertThat(memory().getChunks(timestamp, maxNumber)).extracting("represented")
				.containsExactly((Object[]) represented);
	}

}
