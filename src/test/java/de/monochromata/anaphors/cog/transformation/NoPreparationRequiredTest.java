package de.monochromata.anaphors.cog.transformation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.cog.transform.CheckResult;
import de.monochromata.anaphors.cog.transform.NoPreparationRequired;

public class NoPreparationRequiredTest extends AbstractPrepararatoryTransformationTest {

    public NoPreparationRequiredTest() {
        super("NOP", NoPreparationRequired::new);
    }

    @Override
    protected String getNameOfCheckResultClass() {
        return "de.monochromata.anaphors.cog.transform.NoPreparationRequired$NopCheckResult";
    }

    @Test
    @SuppressWarnings("unchecked")
    public void canPerformFailsForExpressionsThatAreInDifferentInvocables() {
        assertCanPerformFails(
                transformationsSpi -> when(transformationsSpi.partOfSameInvocable(null, null)).thenReturn(false));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void canPerformSucceedsForExpressionsFromDifferentInvocables() {
        assertCanPerformSucceeds(
                transformationsSpi -> when(transformationsSpi.partOfSameInvocable(null, null)).thenReturn(true));
    }

    @Test
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void performReturnsThePreliminaryAnaphora() {
        when(chunk.getRepresented()).thenReturn(null);
        when(transformationsSpi.partOfSameInvocable(null, null)).thenReturn(true);

        final CheckResult result = getStrategy().canPerform(chunk, null, null);
        final ASTBasedAnaphora potentialAnaphora = getStrategy().perform(result, preliminaryAnaphora);

        assertThat(potentialAnaphora).isSameAs(preliminaryAnaphora);
    }

}
