package de.monochromata.anaphors.cog.transformation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.transform.ASTTransformation;
import de.monochromata.anaphors.cog.transform.AddParameterToCallChain;
import de.monochromata.anaphors.cog.transform.CheckResult;

public class AddParameterToCallChainTest extends AbstractPrepararatoryTransformationTest {

	public AddParameterToCallChainTest() {
		super("AddParam", AddParameterToCallChain::new);
	}

	@Override
	protected String getNameOfCheckResultClass() {
		return "de.monochromata.anaphors.cog.transform.AddParameterToCallChain$AddParamCheckResult";
	}

	@Test
	@SuppressWarnings("unchecked")
	public void canPerformFailsForExpressionsThatAreNotOnCallChain() {
		assertCanPerformFails(
				transformationsSpi -> when(transformationsSpi.passAlongCallChain(null, null, null)).thenReturn(null));
	}

	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void canPerformSucceedsForExpressionsOnCallChain() {
		final ASTTransformation mockTransformation = mock(ASTTransformation.class);
		assertCanPerformSucceeds(transformationsSpi -> when(transformationsSpi.passAlongCallChain(null, null, null))
				.thenReturn(mockTransformation));
	}

	@Test
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void performIntroducesAParameterAndCreatesAFreshAnaphora() {
		final ASTBasedAnaphora potentialAnaphora = mock(ASTBasedAnaphora.class, "potentialAnaphora");

		final ASTTransformation mockTransformation = mock(ASTTransformation.class);
		when(mockTransformation.perform(preliminaryAnaphora)).thenReturn(potentialAnaphora);

		when(chunk.getRepresented()).thenReturn(null);
		when(transformationsSpi.passAlongCallChain(null, null, null)).thenReturn(mockTransformation);

		final CheckResult result = getStrategy().canPerform(chunk, null, null);
		final ASTBasedAnaphora returnedAnaphora = getStrategy().perform(result, preliminaryAnaphora);

		assertThat(returnedAnaphora).isSameAs(potentialAnaphora);
	}

}
