package de.monochromata.anaphors.cog.transformation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.function.Consumer;
import java.util.function.Function;

import org.assertj.core.api.AbstractBooleanAssert;
import org.junit.jupiter.api.Test;

import de.monochromata.AbstractSuppliedStrategyTest;
import de.monochromata.anaphors.ast.TestableASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.TestableRelatedExpression;
import de.monochromata.anaphors.ast.spi.TransformationsSpi;
import de.monochromata.anaphors.cog.memory.Chunk;
import de.monochromata.anaphors.cog.transform.CheckResult;
import de.monochromata.anaphors.cog.transform.PreparatoryTransformation;

public abstract class AbstractPrepararatoryTransformationTest extends
        AbstractSuppliedStrategyTest<PreparatoryTransformation<?, ?, ?, ?, ?, ?, ?, ?, ?, TestableASTBasedAnaphora<TestableRelatedExpression>>> {

    @SuppressWarnings("rawtypes")
    protected final TransformationsSpi transformationsSpi = mock(TransformationsSpi.class);
    @SuppressWarnings("rawtypes")
    protected final Chunk chunk = mock(Chunk.class);
    @SuppressWarnings("rawtypes")
    protected final CheckResult checkResult = mock(CheckResult.class);
    @SuppressWarnings("rawtypes")
    protected final TestableASTBasedAnaphora<TestableRelatedExpression> preliminaryAnaphora = mock(
            TestableASTBasedAnaphora.class, "preliminaryAnaphora");
    private final Function<TransformationsSpi, PreparatoryTransformation<?, ?, ?, ?, ?, ?, ?, ?, ?, TestableASTBasedAnaphora<TestableRelatedExpression>>> strategyFactory;
    private PreparatoryTransformation<?, ?, ?, ?, ?, ?, ?, ?, ?, TestableASTBasedAnaphora<TestableRelatedExpression>> strategy;

    @SuppressWarnings("rawtypes")
    public AbstractPrepararatoryTransformationTest(final String expectedKind,
            final Function<TransformationsSpi, PreparatoryTransformation<?, ?, ?, ?, ?, ?, ?, ?, ?, TestableASTBasedAnaphora<TestableRelatedExpression>>> strategyFactory) {
        super(expectedKind);
        this.strategyFactory = strategyFactory;
    }

    @Override
    public PreparatoryTransformation<?, ?, ?, ?, ?, ?, ?, ?, ?, TestableASTBasedAnaphora<TestableRelatedExpression>> getStrategy() {
        if (strategy == null) {
            strategy = strategyFactory.apply(transformationsSpi);
        }
        return strategy;
    }

    @Test
    @SuppressWarnings({ "unchecked" })
    public void passingAnUnknownCheckResultInstanceToPerformRaisesAnException() {
        when(checkResult.canPerformTransformation()).thenReturn(true);

        assertThatThrownBy(() -> getStrategy().perform(checkResult, null))
                .hasMessage("Instance of " + getNameOfCheckResultClass() + " required")
                .isInstanceOf(IllegalArgumentException.class);
    }

    protected abstract String getNameOfCheckResultClass();

    @Test
    @SuppressWarnings("unchecked")
    public void passingAFailedCheckResultToPerformRaisesAnException() {
        when(checkResult.canPerformTransformation()).thenReturn(false);

        assertThatThrownBy(() -> getStrategy().perform(checkResult, preliminaryAnaphora))
                .hasMessage("Only results of successful checks may be passed")
                .isInstanceOf(IllegalArgumentException.class);
    }

    @SuppressWarnings("rawtypes")
    protected void assertCanPerformSucceeds(final Consumer<TransformationsSpi> mockConfigurer) {
        assertCanPerform(mockConfigurer, AbstractBooleanAssert::isTrue);
    }

    @SuppressWarnings("rawtypes")
    protected void assertCanPerformFails(final Consumer<TransformationsSpi> mockConfigurer) {
        assertCanPerform(mockConfigurer, AbstractBooleanAssert::isFalse);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void assertCanPerform(final Consumer<TransformationsSpi> mockConfigurer,
            final Consumer<AbstractBooleanAssert<?>> resultConfigurer) {
        when(chunk.getRepresented()).thenReturn(null);
        mockConfigurer.accept(transformationsSpi);
        resultConfigurer.accept(assertThat(getStrategy().canPerform(chunk, null, null).canPerformTransformation()));
    }

    // @Test
    @Override
    public void testEqualsContract() {
        // Do not test the equals contract because EqualsVerifier 3.1.8 does not allow
        // both inheritance of Object.equals(...) and Object.hashCode() while at the
        // same time ignoring fields.
    }

}
