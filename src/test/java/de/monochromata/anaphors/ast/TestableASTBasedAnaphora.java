package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

public interface TestableASTBasedAnaphora<R extends RelatedExpression<Object, Object, Object, Object, Object, Object, R>>
		extends
		ASTBasedAnaphora<Object, Object, Object, Object, Object, Object, Object, Object, R, TestableASTBasedAnaphora<R>> {

}
