package de.monochromata.anaphors.ast.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import de.monochromata.AbstractSuppliedStrategyTest;
import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.ASTSpis;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.ast.spi.SpiTesting;
import nl.jqno.equalsverifier.api.SingleTypeEqualsVerifierApi;

public abstract class AbstractAnaphorResolutionStrategyTest<S extends AbstractAnaphorResolutionStrategy>
        extends AbstractSuppliedStrategyTest<S> implements AnaphorResolutionStrategyTesting, SpiTesting {

    private ASTSpis spis;
    private final Function<ASTSpis, S> strategyFactory;
    private final List<Class<? extends RelatedExpressionStrategy>> supportedRelatedExpressionStrategies;

    public AbstractAnaphorResolutionStrategyTest(final String expectedKind, final Function<ASTSpis, S> strategyFactory,
            final List<Class<? extends RelatedExpressionStrategy>> supportedRelatedExpressionStrategies) {
        super(expectedKind);
        this.strategyFactory = strategyFactory;
        this.supportedRelatedExpressionStrategies = supportedRelatedExpressionStrategies;
    }

    @Override
    public S getStrategy() {
        final ASTSpis spis = getASTSpis();
        return strategyFactory.apply(spis);
    }

    protected ASTSpis getASTSpis() {
        if (spis == null) {
            spis = createASTSpis();
        }
        return spis;
    }

    @Override
    protected SingleTypeEqualsVerifierApi<? extends Strategy> createEqualsVerifier() {
        return super.createEqualsVerifier().withIgnoredFields("supportedRelatedExpressionStrategies", "anaphorsSpi",
                "relatedExpressionsSpi", "anaphoraResolutionSpi");
    }

    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void testThatReferentHasNoFeaturesInitially() {

        final var scope = new Object();
        final var relatedExpression = mock(RelatedExpression.class);
        final var relatedExpressionsSpi = getASTSpis().relatedExpressionsSpi;

        createExpectationsForReferentCreation(relatedExpression, relatedExpressionsSpi, scope);

        final List<Referent> referents = getStrategy().createPotentialReferents(scope, relatedExpression);

        assertThat(referents).hasSize(1);

        final Set<Feature> features = referents.get(0).getFeatures();

        assertThat(features).isEmpty();
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void referentsWithDifferentFeaturesHaveDifferentHashCodes() throws Exception {
        final Pair<Referent, Referent> referentWithAndWithoutFeatures = createReferentWithAndWithoutFeature(
                getStrategy());
        final Referent referentWithoutFeatures = referentWithAndWithoutFeatures.getLeft();
        final Referent referentWithFeatures = referentWithAndWithoutFeatures.getRight();

        assertThat(referentWithoutFeatures.hashCode()).isNotEqualTo(referentWithFeatures.hashCode());
    }

    @Test
    @SuppressWarnings("rawtypes")
    public void referentsWithDifferentFeaturesAreNotEqual() throws Exception {
        final Pair<Referent, Referent> referentWithAndWithoutFeatures = createReferentWithAndWithoutFeature(
                getStrategy());
        final Referent referentWithoutFeatures = referentWithAndWithoutFeatures.getLeft();
        final Referent referentWithFeatures = referentWithAndWithoutFeatures.getRight();

        assertThat(referentWithoutFeatures.equals(referentWithFeatures)).isFalse();
    }

    @Test
    public void testSupportedRelatedExpressionStrategies() {
        for (final Class<? extends RelatedExpressionStrategy> reStrategyType : supportedRelatedExpressionStrategies) {
            final RelatedExpressionStrategy reStrategy = mock(reStrategyType);
            assertThat(getStrategy().canRelateTo(reStrategy)).isTrue();
        }
    }

    @Test
    public void testUnsupportedRelatedExpressionStrategy() {
        final RelatedExpressionStrategy unknownReStrategy = mock(RelatedExpressionStrategy.class);
        assertThat(getStrategy().canRelateTo(unknownReStrategy)).isFalse();
    }

    protected Pair<Referent, Referent> createReferentWithAndWithoutFeature(
            final AbstractAnaphorResolutionStrategy strategy) throws Exception {

        final Object scope = new Object();
        final RelatedExpression relatedExpression = mock(RelatedExpression.class);
        final RelatedExpressionsSpi relExpSpi = spis.relatedExpressionsSpi;

        createExpectationsForReferentCreation(relatedExpression, relExpSpi, scope);

        final Referent referentWithoutFeatures = createReferent(scope, relatedExpression, strategy);
        final Referent referentWithFeatures = addFeature("foo", createReferent(scope, relatedExpression, strategy));
        return new ImmutablePair<>(referentWithoutFeatures, referentWithFeatures);
    }

    protected void createExpectationsForReferentCreation(final RelatedExpression relatedExpression,
            final RelatedExpressionsSpi relExpSpi, final Object scope) {
        when(relatedExpression.getRelatedExpression()).thenReturn(new Object());
        when(relExpSpi.getFeatures(Mockito.any())).thenReturn(Collections.<Feature<String>>emptySet());
    }
}
