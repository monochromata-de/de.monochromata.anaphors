package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy.MI_KIND;
import static de.monochromata.anaphors.ast.strategy.IA1MrStrategy.IA1Mr_SUPPORTED_RELATED_EXPRESSION_STRATEGIES;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.DefaultIndirectAnaphora;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

public class IA1MrStrategyTest extends AbstractAnaphorResolutionStrategyTest<IA1MrStrategy> {

	public IA1MrStrategyTest() {
		super("IA1Mr",
				spis -> new IA1MrStrategy(spis.anaphorsSpi, spis.relatedExpressionsSpi, spis.anaphoraResolutionSpi),
				IA1Mr_SUPPORTED_RELATED_EXPRESSION_STRATEGIES);
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected void createExpectationsForReferentCreation(final RelatedExpression relatedExpression,
			final RelatedExpressionsSpi relatedExpressionsSpi, final Object scope) {
		super.createExpectationsForReferentCreation(relatedExpression, relatedExpressionsSpi, scope);

		final var relatedExpressionStrategy = mock(RelatedExpressionStrategy.class);
		final var typeBinding = new Object();
		final var methodBinding = new Object();
		final var methodName = new Object();

		final var anaphorResolutionSpi = getASTSpis().anaphoraResolutionSpi;

		when(relatedExpression.resolveType(scope)).thenReturn(typeBinding);
		when(relatedExpression.getStrategy()).thenReturn(relatedExpressionStrategy);

		when(relatedExpressionStrategy.getKind()).thenReturn(MI_KIND);

		when(relatedExpressionsSpi.resolveMethodInvocationBinding(any(), eq(scope))).thenReturn(methodBinding);

		when(anaphorResolutionSpi.getAccessibleGetterMethods(typeBinding, scope)).thenReturn(asList(methodBinding));
		when(anaphorResolutionSpi.getMethodName(methodBinding)).thenReturn(methodName);
		when(anaphorResolutionSpi.getMethodDescription(methodBinding)).thenReturn("description of method");
	}

	@Test
	public void doNotCreatePotentialReferentsIfTheMethodBindingCannotBeResolved() {
		final var potentialRelatedExpression = mock(RelatedExpression.class);
		final var relatedExpressionStrategy = mock(RelatedExpressionStrategy.class);
		when(potentialRelatedExpression.getStrategy()).thenReturn(relatedExpressionStrategy);
		when(relatedExpressionStrategy.getKind()).thenReturn(MI_KIND);
		when(getASTSpis().relatedExpressionsSpi.resolveMethodInvocationBinding(potentialRelatedExpression, null))
				.thenReturn(null);

		assertThat(getStrategy().createPotentialReferents(null, potentialRelatedExpression)).isEmpty();
	}

	@Test
	public void doNotCreatePotentialReferentsIfTheReturnTypeCannotBeResolved() {
		final var potentialRelatedExpression = mock(RelatedExpression.class);
		final var relatedExpressionStrategy = mock(RelatedExpressionStrategy.class);
		when(potentialRelatedExpression.getStrategy()).thenReturn(relatedExpressionStrategy);
		when(relatedExpressionStrategy.getKind()).thenReturn(MI_KIND);
		when(getASTSpis().relatedExpressionsSpi.resolveMethodInvocationBinding(potentialRelatedExpression, null))
				.thenReturn(null);
		when(potentialRelatedExpression.resolveType(any())).thenReturn(null);

		assertThat(getStrategy().createPotentialReferents(null, potentialRelatedExpression)).isEmpty();
	}

	@Test
	public void referenceDescriptionReplacesReferentDescriptionByMethodReturnTypeAfterRelatedExpression() {
		final RelatedExpression relatedExpression = mock(RelatedExpression.class);
		when(relatedExpression.getStrategy()).thenReturn(
				new MethodInvocationStrategy<>(getASTSpis().relatedExpressionsSpi, getASTSpis().preferences));
		when(relatedExpression.getRelatedExpression()).thenReturn("relatedExpression");
		when(relatedExpression.getLine()).thenReturn(10);
		when(relatedExpression.getColumn()).thenReturn(5);
		when(relatedExpression.resolveType(any())).thenReturn("ReturnType");

		final AnaphoraResolutionSpi anaphoraResolutionSpi = getASTSpis().anaphoraResolutionSpi;
		when(anaphoraResolutionSpi.getMethodName("methodBinding")).thenReturn("methodName");
		when(anaphoraResolutionSpi.getMethodDescription("methodBinding")).thenReturn("methodDescription");
		when(anaphoraResolutionSpi.resolveReturnType("methodBinding", null)).thenReturn("ReturnType");
		when(anaphoraResolutionSpi.createIndirectAnaphora(any(), any(), any(), any(), anyBoolean())).thenAnswer(
				invocation -> new DefaultIndirectAnaphora<>(invocation.getArgument(0), invocation.getArgument(1),
						invocation.getArgument(2), invocation.getArgument(3), invocation.getArgument(4)));

		final RelatedExpressionsSpi relatedExpressionsSpi = getASTSpis().relatedExpressionsSpi;
		when(relatedExpressionsSpi.getIdentifier("ReturnType")).thenReturn("ReturnType");
		when(relatedExpressionsSpi.identifierToString("ReturnType")).thenReturn("ReturnType");
		when(relatedExpressionsSpi.resolveMethodInvocationBinding("relatedExpression", null))
				.thenReturn("methodBinding");

		final Referent referent = (Referent) getStrategy().createPotentialReferents(null, relatedExpression).get(0);
		final ASTBasedAnaphora anaphora = getStrategy().createAnaphora(null, "anaphor", "anaphorExpression",
				relatedExpression, referent, new NameRecurrence<>(getASTSpis().anaphorsSpi));

		assertThat(anaphora.getReferenceDescription()).isEqualTo("relatedExpression:ReturnType at 10:5 (MI-IA1Mr-Rn)");
	}

}
