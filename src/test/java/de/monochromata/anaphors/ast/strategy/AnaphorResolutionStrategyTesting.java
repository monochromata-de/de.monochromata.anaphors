package de.monochromata.anaphors.ast.strategy;

import static java.util.Collections.singleton;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import de.monochromata.anaphors.ast.feature.DefaultFeatureContainer;
import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

public interface AnaphorResolutionStrategyTesting {

	default Referent createReferent(final Object scope, final RelatedExpression relatedExpression,
			final AbstractAnaphorResolutionStrategy strategy) {
		return (Referent) strategy.createPotentialReferents(scope, relatedExpression).get(0);
	}

	default Referent addFeature(final String feature, final Referent referent)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		final Method addAllMethod = DefaultFeatureContainer.class.getDeclaredMethod("addAll", Set.class);
		addAllMethod.setAccessible(true);
		addAllMethod.invoke(referent, singleton((Feature) () -> feature));
		return referent;
	}

}
