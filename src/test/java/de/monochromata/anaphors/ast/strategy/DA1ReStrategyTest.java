package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.strategy.DA1ReStrategy.DA1Re_SUPPORTED_RELATED_EXPRESSION_STRATEGIES;
import static org.mockito.Mockito.when;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

public class DA1ReStrategyTest extends AbstractAnaphorResolutionStrategyTest<DA1ReStrategy> {

	public DA1ReStrategyTest() {
		super("DA1Re",
				spis -> new DA1ReStrategy(spis.anaphorsSpi, spis.relatedExpressionsSpi, spis.anaphoraResolutionSpi),
				DA1Re_SUPPORTED_RELATED_EXPRESSION_STRATEGIES);
	}

	@Override
	protected void createExpectationsForReferentCreation(final RelatedExpression relatedExpression,
			final RelatedExpressionsSpi relExpSpi, final Object scope) {
		super.createExpectationsForReferentCreation(relatedExpression, relExpSpi, scope);
		when(relatedExpression.getDescription()).thenReturn("description of the related expression");
	}

}
