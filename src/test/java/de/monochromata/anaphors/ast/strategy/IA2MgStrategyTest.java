package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.strategy.IA2MgStrategy.IA2Mg_SUPPORTED_RELATED_EXPRESSION_STRATEGIES;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

public class IA2MgStrategyTest extends AbstractAnaphorResolutionStrategyTest<IA2MgStrategy> {

	public IA2MgStrategyTest() {
		super("IA2Mg",
				spis -> new IA2MgStrategy(spis.anaphorsSpi, spis.relatedExpressionsSpi, spis.anaphoraResolutionSpi),
				IA2Mg_SUPPORTED_RELATED_EXPRESSION_STRATEGIES);
	}

	@Override
	protected void createExpectationsForReferentCreation(final RelatedExpression relatedExpression,
			final RelatedExpressionsSpi relExpSpi, final Object scope) {
		super.createExpectationsForReferentCreation(relatedExpression, relExpSpi, scope);

		final Object typeBinding = new Object();
		final Object methodBinding = new Object();
		final Object methodName = new Object();

		final AnaphoraResolutionSpi anaphorResolutionSpi = getASTSpis().anaphoraResolutionSpi;

		when(relatedExpression.resolveType(scope)).thenReturn(typeBinding);
		when(anaphorResolutionSpi.getAccessibleGetterMethods(typeBinding, scope)).thenReturn(asList(methodBinding));
		when(anaphorResolutionSpi.getMethodName(methodBinding)).thenReturn(methodName);
		when(anaphorResolutionSpi.getMethodDescription(methodBinding)).thenReturn("description of method");
	}

	@Test
	public void getAnaphorToBeRealizedYieldsExceptionForMissingGetPrefix() {
		assertThatThrownBy(() -> getStrategy().getAnaphorToBeRealized("doSomething"))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Missing get- prefix in method name doSomething");
	}

	@Test
	public void getAnaphorToBeRealizedYieldsExceptionForGetOnlyMethodName() {
		assertThatThrownBy(() -> getStrategy().getAnaphorToBeRealized("get"))
				.isInstanceOf(IllegalArgumentException.class).hasMessage("Method name contains get- prefix only");
	}

	@Test
	public void getAnaphorToBeRealizedYieldsExceptionForMethodThatContinuesLowerCaseAfterGetPrefix() {
		assertThatThrownBy(() -> getStrategy().getAnaphorToBeRealized("getting"))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Method name must continue in upper case after get- prefix: getting");
	}

	@Test
	public void getAnaphorToBeRealizedReturnsSingleCharAnaphorWithoutGetPrefix() {
		assertThat(getStrategy().getAnaphorToBeRealized("getX")).isEqualTo("x");
	}

	@Test
	public void getAnaphorToBeRealizedReturnsUcFirstAnaphorWithoutGetPrefix() {
		assertThat(getStrategy().getAnaphorToBeRealized("getFoo")).isEqualTo("foo");
	}

}
