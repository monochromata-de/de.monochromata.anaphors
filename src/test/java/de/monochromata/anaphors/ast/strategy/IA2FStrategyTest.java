package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.strategy.IA2FStrategy.IA2F_SUPPORTED_RELATED_EXPRESSION_STRATEGIES;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.AnaphoraResolutionSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.perspectivation.Perspectivation.Hide;
import de.monochromata.anaphors.perspectivation.Perspectivation.ToLowerCase;

public class IA2FStrategyTest extends AbstractAnaphorResolutionStrategyTest<IA2FStrategy> {

	public IA2FStrategyTest() {
		super("IA2F", spis -> new IA2FStrategy(spis.anaphorsSpi, spis.relatedExpressionsSpi, spis.anaphoraResolutionSpi),
				IA2F_SUPPORTED_RELATED_EXPRESSION_STRATEGIES);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void createExpectationsForReferentCreation(final RelatedExpression relatedExpression,
			final RelatedExpressionsSpi relExpSpi, final Object scope) {
		super.createExpectationsForReferentCreation(relatedExpression, relExpSpi, scope);

		final Object typeBinding = new Object();
		final Object fieldBinding = new Object();
		final Object fieldName = new Object();

		final AnaphoraResolutionSpi anaphorResolutionSpi = getASTSpis().anaphoraResolutionSpi;

		when(relatedExpression.resolveType(scope)).thenReturn(typeBinding);
		when(anaphorResolutionSpi.getAccessibleFields(typeBinding, scope)).thenReturn(asList(fieldBinding));
		when(anaphorResolutionSpi.getFieldName(fieldBinding)).thenReturn(fieldName);
		when(anaphorResolutionSpi.getFieldDescription(fieldBinding)).thenReturn("description of field");
		when(relExpSpi.toQualifiedIdentifier(fieldName)).thenReturn(fieldName);
	}

	@Test
	public void underspecificationLowerCasesUpperCaseAnaphor() {
		assertThat(getStrategy().underspecifyAnaphorForIA2F(10, true)).containsExactly(new Hide(0, 10),
				new ToLowerCase(10));
	}

	@Test
	public void underspecificationDoesNotAddToLowerCaseIfAnaphorIsAlreadyLowerCase() {
		assertThat(getStrategy().underspecifyAnaphorForIA2F(10, false)).containsExactly(new Hide(0, 10));
	}

	@Test
	public void emptyRealizedAnaphorDoesNotYieldException() {
		assertThat(getStrategy().getAnaphorToBeRealized("")).isEmpty();
	}

	@Test
	public void singleUpperCaseCharacterRealizedAnaphorIsLowerCased() {
		assertThat(getStrategy().getAnaphorToBeRealized("A")).isEqualTo("a");
	}

	@Test
	public void multiCharacterUpperCaseRealizedAnaphorIsLowerCased() {
		assertThat(getStrategy().getAnaphorToBeRealized("AlphaBeta")).isEqualTo("alphaBeta");
	}

	@Test
	public void lowerCaseRealizedAnaphorIsRetained() {
		assertThat(getStrategy().getAnaphorToBeRealized("beta")).isEqualTo("beta");
	}
}
