package de.monochromata.anaphors.ast;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;
import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.spi.ASTSpis;
import de.monochromata.anaphors.ast.spi.SpiTesting;

public class ASTBasedAnaphorResolutionTest implements SpiTesting {

	@Test
	public void referentializationStrategies_atLeastNameRecurrenceIsProvided() {
		assertThatThrownBy(() -> new ASTBasedAnaphorResolution<>(null, null, emptyList(), null))
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("No referentialization strategies provided. At least "
						+ NameRecurrence.class.getSimpleName() + " needs to be provided because "
						+ LocalTempVariableIntroducingStrategy.class.getSimpleName()
						+ " instances use it for re-resolution.");
	}

	@Test
	public void referentializationStrategies_firstMustBeNameRecurrence() {
		final ASTSpis spis = createASTSpis();
		assertThatThrownBy(
				() -> new ASTBasedAnaphorResolution(null, null, List.of(new TypeRecurrence<>(spis.anaphorsSpi)), null))
						.isInstanceOf(IllegalArgumentException.class)
						.hasMessage("No referentialization strategies provided. At least "
								+ NameRecurrence.class.getSimpleName() + " needs to be provided because "
								+ LocalTempVariableIntroducingStrategy.class.getSimpleName()
								+ " instances use it for re-resolution.");
	}

	@Test
	public void referentializationStrategies_firstIsNameRecurrence() {
		final ASTSpis spis = createASTSpis();
		new ASTBasedAnaphorResolution(null, null, List.of(new NameRecurrence<>(spis.anaphorsSpi)), null);
		// No exception is raised
	}

}
