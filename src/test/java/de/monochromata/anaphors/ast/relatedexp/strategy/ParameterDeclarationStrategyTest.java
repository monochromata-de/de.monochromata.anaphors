package de.monochromata.anaphors.ast.relatedexp.strategy;

import static de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy.PD_KIND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.spi.ASTSpis;

public class ParameterDeclarationStrategyTest
        extends AbstractRelatedExpressionStrategyTest<ParameterDeclarationStrategy> {

    public ParameterDeclarationStrategyTest() {
        super("PD", spis -> new ParameterDeclarationStrategy(spis.relatedExpressionsSpi, spis.preferences));
    }

    @Override
    public ASTSpis createASTSpis() {
        final ASTSpis spis = super.createASTSpis();
        when(spis.relatedExpressionsSpi.compare(any(), any()))
                .thenAnswer(invocation -> invocation.getArgument(0).equals(invocation.getArgument(1)));
        return spis;
    }

    /*
     * Functionality has been moved to d.m.e.a
     *
     * @Test public void
     * twoInstancesReferringToIdenticalASTNodeCanBeUsedInsteadOfEachOther() { final
     * String astNode = "astNode"; final RelatedExpression re1 =
     * getStrategy().createRelatedExpression(astNode); final RelatedExpression re2 =
     * getStrategy().createRelatedExpression(astNode);
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isTrue();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isTrue(); }
     *
     * @Test public void
     * twoInstancesReferringToEqualButNonIdenticalASTNodesCannotBeUsedInsteadOfEachOther
     * () { // This case could apply of related expressions from different members
     * are // collected at the same time. final String astNode = "astNode"; final
     * RelatedExpression re1 = getStrategy().createRelatedExpression(new
     * String(astNode)); final RelatedExpression re2 =
     * getStrategy().createRelatedExpression(new String(astNode));
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isFalse();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isFalse(); }
     *
     * @Test public void
     * twoInstancesReferringToUnequalASTNodesCannotBeUsedInsteadOfEachOther() {
     * final RelatedExpression re1 = getStrategy().createRelatedExpression("node1");
     * final RelatedExpression re2 = getStrategy().createRelatedExpression("node2");
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isFalse();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isFalse(); }
     *
     */

    @Test
    public void getKindOfRelatedExpressionToBeRealized_returnsPDKind() {
        assertThat(getStrategy().getKindOfRelatedExpressionToBeRealized()).isEqualTo(PD_KIND);
    }
}
