package de.monochromata.anaphors.ast.relatedexp;

public interface TestableRelatedExpression
		extends RelatedExpression<Object, Object, Object, Object, Object, Object, TestableRelatedExpression> {

}
