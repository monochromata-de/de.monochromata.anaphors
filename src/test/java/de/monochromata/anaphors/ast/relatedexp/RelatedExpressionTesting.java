package de.monochromata.anaphors.ast.relatedexp;

import static org.mockito.Mockito.when;

import org.mockito.Mockito;

public interface RelatedExpressionTesting {

	default TestableRelatedExpression mockRelatedExpression() {
		return mockRelatedExpression(new Object());
	}

	default TestableRelatedExpression mockRelatedExpression(final Object relatedExpressionNode) {
		final TestableRelatedExpression relatedExpression = Mockito.mock(TestableRelatedExpression.class);
		when(relatedExpression.getRelatedExpression()).thenReturn(relatedExpressionNode);
		return relatedExpression;
	}

}
