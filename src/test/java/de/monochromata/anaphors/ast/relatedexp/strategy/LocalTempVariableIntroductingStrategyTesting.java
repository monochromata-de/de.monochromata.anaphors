package de.monochromata.anaphors.ast.relatedexp.strategy;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

public interface LocalTempVariableIntroductingStrategyTesting {

    default AnaphorPart mockAnaphorPart(final String anaphor,
            final AnaphorResolutionStrategy anaphorResolutionStrategy,
            final ReferentializationStrategy referentializationStrategy) {
        final AnaphorPart anaphorPart = mockAnaphorPart(anaphor, anaphorResolutionStrategy);
        when(anaphorPart.getReferentializationStrategy()).thenReturn(referentializationStrategy);
        return anaphorPart;
    }

    default AnaphorPart mockAnaphorPart(final String anaphor,
            final AnaphorResolutionStrategy anaphorResolutionStrategy) {
        final AnaphorPart anaphorPart = mockAnaphorPart(anaphor);
        when(anaphorPart.getAnaphorResolutionStrategy()).thenReturn(anaphorResolutionStrategy);
        return anaphorPart;
    }

    default AnaphorPart mockAnaphorPart(final String anaphor) {
        final AnaphorPart anaphorPart = mock(AnaphorPart.class);
        when(anaphorPart.getAnaphor()).thenReturn(anaphor);
        return anaphorPart;
    }

}
