package de.monochromata.anaphors.ast.relatedexp.strategy;

public class ClassInstanceCreationStrategyTest
        extends AbstractLocalTempVariableIntroducingStrategyTest<ClassInstanceCreationStrategy> {

    public ClassInstanceCreationStrategyTest() {
        super("CIC", spis -> new ClassInstanceCreationStrategy(spis.relatedExpressionsSpi, spis.preferences));
    }

}
