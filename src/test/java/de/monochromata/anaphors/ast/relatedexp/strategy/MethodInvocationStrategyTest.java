package de.monochromata.anaphors.ast.relatedexp.strategy;

public class MethodInvocationStrategyTest
        extends AbstractLocalTempVariableIntroducingStrategyTest<MethodInvocationStrategy> {

    public MethodInvocationStrategyTest() {
        super("MI", spis -> new MethodInvocationStrategy(spis.relatedExpressionsSpi, spis.preferences));
    }

}
