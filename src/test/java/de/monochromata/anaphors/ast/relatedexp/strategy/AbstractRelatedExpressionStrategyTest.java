package de.monochromata.anaphors.ast.relatedexp.strategy;

import java.util.function.Function;

import de.monochromata.AbstractSuppliedStrategyTest;
import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.spi.ASTSpis;
import de.monochromata.anaphors.ast.spi.SpiTesting;
import nl.jqno.equalsverifier.api.SingleTypeEqualsVerifierApi;

public abstract class AbstractRelatedExpressionStrategyTest<S extends RelatedExpressionStrategy>
        extends AbstractSuppliedStrategyTest<S> implements SpiTesting {

    protected ASTSpis spis;
    private final Function<ASTSpis, S> strategyFactory;

    public AbstractRelatedExpressionStrategyTest(final String expectedKind,
            final Function<ASTSpis, S> strategyFactory) {
        super(expectedKind);
        this.strategyFactory = strategyFactory;
    }

    @Override
    public S getStrategy() {
        spis = getASTSpis();
        return strategyFactory.apply(spis);
    }

    protected ASTSpis getASTSpis() {
        if (spis == null) {
            spis = createASTSpis();
        }
        return spis;
    }

    @Override
    protected SingleTypeEqualsVerifierApi<? extends Strategy> createEqualsVerifier() {
        return super.createEqualsVerifier().withIgnoredFields("collection", "relatedExpressionsSpi", "preferences");
    }

}
