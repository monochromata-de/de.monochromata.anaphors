package de.monochromata.anaphors.ast.relatedexp.strategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.spi.ASTSpis;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

public abstract class AbstractLocalTempVariableIntroducingStrategyTest<S extends LocalTempVariableIntroducingStrategy>
        extends AbstractRelatedExpressionStrategyTest<S> implements LocalTempVariableIntroductingStrategyTesting {

    public AbstractLocalTempVariableIntroducingStrategyTest(final String expectedKind,
            final Function<ASTSpis, S> strategyFactory) {
        super(expectedKind, strategyFactory);
    }

    @Override
    public ASTSpis createASTSpis() {
        final ASTSpis spis = super.createASTSpis();
        mockRelatedExpressionsSpi(spis.relatedExpressionsSpi);
        return spis;
    }

    @SuppressWarnings("unchecked")
    protected void mockRelatedExpressionsSpi(final RelatedExpressionsSpi relatedExpressionsSpi) {
        when(relatedExpressionsSpi.compare(any(), any()))
                .thenAnswer(invocation -> invocation.getArgument(0).equals(invocation.getArgument(1)));
        when(relatedExpressionsSpi.getReservedTypeVar(any())).thenReturn("var");
        when(relatedExpressionsSpi.guessTempName(any(), any(), any())).thenReturn("tempName");
        when(relatedExpressionsSpi.getLength(any()))
                .thenAnswer(invocation -> ((String) invocation.getArgument(0)).length());
        when(relatedExpressionsSpi.getLengthOfSimpleNameOfType(any()))
                .thenAnswer(invocation -> ((String) invocation.getArgument(0)).length());
    }

    @Test
    public void realizedKindOfRelatedExpressionIsLVD() {
        assertThat(getStrategy().getKindOfRelatedExpressionToBeRealized()).isEqualTo("LVD");
    }

    /*
     * Functionality has been moved to d.m.e.a
     *
     * @Test public void
     * twoInstancesReferringToIdenticalASTNodeCanBeUsedInsteadOfEachOther() { final
     * String astNode = "astNode"; final RelatedExpression re1 =
     * getStrategy().createRelatedExpression(astNode); final RelatedExpression re2 =
     * getStrategy().createRelatedExpression(astNode);
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isTrue();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isTrue(); }
     *
     * @Test public void
     * twoInstancesReferringToEqualButNonIdenticalASTNodesCannotBeUsedInsteadOfEachOther
     * () { final String astNode = "astNode"; final RelatedExpression re1 =
     * getStrategy().createRelatedExpression(new String(astNode)); final
     * RelatedExpression re2 = getStrategy().createRelatedExpression(new
     * String(astNode));
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isFalse();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isFalse(); }
     *
     * @Test public void
     * twoInstancesReferringToUnequalASTNodesCannotBeUsedInsteadOfEachOther() {
     * final RelatedExpression re1 = getStrategy().createRelatedExpression("node1");
     * final RelatedExpression re2 = getStrategy().createRelatedExpression("node2");
     *
     * assertThat(re1.canBeUsedInsteadOf(re2)).isFalse();
     * assertThat(re2.canBeUsedInsteadOf(re1)).isFalse(); }
     *
     * @Test public void
     * useRelatedExpressionTypeNameWhenLocalVariableTypeInferenceIsNotUsed() { final
     * RelatedExpression relatedExpression =
     * getStrategy().createRelatedExpression("node1"); final Object
     * typeForTempVariable = ((AbstractLocalTempVariableIntroducingStrategy)
     * getStrategy()) .getTypeForTempVariable(relatedExpression, null, null);
     *
     * assertThat(typeForTempVariable).isEqualTo("RelExpType"); }
     *
     * @Test public void
     * useReservedTypeNameVarWhenLocalVariableTypeInferenceIsUsed() {
     * when(getASTSpis().preferences.getUseLocalVariableTypeInference()).thenReturn(
     * true);
     * when(getASTSpis().relatedExpressionsSpi.supportsLocalVariableTypeInference(
     * any())).thenReturn(true); final RelatedExpression relatedExpression =
     * getStrategy().createRelatedExpression("node1");
     *
     * final Object typeForTempVariable =
     * ((AbstractLocalTempVariableIntroducingStrategy) getStrategy())
     * .getTypeForTempVariable(relatedExpression, null, null);
     *
     * assertThat(typeForTempVariable).isEqualTo("var"); }
     *
     * @Test public void
     * underspecifyRelatedExpressionWithoutLocalVariableTypeInference() { final
     * RelatedExpression relatedExpression =
     * getStrategy().createRelatedExpression("node1");
     *
     * final List<Perspectivation> perspectivations =
     * ((AbstractLocalTempVariableIntroducingStrategy) getStrategy())
     * .underspecifyRelatedExpressionForLocalTempVariable(relatedExpression, null,
     * null);
     *
     * assertThat(perspectivations).containsExactly(new Hide(0,
     * "RelExpType tempName = ".length())); }
     *
     * @Test public void
     * underspecifyRelatedExpressionWithLocalVariableTypeInference() {
     * when(getASTSpis().preferences.getUseLocalVariableTypeInference()).thenReturn(
     * true);
     * when(getASTSpis().relatedExpressionsSpi.supportsLocalVariableTypeInference(
     * any())).thenReturn(true); final RelatedExpression relatedExpression =
     * getStrategy().createRelatedExpression("node1");
     *
     * final List<Perspectivation> perspectivations =
     * ((AbstractLocalTempVariableIntroducingStrategy) getStrategy())
     * .underspecifyRelatedExpressionForLocalTempVariable(relatedExpression, null,
     * null);
     *
     * assertThat(perspectivations).containsExactly(new Hide(0,
     * "var tempName = ".length())); }
     */
}
