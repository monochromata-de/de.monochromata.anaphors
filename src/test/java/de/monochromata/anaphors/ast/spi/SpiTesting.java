package de.monochromata.anaphors.ast.spi;

import static org.mockito.Mockito.mock;

import de.monochromata.anaphors.preferences.Preferences;

public interface SpiTesting {

    default ASTSpis createASTSpis() {
        final AnaphorsSpi anaphorsSpi = mock(AnaphorsSpi.class);
        final RelatedExpressionsSpi relatedExpressionsSpi = mock(RelatedExpressionsSpi.class);
        final AnaphoraResolutionSpi anaphorResolutionSpi = mock(AnaphoraResolutionSpi.class);
        final Preferences preferences = mock(Preferences.class);
        return new ASTSpis(anaphorsSpi, relatedExpressionsSpi, anaphorResolutionSpi, preferences);
    }

}
