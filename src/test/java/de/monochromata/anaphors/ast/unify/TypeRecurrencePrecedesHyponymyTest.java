package de.monochromata.anaphors.ast.unify;

import static de.monochromata.anaphors.ast.reference.strategy.concept.FauxHyponymy.HyFx_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy.Hy_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence.Rt_KIND;
import static de.monochromata.anaphors.ast.unify.HyponymyPrecedesFauxHyponymy.preferHynonymyOverFauxHyponymy;
import static de.monochromata.anaphors.ast.unify.TypeRecurrencePrecedesHyponymy.preferTypeRecurrenceOverHynonymy;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.reference.Referent;

public class TypeRecurrencePrecedesHyponymyTest implements UnificationTesting {

	@Test
	public void ambiguityBetweenRtAndHy_rtIsPreferred() {
		final var rtAnaphora = getAnaphoraForReferentialization(Rt_KIND);
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND);

		assertThat(preferTypeRecurrenceOverHynonymy(List.of(rtAnaphora, hyAnaphora))).containsExactly(rtAnaphora);
	}

	@Test
	public void noAmbiguityBetweenRtAndHy_ifFirstIsNotRt() {
		final var hyFxAnaphora = getAnaphoraForReferentialization(HyFx_KIND);
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND);
		final var allAnaphoras = List.of(hyFxAnaphora, hyAnaphora);

		assertThat(preferTypeRecurrenceOverHynonymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

	@Test
	public void noAmbiguityBetweenRtAndHy_ifSecondIsNotHy() {
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND);
		final var hyFxAnaphora = getAnaphoraForReferentialization(HyFx_KIND);
		final var allAnaphoras = List.of(hyAnaphora, hyFxAnaphora);

		assertThat(preferTypeRecurrenceOverHynonymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

	@Test
	public void noAmbiguityBetweenRtAndHy_forDifferentReferents() {
		final var rtAnaphora = getAnaphoraForReferentialization(Rt_KIND, mock(Referent.class));
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND, mock(Referent.class));
		final var allAnaphoras = List.of(rtAnaphora, hyAnaphora);

		assertThat(preferHynonymyOverFauxHyponymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

}
