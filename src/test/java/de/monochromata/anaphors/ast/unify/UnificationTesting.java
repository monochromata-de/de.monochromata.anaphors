package de.monochromata.anaphors.ast.unify;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.reference.Referent;

public interface UnificationTesting {

	Referent DEFAULT_REFERENT = mock(Referent.class);

	default ASTBasedAnaphora getAnaphoraForReferentialization(final String kindOfReferentialization) {
		return getAnaphoraForReferentialization(kindOfReferentialization, DEFAULT_REFERENT);
	}

	default ASTBasedAnaphora getAnaphoraForReferentialization(final String kindOfReferentialization,
			final Referent referent) {
		final var anaphora = mock(ASTBasedAnaphora.class, RETURNS_DEEP_STUBS);
		when(anaphora.getReferentializationStrategy().getKind()).thenReturn(kindOfReferentialization);
		when(anaphora.getReferent()).thenReturn(referent);
		return anaphora;
	}

}
