package de.monochromata.anaphors.ast.unify;

import static de.monochromata.anaphors.ast.reference.strategy.concept.FauxHyponymy.HyFx_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy.Hy_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence.Rt_KIND;
import static de.monochromata.anaphors.ast.unify.HyponymyPrecedesFauxHyponymy.preferHynonymyOverFauxHyponymy;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.reference.Referent;

public class HyponymyPrecedesFauxHyponymyTest implements UnificationTesting {

	@Test
	public void ambiguityBetweenHyFxAndHy_hyIsPreferredForClassBasedHy() {
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND);
		final var hyFxAnaphora = getAnaphoraForReferentialization(HyFx_KIND);

		assertThat(preferHynonymyOverFauxHyponymy(List.of(hyAnaphora, hyFxAnaphora))).containsExactly(hyAnaphora);
	}

	@Test
	public void noAmbiguityBetweenHyFxAndHy_ifFirstIsNotHy() {
		final var rtAnaphora = getAnaphoraForReferentialization(Rt_KIND);
		final var hyFxAnaphora = getAnaphoraForReferentialization(HyFx_KIND);
		final var allAnaphoras = List.of(rtAnaphora, hyFxAnaphora);

		assertThat(preferHynonymyOverFauxHyponymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

	@Test
	public void noAmbiguityBetweenHyFxAndHy_ifSecondIsNotHyFx() {
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND);
		final var rtAnaphora = getAnaphoraForReferentialization(Rt_KIND);
		final var allAnaphoras = List.of(hyAnaphora, rtAnaphora);

		assertThat(preferHynonymyOverFauxHyponymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

	@Test
	public void noAmbiguityBetweenHyFxAndHy_forDifferentReferents() {
		final var hyAnaphora = getAnaphoraForReferentialization(Hy_KIND, mock(Referent.class));
		final var hyFxAnaphora = getAnaphoraForReferentialization(HyFx_KIND, mock(Referent.class));
		final var allAnaphoras = List.of(hyAnaphora, hyFxAnaphora);

		assertThat(preferHynonymyOverFauxHyponymy(allAnaphoras)).containsExactlyElementsOf(allAnaphoras);
	}

}
