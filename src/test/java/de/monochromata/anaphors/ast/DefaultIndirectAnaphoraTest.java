package de.monochromata.anaphors.ast;

import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class DefaultIndirectAnaphoraTest {

    @Test
    public void testEqualsContract() {
        EqualsVerifier
                .forClass(DefaultIndirectAnaphora.class)
                .usingGetClass()
                .withIgnoredFields("customReferenceDescriptionSupplier")
                .verify();
    }
}
