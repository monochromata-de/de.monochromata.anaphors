package de.monochromata.anaphors.ast;

import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class DefaultAnaphorPartTest {

    @Test
    public void testEqualsContract() {
        EqualsVerifier.forClass(DefaultAnaphorPart.class).usingGetClass().verify();
    }

}
