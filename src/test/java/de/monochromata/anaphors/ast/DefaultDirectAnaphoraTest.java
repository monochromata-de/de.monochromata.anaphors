package de.monochromata.anaphors.ast;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import nl.jqno.equalsverifier.EqualsVerifier;

public class DefaultDirectAnaphoraTest {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testAccessors() {

        final RelatedExpression relatedExpression = mock(RelatedExpression.class);
        final Object anaphor = "anaphor";
        final Referent referent = mock(Referent.class);
        final RelatedExpressionStrategy relExprStrategy = mock(RelatedExpressionStrategy.class);
        final AnaphorResolutionStrategy arStrategy = mock(AnaphorResolutionStrategy.class);
        final ReferentializationStrategy refStrategy = mock(ReferentializationStrategy.class);
        final Object binding = "binding";

        when(relatedExpression.getStrategy()).thenReturn(relExprStrategy);

        final DefaultDirectAnaphora dda = new DefaultDirectAnaphora<>(
                new DefaultRelatedExpressionPart<>(relatedExpression),
                new DefaultAnaphorPart<>(anaphor.toString(), anaphor,
                        referent, arStrategy, refStrategy, binding),
                false);
        assertThat(dda).isNotNull();

        assertThat(dda.getAnaphor()).isSameAs(anaphor);
        assertThat(dda.getAnaphorExpression()).isSameAs(anaphor);
        assertThat(dda.getAnaphorResolutionStrategy()).isSameAs(arStrategy);

        assertThat(dda.getAntecedent()).isSameAs(relatedExpression);
        assertThat(dda.getRelatedExpression()).isSameAs(relatedExpression);
        assertThat(dda.getRelatedExpressionStrategy()).isSameAs(relExprStrategy);

        assertThat(dda.getReferent()).isSameAs(referent);
        assertThat(dda.getReferentializationStrategy()).isSameAs(refStrategy);

        assertThat(dda.getBinding()).isSameAs(binding);
        assertThat(dda.isUnderspecified()).isFalse();
        assertThat(dda.isExplicated()).isTrue();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testFacadeMethods() {
        final RelatedExpression relatedExpression = mock(RelatedExpression.class);
        final Object scope = "scope";
        final Object anaphor = "anaphor";
        final Object typeBinding = "typeBinding";
        final Referent referent = mock(Referent.class);
        final RelatedExpressionStrategy relExprStrategy = mock(RelatedExpressionStrategy.class);
        final AnaphorResolutionStrategy arStrategy = mock(AnaphorResolutionStrategy.class);
        final ReferentializationStrategy refStrategy = mock(ReferentializationStrategy.class);
        final Object binding = "binding";

        final String reKind = "RE";
        final String arKind = "AR";
        final String refKind = "REF";

        when(referent.resolveType(scope)).thenReturn(typeBinding);
        when(relatedExpression.getStrategy()).thenReturn(relExprStrategy);
        when(relExprStrategy.getKind()).thenReturn(reKind);
        when(arStrategy.getKind()).thenReturn(arKind);
        when(refStrategy.getKind()).thenReturn(refKind);

        final DefaultDirectAnaphora dda = new DefaultDirectAnaphora<>(
                new DefaultRelatedExpressionPart<>(relatedExpression),
                new DefaultAnaphorPart<>(anaphor.toString(), anaphor,
                        referent, arStrategy, refStrategy, binding),
                true);
        assertThat(dda).isNotNull();
        assertThat(dda.resolveType(scope)).isSameAs(typeBinding);
        assertThat(dda.getKind()).isEqualTo(reKind + "-" + arKind + "-" + refKind);
    }

    @Test
    public void testEqualsContract() {
        EqualsVerifier.forClass(DefaultDirectAnaphora.class).usingGetClass().verify();
    }

    @Test
    public void testToString() {

        final RelatedExpression relatedExpression = mock(RelatedExpression.class);
        final Object scope = "scope";
        final Object anaphor = "anaphor";
        final Object typeBinding = "typeBinding";
        final Referent referent = mock(Referent.class);
        final RelatedExpressionStrategy relExprStrategy = mock(RelatedExpressionStrategy.class);
        final AnaphorResolutionStrategy arStrategy = mock(AnaphorResolutionStrategy.class);
        final ReferentializationStrategy refStrategy = mock(ReferentializationStrategy.class);
        final Object binding = "binding";

        final String reKind = "RE";
        final String arKind = "AR";
        final String refKind = "REF";

        when(relatedExpression.getStrategy()).thenReturn(relExprStrategy);
        when(relExprStrategy.getKind()).thenReturn(reKind);
        when(arStrategy.getKind()).thenReturn(arKind);
        when(refStrategy.getKind()).thenReturn(refKind);

        final DefaultDirectAnaphora dda = new DefaultDirectAnaphora<>(
                new DefaultRelatedExpressionPart<>(relatedExpression),
                new DefaultAnaphorPart<>(anaphor.toString(), anaphor,
                        referent, arStrategy, refStrategy, binding),
                false);
        assertThat(dda).isNotNull();

        assertThat(dda.toString()).isEqualTo("RE-AR-REF [getRelatedExpression()=" + relatedExpression
                + ", getAnaphor()=anaphor, getAnaphorExpression()=anaphor, getReferent()=" + referent.toString()
                + ", getRelatedExpressionStrategy()=" + relExprStrategy.toString()
                + ", getAnaphorResolutionStrategy()=" + arStrategy.toString()
                + ", getReferentializationStrategy()=" + refStrategy.toString()
                + ", getBinding()=binding, isUnderspecified()=false, isExplicated()=true]");
    }
}
