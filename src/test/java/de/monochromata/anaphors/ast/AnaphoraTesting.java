package de.monochromata.anaphors.ast;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpressionTesting;
import de.monochromata.anaphors.ast.relatedexp.TestableRelatedExpression;

public interface AnaphoraTesting extends RelatedExpressionTesting {

	default TestableASTBasedAnaphora<TestableRelatedExpression> mockAnaphora() {
		return mockAnaphora(mockRelatedExpression());
	}

	default TestableASTBasedAnaphora<TestableRelatedExpression> mockAnaphora(
			final TestableRelatedExpression relatedExpression) {
		final var anaphora = mock(TestableASTBasedAnaphora.class);
		final RelatedExpressionPart relatedExpressionPart = mock(RelatedExpressionPart.class);
		final AnaphorPart anaphorPart = mock(AnaphorPart.class);

		when(anaphora.getRelatedExpressionPart()).thenReturn(relatedExpressionPart);
		when(anaphora.getRelatedExpression()).thenReturn(relatedExpression);
		when(relatedExpressionPart.getRelatedExpression()).thenReturn(relatedExpression);
		when(anaphora.getAnaphorPart()).thenReturn(anaphorPart);

		return anaphora;
	}

}
