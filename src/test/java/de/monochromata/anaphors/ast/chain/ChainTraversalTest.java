package de.monochromata.anaphors.ast.chain;

import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElements;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getAnaphorElementsForRelatedExpressionElement;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElementForAnaphorElement;
import static de.monochromata.anaphors.ast.chain.ChainTraversal.getRelatedExpressionElements;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.AnaphoraTesting;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpressionTesting;

public class ChainTraversalTest implements ChainElementTesting, AnaphoraTesting, RelatedExpressionTesting {

    // getRelatedExpressionElements(...)

    @Test
    public void getRelatedExpressionElementWhenIdenticalToRoot() {
        final ChainElement root = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));

        assertThat(getRelatedExpressionElements(root)).containsExactly(root);
    }

    @Test
    public void getRelatedExpressionElementFollowingRoot() {
        final ChainElement leaf = new ChainElement(null, emptyList(), mock(RelatedExpressionPart.class), null, null);
        final ChainElement root = new ChainElement(null, asList(leaf), null, mock(AnaphorPart.class), null);

        assertThat(getRelatedExpressionElements(root)).containsExactly(leaf);
    }

    @Test
    public void getTwoRelatedExpressionElementsFollowingRoot() {
        final ChainElement leaf1 = new ChainElement(null, emptyList(), mock(RelatedExpressionPart.class), null, null);
        final ChainElement leaf2 = new ChainElement(null, emptyList(), mock(RelatedExpressionPart.class), null, null);
        final ChainElement root = new ChainElement(null, asList(leaf1, leaf2), null, null, null);

        assertThat(getRelatedExpressionElements(root)).containsExactly(leaf1, leaf2);
    }

    @Test
    public void getTwoRelatedExpressionElementsFollowingEachOther() {
        final ChainElement leaf = new ChainElement(null, emptyList(), mock(RelatedExpressionPart.class), null, null);
        final ChainElement intermediate = new ChainElement(null, asList(leaf), mock(RelatedExpressionPart.class), null,
                null);
        final ChainElement root = new ChainElement(null, asList(intermediate), null, null, null);

        assertThat(getRelatedExpressionElements(root)).containsExactly(intermediate, leaf);
    }

    // getAnaphorElements(...)

    @Test
    public void getAnaphorElementWhenIdenticalToRoot() {
        final ChainElement root = new ChainElement<>(null, mock(AnaphorPart.class), null);

        assertThat(getAnaphorElements(root)).containsExactly(root);
    }

    @Test
    public void getAnaphorElementFollowingRoot() {
        final ChainElement leaf = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(leaf), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElements(root)).containsExactly(leaf);
    }

    @Test
    public void getTwoAnaphorElementsFollowingRoot() {
        final ChainElement leaf1 = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement leaf2 = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(leaf1, leaf2), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElements(root)).containsExactly(leaf1, leaf2);
    }

    @Test
    public void getTwoAnaphorElementsFollowingEachOther() {
        final ChainElement leaf = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement intermediate = new ChainElement(null, asList(leaf), null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(intermediate), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElements(root)).containsExactly(intermediate, leaf);
    }

    // getRelatedExpressionElementForAnaphorElement(...)

    @Test
    public void getRelatedExpressionElementIdenticalToGivenElement() {
        final ChainElement root = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));

        assertThat(getRelatedExpressionElementForAnaphorElement(root)).isSameAs(root);
    }

    @Test
    public void getRelatedExpressionElementPrecedingAnaphorElement() {
        final ChainElement root = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));
        final ChainElement leaf = new ChainElement(root, mock(AnaphorPart.class), null);

        assertThat(getRelatedExpressionElementForAnaphorElement(leaf)).isSameAs(root);
    }

    @Test
    public void getRelatedExpressionElementIndirectlyPrecedingAnaphorElement() {
        final ChainElement root = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));
        final ChainElement intermediate = new ChainElement(root, mock(AnaphorPart.class), null);
        final ChainElement leaf = new ChainElement(intermediate, mock(AnaphorPart.class), null);

        assertThat(getRelatedExpressionElementForAnaphorElement(leaf)).isSameAs(root);
    }

    @Test
    public void getRelatedExpressionElementDirectlyPrecedingAnaphorElementNotItsPredecessor() {
        final ChainElement root = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));
        final ChainElement intermediate = new ChainElement(root, emptyList(), mock(RelatedExpressionPart.class),
                mock(AnaphorPart.class), null);
        final ChainElement leaf = new ChainElement(intermediate, mock(AnaphorPart.class), null);

        assertThat(getRelatedExpressionElementForAnaphorElement(leaf)).isSameAs(intermediate);
    }

    @Test
    public void throwExceptionIfNoRelatedExpressionElementIsFound() {
        final ChainElement leaf = new ChainElement(null, mock(AnaphorPart.class), null);

        assertThatThrownBy(() -> getRelatedExpressionElementForAnaphorElement(leaf))
                .isInstanceOf(IllegalStateException.class);
    }

    // getAnaphorElementsForRelatedExpressionElement(...)

    @Test
    public void getAnaphorElementsSucceedingRelatedExpressionElement() {
        final ChainElement leaf = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(leaf), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElementsForRelatedExpressionElement(root)).containsExactly(leaf);
    }

    @Test
    public void getAnaphorElementsIndirectlySucceedingRelatedExpressionElement() {
        final ChainElement leaf = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement intermediate = new ChainElement(null, asList(leaf), null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(intermediate), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElementsForRelatedExpressionElement(root)).containsExactly(intermediate, leaf);
    }

    @Test
    public void getAnaphorElementsDirectlySucceedingRelatedExpressionElementNotItsSuccessor() {
        final ChainElement leaf1 = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement intermediate = new ChainElement(asList(leaf1), mock(RelatedExpressionPart.class));
        final ChainElement leaf2 = new ChainElement(null, mock(AnaphorPart.class), null);
        final ChainElement root = new ChainElement(asList(intermediate, leaf2), mock(RelatedExpressionPart.class));

        assertThat(getAnaphorElementsForRelatedExpressionElement(root)).containsExactly(leaf2);
    }

    @Test
    public void throwExceptionIfNoAnaphorElementIsFound() {
        final ChainElement leaf = new ChainElement(emptyList(), mock(RelatedExpressionPart.class));

        assertThatThrownBy(() -> getAnaphorElementsForRelatedExpressionElement(leaf))
                .isInstanceOf(IllegalStateException.class);
    }
}
