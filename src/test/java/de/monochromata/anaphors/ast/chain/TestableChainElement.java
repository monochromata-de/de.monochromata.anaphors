package de.monochromata.anaphors.ast.chain;

import java.util.List;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.TestableASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.TestableRelatedExpression;

public class TestableChainElement extends
		ChainElement<Object, Object, Object, Object, Object, Object, Object, Object, Object, TestableRelatedExpression, TestableASTBasedAnaphora<TestableRelatedExpression>, TestableChainElement> {

	public TestableChainElement(final TestableChainElement previous, final List<TestableChainElement> next,
			final RelatedExpressionPart<Object, Object, Object, Object, Object, Object, Object, Object, TestableRelatedExpression> relatedExpression,
			final AnaphorPart<Object, Object, Object, Object, Object, Object, Object, Object, TestableRelatedExpression, TestableASTBasedAnaphora<TestableRelatedExpression>> anaphor,
			final Object anaphorAttachment) {
		super(previous, next, relatedExpression, anaphor, anaphorAttachment);
	}

}
