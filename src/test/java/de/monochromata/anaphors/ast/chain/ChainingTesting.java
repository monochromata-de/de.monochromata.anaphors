package de.monochromata.anaphors.ast.chain;

import de.monochromata.anaphors.ast.AnaphoraTesting;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpressionTesting;

public interface ChainingTesting extends AnaphoraTesting, RelatedExpressionTesting, ChainElementTesting {

}
