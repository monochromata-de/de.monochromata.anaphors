package de.monochromata.anaphors.ast.chain;

import static de.monochromata.anaphors.ast.chain.Chaining.emptyListOfChains;
import static de.monochromata.anaphors.ast.chain.Chaining.singletonListOfChains;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.TestableASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.TestableRelatedExpression;

public class ChainingTest implements ChainingTesting {

	@Test
	public void createEmptyListOfChains() {
		assertThat((List<? extends Object>) emptyListOfChains()).isEmpty();
	}

	@Test
	public void createSingletonListOfChains() {
		final TestableASTBasedAnaphora<TestableRelatedExpression> anaphora = mockAnaphora();
		final Object anaphorAttachment = new Object();

		final List<TestableChainElement> chains = singletonListOfChains(anaphora, anaphorAttachment,
				TestableChainElement::new);

		assertThat(chains).hasSize(1);
		assertSingleAnaphoraChain(anaphora, chains.get(0));
		assertThat(chains.get(0).next.get(0).anaphorAttachment).isSameAs(anaphorAttachment);
	}

	@Test
	public void mergeAnaphoraForDifferentRelatedExpressionNodeIntoSingletonListOfChains() {
		final TestableASTBasedAnaphora<TestableRelatedExpression> anaphora1 = mockAnaphora();
		final Object anaphorAttachment1 = new Object();
		final TestableASTBasedAnaphora<TestableRelatedExpression> anaphora2 = mockAnaphora();
		final Object anaphorAttachment2 = new Object();

		final List<TestableChainElement> initialChains = singletonListOfChains(anaphora1, anaphorAttachment1,
				TestableChainElement::new);
		final List<TestableChainElement> mergedChains = Chaining.merge(initialChains, anaphora2, anaphorAttachment2,
				TestableChainElement::new);

		assertThat(mergedChains).hasSize(2);
		assertSingleAnaphoraChain(anaphora1, mergedChains.get(0));
		assertThat(mergedChains.get(0).next.get(0).anaphorAttachment).isSameAs(anaphorAttachment1);
		assertSingleAnaphoraChain(anaphora2, mergedChains.get(1));
		assertThat(mergedChains.get(1).next.get(0).anaphorAttachment).isSameAs(anaphorAttachment2);
	}

	@Test
	public void mergeAnaphoraForSameRelatedExpressionNodeIntoSingletonListOfChains() {
		final Object relatedExpressionNode = new Object();

		final TestableRelatedExpression relatedExpression1 = mockRelatedExpression(relatedExpressionNode);
		final TestableASTBasedAnaphora<TestableRelatedExpression> anaphora1 = mockAnaphora(relatedExpression1);
		final Object anaphorAttachment1 = new Object();
		final TestableRelatedExpression relatedExpression2 = mockRelatedExpression(relatedExpressionNode);
		final TestableASTBasedAnaphora<TestableRelatedExpression> anaphora2 = mockAnaphora(relatedExpression2);
		final Object anaphorAttachment2 = new Object();

		assertThat(anaphora1.getRelatedExpression().getRelatedExpression())
				.isSameAs(anaphora2.getRelatedExpression().getRelatedExpression());

		final List<TestableChainElement> initialChains = singletonListOfChains(anaphora1, anaphorAttachment1,
				TestableChainElement::new);
		final List<TestableChainElement> mergedChains = Chaining.merge(initialChains, anaphora2, anaphorAttachment2,
				TestableChainElement::new);

		assertThat(mergedChains).hasSize(1);

		final TestableChainElement chainRoot = mergedChains.get(0);
		assertThat(chainRoot.next).hasSize(2);

		assertChainElement(chainRoot, null, chainRoot.next, anaphora1.getRelatedExpressionPart(), null);

		// Assert chain leaves
		assertChainElement(chainRoot.next.get(0), chainRoot, emptyList(), null, anaphora1.getAnaphorPart());
		assertThat(chainRoot.next.get(0).anaphorAttachment).isSameAs(anaphorAttachment1);
		assertChainElement(chainRoot.next.get(1), chainRoot, emptyList(), null, anaphora2.getAnaphorPart());
		assertThat(chainRoot.next.get(1).anaphorAttachment).isSameAs(anaphorAttachment2);
	}

}
