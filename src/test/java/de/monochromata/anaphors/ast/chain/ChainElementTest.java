package de.monochromata.anaphors.ast.chain;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import nl.jqno.equalsverifier.EqualsVerifier;

public class ChainElementTest {

	@Test
    public void testEqualsContract() {
        final List<ChainElement> nextElements = new ArrayList<>();
        final ChainElement previous = new ChainElement(nextElements, mock(RelatedExpressionPart.class));
        final ChainElement next = new ChainElement(previous, mock(AnaphorPart.class), null);
        nextElements.add(next);

        EqualsVerifier
                .forClass(ChainElement.class)
                .usingGetClass()
                .withPrefabValues(ChainElement.class, previous, next)
                .withIgnoredFields("previous", "next")
                .verify();
    }

}
