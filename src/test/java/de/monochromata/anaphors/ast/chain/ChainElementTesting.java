package de.monochromata.anaphors.ast.chain;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public interface ChainElementTesting {

	default void assertSingleAnaphoraChain(final ASTBasedAnaphora anaphora, final TestableChainElement chainRoot) {
		assertThat(chainRoot.next).hasSize(1);
		final TestableChainElement chainLeaf = chainRoot.next.get(0);

		assertChainElement(chainRoot, null, singletonList(chainLeaf), anaphora.getRelatedExpressionPart(), null);
		assertChainElement(chainLeaf, chainRoot, emptyList(), null, anaphora.getAnaphorPart());
	}

	default void assertChainElement(final TestableChainElement current, final TestableChainElement previous,
			final List<TestableChainElement> next, final RelatedExpressionPart relatedExpressionPart,
			final AnaphorPart anaphorPart) {
		assertThat(current.previous).isEqualTo(previous);
		assertThat(current.next).isEqualTo(next);
		assertThat(current.relatedExpression).isSameAs(relatedExpressionPart);
		assertThat(current.anaphor).isSameAs(anaphorPart);
	}

}
