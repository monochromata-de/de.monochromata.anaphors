package de.monochromata.anaphors.ast;

import org.junit.jupiter.api.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

public class DefaultRelatedExpressionPartTest {

    @Test
    public void testEqualsContract() {
        EqualsVerifier.forClass(DefaultRelatedExpressionPart.class).usingGetClass().verify();
    }

}
