package de.monochromata.anaphors.ast.reference.strategy.feature;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;
import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;

public class FeatureRecurrenceWithNameRecurrenceTest extends AbstractReferentializationStrategyTest {

    public FeatureRecurrenceWithNameRecurrenceTest() {
        super("RfRn", new FeatureRecurrence<>(new NameRecurrence<>(null), null));
    }

}
