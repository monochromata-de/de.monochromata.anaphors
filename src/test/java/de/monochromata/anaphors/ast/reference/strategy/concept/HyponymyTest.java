package de.monochromata.anaphors.ast.reference.strategy.concept;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;
import nl.jqno.equalsverifier.api.SingleTypeEqualsVerifierApi;

public class HyponymyTest extends AbstractReferentializationStrategyTest implements HyponymyTesting {

    public HyponymyTest() {
        super("Hy",
                new Hyponymy<>(type -> ofNullable(type.getSuperclass()), type -> asList(type.getInterfaces()),
                        (final String id) -> (final Class type) -> type.getSimpleName().equalsIgnoreCase(id),
                        (final String id) -> (final Class type) -> {
                            throw new UnsupportedOperationException("Only tested via contract tests");
                        }));
    }

    @Override
    protected SingleTypeEqualsVerifierApi<? extends Strategy> createEqualsVerifier() {
        return super.createEqualsVerifier().withIgnoredFields("getSuperClass", "getImplementedInterfaces",
                "nameOfIdentifierEqualsSimpleNameOfTypeBinding",
                "conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding");
    }

    @Test
    public void referentTypeCannotBeUsedForReference() {
        final var potentialReferent = referentOfType(A.class);
        final boolean result = getStrategy().canReferTo("a", potentialReferent, null);
        assertThat(result).isFalse();
    }

    @Test
    public void directSuperClassOfReferentTypeCanBeUsedForReference() {
        final var potentialReferent = referentOfType(B.class);
        final boolean result = getStrategy().canReferTo("a", potentialReferent, null);
        assertThat(result).isTrue();
    }

    @Test
    public void indirectSuperClassOfReferentTypeCanBeUsedForReference() {
        final var potentialReferent = referentOfType(C.class);
        final boolean result = getStrategy().canReferTo("a", potentialReferent, null);
        assertThat(result).isTrue();
    }

    @Test
    public void unrelatedClassCannotBeUsedForReferenceWithManySuperclasses() {
        final var potentialReferent = referentOfType(C.class);
        final boolean result = getStrategy().canReferTo("d", potentialReferent, null);
        assertThat(result).isFalse();
    }

    @Test
    public void interfaceImplementedByReferentTypeCanBeUsedForReference() {
        final var potentialReferent = referentOfType(I1.class);
        final boolean result = getStrategy().canReferTo("interfaceA", potentialReferent, null);
        assertThat(result).isTrue();
    }

    @Test
    public void interfaceImplementedBySuperclassOfReferentTypeCanBeUsedForReference() {
        final var potentialReferent = referentOfType(I2.class);
        final boolean result = getStrategy().canReferTo("interfaceA", potentialReferent, null);
        assertThat(result).isTrue();
    }

    @Test
    public void interfaceImplementedByInterfaceImplementedByReferentTypeCanBeUsedForReference() {
        final var potentialReferent = referentOfType(I3.class);
        final var result = getStrategy().canReferTo("interfaceA", potentialReferent, null);
        assertThat(result).isTrue();
    }

    private static class A {
    }

    private static class B extends A {
    }

    private static class C extends B {
    }

    private static class I1 implements InterfaceA {
    }

    private static class I2 extends I1 {
    }

    private static class I3 implements InterfaceB {
    }

    private interface InterfaceA {
    }

    private interface InterfaceB extends InterfaceA {
    }
}
