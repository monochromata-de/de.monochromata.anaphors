package de.monochromata.anaphors.ast.reference.strategy.feature;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;
import de.monochromata.anaphors.ast.reference.strategy.concept.FauxHyponymy;

public class FeatureRecurrenceWithFauxHyponymyTest extends AbstractReferentializationStrategyTest {

    public FeatureRecurrenceWithFauxHyponymyTest() {
        super("RfHyFx", new FeatureRecurrence<>(new FauxHyponymy<>(null), null));
    }

}
