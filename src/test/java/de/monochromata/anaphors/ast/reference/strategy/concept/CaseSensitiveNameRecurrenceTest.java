package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;

public class CaseSensitiveNameRecurrenceTest extends AbstractReferentializationStrategyTest {

    public CaseSensitiveNameRecurrenceTest() {
        super("RnCS", new CaseSensitiveNameRecurrence<>(null));
    }

}
