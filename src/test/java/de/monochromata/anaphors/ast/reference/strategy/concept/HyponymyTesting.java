package de.monochromata.anaphors.ast.reference.strategy.concept;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.monochromata.anaphors.ast.reference.Referent;

public interface HyponymyTesting {

	default Referent referentOfType(final Class<?> type) {
		final var potentialReferent = mock(Referent.class);
		when(potentialReferent.resolveType(any())).thenReturn(type);
		return potentialReferent;
	}

}
