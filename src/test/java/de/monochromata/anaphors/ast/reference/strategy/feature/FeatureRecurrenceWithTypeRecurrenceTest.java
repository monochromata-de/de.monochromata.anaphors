package de.monochromata.anaphors.ast.reference.strategy.feature;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;
import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;

public class FeatureRecurrenceWithTypeRecurrenceTest extends AbstractReferentializationStrategyTest {

    public FeatureRecurrenceWithTypeRecurrenceTest() {
        super("RfRt", new FeatureRecurrence<>(new TypeRecurrence<>(null), null));
    }

}
