package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;

public class FauxHyponymyTest extends AbstractReferentializationStrategyTest {

    public FauxHyponymyTest() {
        super("HyFx", new FauxHyponymy<>(null));
    }

}
