package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;

public class TypeRecurrenceTest extends AbstractReferentializationStrategyTest {

    public TypeRecurrenceTest() {
        super("Rt", new TypeRecurrence<>(null));
    }

}
