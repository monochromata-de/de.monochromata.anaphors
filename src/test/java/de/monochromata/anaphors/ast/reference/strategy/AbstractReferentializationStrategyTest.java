package de.monochromata.anaphors.ast.reference.strategy;

import de.monochromata.AbstractStrategyTest;
import de.monochromata.Strategy;
import nl.jqno.equalsverifier.api.SingleTypeEqualsVerifierApi;

public abstract class AbstractReferentializationStrategyTest extends AbstractStrategyTest<ReferentializationStrategy> {

    public AbstractReferentializationStrategyTest(final String expectedKind,
            final ReferentializationStrategy strategy) {
        super(expectedKind, strategy);
    }

    @Override
    protected SingleTypeEqualsVerifierApi<? extends Strategy> createEqualsVerifier() {
        return super.createEqualsVerifier().withIgnoredFields("anaphorsSpi");
    }

}
