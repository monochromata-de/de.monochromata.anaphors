package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;

public class NameRecurrenceTest extends AbstractReferentializationStrategyTest {

    public NameRecurrenceTest() {
        super("Rn", new NameRecurrence<>(null));
    }

}
