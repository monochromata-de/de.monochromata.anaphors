package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategyTest;

public class CaseSensitiveTypeRecurrenceTest extends AbstractReferentializationStrategyTest {

    public CaseSensitiveTypeRecurrenceTest() {
        super("RtCS", new CaseSensitiveTypeRecurrence<>(null));
    }

}
