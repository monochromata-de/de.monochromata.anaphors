package de.monochromata.anaphors.perspectivation;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class PerspectivationTest {

    @Test
    public void negativeOffsetRaisesAssertionErrorForHide() {
        assertThatThrownBy(() -> new Perspectivation.Hide(-1, 1))
                .isInstanceOf(AssertionError.class)
                .hasMessage("relative offset >= 0");
    }

    @Test
    public void zeroOffsetDoesNotRaiseAssertionErrorForHide() {
        new Perspectivation.Hide(0, 1);
    }

    @Test
    public void negativeLengthRaisesAssertionErrorForHide() {
        assertThatThrownBy(() -> new Perspectivation.Hide(1, -11))
                .isInstanceOf(AssertionError.class)
                .hasMessage("length >= 0");
    }

    @Test
    public void zeroLengthDoesNotRaiseAssertionErrorForHide() {
        new Perspectivation.Hide(1, 0);
    }

    @Test
    public void negativeOffsetRaisesAssertionErrorForToLowerCase() {
        assertThatThrownBy(() -> new Perspectivation.ToLowerCase(-1))
                .isInstanceOf(AssertionError.class)
                .hasMessage("relative offset >= 0");
    }

    @Test
    public void zeroOffsetDoesNotRaiseAssertionErrorForToLowerCase() {
        new Perspectivation.ToLowerCase(0);
    }

}
