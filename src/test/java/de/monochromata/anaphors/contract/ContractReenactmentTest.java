package de.monochromata.anaphors.contract;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.fail;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import de.monochromata.contract.config.Configuration;
import de.monochromata.contract.environment.direct.provider.Verification;
import de.monochromata.contract.io.IOConfig;

// TODO: This can be a simple empty class, much like RunCucumberTest ... when a JUnit 5 extension is used
public class ContractReenactmentTest {

    IOConfig IO_CONFIG = new IOConfig() {
        {
            addTypeValidationCustomizer(builder -> builder.allowIfSubType("[Ljava.lang.Object;"));
        }
    };

    private final List<UnaryOperator<Class<?>>> proxyTypeTranslations = List.of(
            sourceType -> sourceType.getName().equals("java.util.Arrays$ArrayList") ? List.class : sourceType,
            sourceType -> sourceType.getName().equals("java.util.stream.ReferencePipeline$Head") ? Stream.class
                    : sourceType);
    Configuration CONFIG = new Configuration(IO_CONFIG, List.of(), List.of(), proxyTypeTranslations, List.of(),
            // TODO: Provider does not need interactionDescriptionContributor
            () -> null);

    // TODO: Move to java-contract-junit
    @Test
    public void reenactPacts() throws Exception {
        final boolean publishVerificationResults = "master".equals(System.getenv("CI_COMMIT_REF_NAME"));
        final var pactBrokerUrl = requireEnvironmentVariable("PACT_BROKER_BASE_URL");
        final var pactBrokerToken = requireEnvironmentVariable("PACT_BROKER_TOKEN");

        // TODO: Publishing results should be optional - only in CI, then the provider
        // version and buildUrl should be optional, too
        final var providerVersion = requireEnvironmentVariable("NEW_VERSION") + "-"
                + requireEnvironmentVariable("CI_COMMIT_SHORT_SHA");
        final var buildUrl = requireEnvironmentVariable("CI_JOB_URL");

        final var verificationErrors = Verification
                .verifyPacts(pactBrokerUrl, pactBrokerToken, "anaphors", providerVersion, "eclipse-anaphors", buildUrl,
                        CONFIG, publishVerificationResults)
                .values().stream().flatMap(results -> results.stream())
                .filter(resultAndMessage -> !resultAndMessage.getLeft().success).map(Pair::getRight).collect(toList());
        if (!verificationErrors.isEmpty()) {
            final var message = "Some pacts failed to verify:\n";
            final var pactDetails = verificationErrors.stream().collect(joining("\n"));
            fail(message + pactDetails);
        }
    }

    protected String requireEnvironmentVariable(final String variableName) {
        return requireNonNull(System.getenv(variableName), variableName + " is not set");
    }

}
