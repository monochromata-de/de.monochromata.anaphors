package de.monochromata.anaphors.perspectivation;

/**
 * A perspectivation relative to an offset in a document.
 * <p>
 * Conceptually, perspectivation is a process that turns a conceptual/semantic
 * representation into a textual representation - i.e. choosing words and
 * syntactic structures to express meaning. This class represents rather
 * low-level operations that are used during a perspectivation process to turn a
 * document that contains a conceptual/semantic representation that could also
 * be used as textual representation into a text optimized for human readers.
 * <p>
 * The offset of a perspectivation is relative to the offset of the positions it
 * belongs to. Since positions in a document are updated when the document is
 * changed, the perspectivation remains applicable even after document
 * modifications.
 *
 * @see Underspecification
 */
public abstract class Perspectivation {

    public final int originOffsetRelativeToPositionOffset;
    public final int length;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected Perspectivation() {
        originOffsetRelativeToPositionOffset = 0;
        length = 0;
    }

    /**
     * @param originOffsetRelativeToPositionOffset is relative to origin offsets
     *                                             because that does not require
     *                                             accounting for previously hidden
     *                                             regions. Must be {@code >= 0}.
     */
    public Perspectivation(final int originOffsetRelativeToPositionOffset, final int length) {
        assert originOffsetRelativeToPositionOffset >= 0 : "relative offset >= 0";
        assert length >= 0 : "length >= 0";
        this.originOffsetRelativeToPositionOffset = originOffsetRelativeToPositionOffset;
        this.length = length;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + length;
        result = prime * result + originOffsetRelativeToPositionOffset;
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Perspectivation other = (Perspectivation) obj;
        if (length != other.length) {
            return false;
        }
        if (originOffsetRelativeToPositionOffset != other.originOffsetRelativeToPositionOffset) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Perspectivation [originOffsetRelativeToPositionOffset=" + originOffsetRelativeToPositionOffset
                + ", length=" + length + "]";
    }

    public static class Hide extends Perspectivation {

        /**
         * @param originOffsetRelativeToPositionOffset uses origin offsets because that
         *                                             makes it easier to hide further
         *                                             regions without needing to
         *                                             account for previously hidden
         *                                             regions
         */
        public Hide(final int originOffsetRelativeToPositionOffset, final int length) {
            super(originOffsetRelativeToPositionOffset, length);
        }

        @Override
        public String toString() {
            return "Hide [originOffsetRelativeToPositionOffset=" + originOffsetRelativeToPositionOffset + ", length="
                    + length + "]";
        }

    }

    public static class ToLowerCase extends Perspectivation {

        /**
         * @param originOffsetRelativeToPositionOffset uses origin offsets because that
         *                                             does not require accounting for
         *                                             previously hidden regions
         */
        public ToLowerCase(final int originOffsetRelativeToPositionOffset) {
            super(originOffsetRelativeToPositionOffset, 1);
        }

        @Override
        public String toString() {
            return "ToLowerCase [originOffsetRelativeToPositionOffset=" + originOffsetRelativeToPositionOffset + "]";
        }

    }

}
