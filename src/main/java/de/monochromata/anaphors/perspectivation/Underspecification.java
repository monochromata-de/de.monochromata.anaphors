package de.monochromata.anaphors.perspectivation;

import static de.monochromata.anaphors.ast.AnaphorPartsStreaming.toVariableContentsAndAnaphors;

import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;
import de.monochromata.anaphors.ast.spi.AnaphorsSpi;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

/**
 * Underspecification is considered a process that is a kind of perspectivation.
 * It drops information while a conceptual-semantic representation is
 * transformed into a textual representation. It is intended to be used when
 * creating reader-specific textual representations that omit information that
 * is well-known to the reader.
 *
 * @see Perspectivation
 */
public interface Underspecification {

	/**
	 * @param condition Can be used to specify when to apply the perspectivations
	 *                  contained in the positions returned by this method. The
	 *                  condition will be contained in the positions. The condition
	 *                  is based on an event typically associated with a
	 *                  modification of a document to which perspectivations are
	 *                  applied. Because perspectivations modify a document, they
	 *                  can only be applied when the document is in a state (a
	 *                  precondition) that the perspectivations are based on.
	 * @param <N>       The node type in the AST
	 * @param <E>       The expression type
	 * @param <T>       The type type
	 * @param <B>       The binding type
	 * @param <MB>      The method binding type
	 * @param <TB>      The type binding type
	 * @param <S>       The scope type (optional)
	 * @param <I>       The type used to represent identifiers
	 * @param <QI>      The type used to represent qualified identifiers
	 * @param <EV>      The type of the event contained in the condition that is
	 *                  evaluated to check when the perspectivations shall be
	 *                  applied.
	 * @param <PP>      The type used for positions that carry perspectivations
	 * @param <R>       The sub-type of related expression to use
	 * @param <A>       The sub-type of AST-based anaphora to use
	 */
	static <N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> PP underspecifyRelatedExpression(
			final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
			final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> anaphorParts, final S scope,
			final Predicate<EV> condition,
			final RelatedExpressionsSpi<N, E, T, B, MB, TB, S, I, QI, EV, PP, R> relatedExpressionsSpi) {
		final R relatedExpression = relatedExpressionPart.getRelatedExpression();
		final var relatedExpressionStrategy = relatedExpressionPart.getRelatedExpressionStrategy();
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = toVariableContentsAndAnaphors(
				anaphorParts);
		final var perspectivations = relatedExpressionStrategy.underspecifyRelatedExpression(relatedExpression,
				variableContentsAndAnaphors, scope);
		return relatedExpressionsSpi.createPositionForNode(relatedExpression.getRelatedExpression(), condition,
				perspectivations);
	}

	/**
	 * @param condition Can be used to specify when to apply the perspectivations
	 *                  contained in the positions returned by this method. The
	 *                  condition will be contained in the positions. The condition
	 *                  is based on an event typically associated with a
	 *                  modification of a document to which perspectivations are
	 *                  applied. Because perspectivations modify a document, they
	 *                  can only be applied when the document is in a state (a
	 *                  precondition) that the perspectivations are based on.
	 * @param <N>       The node type in the AST
	 * @param <E>       The expression type
	 * @param <T>       The type type
	 * @param <B>       The binding type
	 * @param <TB>      The type binding type
	 * @param <S>       The scope type (optional)
	 * @param <I>       The type used to represent identifiers
	 * @param <QI>      The type used to represent qualified identifiers
	 * @param <EV>      The type of the event contained in the condition that is
	 *                  evaluated to check when the perspectivations shall be
	 *                  applied.
	 * @param <PP>      The type used for positions that carry perspectivations
	 * @param <R>       The sub-type of related expression to use
	 * @param <A>       The sub-type of AST-based anaphora to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> PP underspecifyAnaphor(
			final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope, final Predicate<EV> condition,
			final AnaphorsSpi<N, E, TB, S, I, QI, EV, PP> anaphorsSpi) {
		final R relatedExpression = relatedExpressionPart.getRelatedExpression();
		final String anaphor = anaphorPart.getAnaphor();
		final E anaphorExpression = anaphorPart.getAnaphorExpression();
		final var anaphorResolutionStrategy = anaphorPart.getAnaphorResolutionStrategy();
		final var perspectivations = anaphorResolutionStrategy.underspecifyAnaphor(relatedExpression, anaphor,
				anaphorExpression, anaphorPart.getReferent(), scope);
		return anaphorsSpi.createPositionForExpression(anaphorExpression, condition, perspectivations);
	}

}
