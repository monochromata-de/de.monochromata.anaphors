package de.monochromata.anaphors.perspectivation.strategy;

public interface PerspectivationStrategy {

    public PerspectivationConfiguration getPerspectivationConfiguration();

    static PerspectivationStrategy underspecifyEverything() {
        return () -> new PerspectivationConfiguration(true, true);
    }

    static PerspectivationStrategy underspecifyRelatedExpressions() {
        return () -> new PerspectivationConfiguration(true, false);
    }

    static PerspectivationStrategy underspecifyNothing() {
        return () -> new PerspectivationConfiguration(false, false);
    }
}
