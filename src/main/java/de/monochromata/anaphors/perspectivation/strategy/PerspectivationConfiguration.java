package de.monochromata.anaphors.perspectivation.strategy;

public class PerspectivationConfiguration {

    public final boolean underspecifyRelatedExpression;
    public final boolean underspecifyAnaphor;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected PerspectivationConfiguration() {
        underspecifyRelatedExpression = false;
        underspecifyAnaphor = false;
    }

    public PerspectivationConfiguration(final boolean underspecifyRelatedExpression,
            final boolean underspecifyAnaphor) {
        this.underspecifyRelatedExpression = underspecifyRelatedExpression;
        this.underspecifyAnaphor = underspecifyAnaphor;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (underspecifyAnaphor ? 1231 : 1237);
        result = prime * result + (underspecifyRelatedExpression ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerspectivationConfiguration other = (PerspectivationConfiguration) obj;
        if (underspecifyAnaphor != other.underspecifyAnaphor) {
            return false;
        }
        if (underspecifyRelatedExpression != other.underspecifyRelatedExpression) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "(" + underspecifyRelatedExpression + ", " + underspecifyAnaphor + ")";
    }

}
