/**
 * A service-provider interface to cognitive models of programming.
 */
package de.monochromata.anaphors.cog.spi;