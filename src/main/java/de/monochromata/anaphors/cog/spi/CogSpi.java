package de.monochromata.anaphors.cog.spi;

import de.monochromata.anaphors.cog.memory.Memory;

public class CogSpi {

	private static Memory<?> memory;

	@SuppressWarnings("unchecked")
	public static <E> Memory<E> getMemory() {
		return (Memory<E>) memory;
	}

	public static <E> void setMemory(final Memory<E> memory) {
		CogSpi.memory = memory;
	}

}
