package de.monochromata.anaphors.cog.memory;

import static java.lang.Long.MIN_VALUE;

import de.monochromata.anaphors.cog.activation.ActivationFormula;
import de.monochromata.anaphors.cog.activation.EstimatedActivationValue;

/**
 * @param <T> The type of object that this chunk represents.
 * @since 1.1.0
 */
public class DefaultChunk<T> implements Chunk<T> {

    private final ActivationFormula activationFormula;
    private final T represented;

    private EstimatedActivationValue lastActivation;
    private long lastActivationCalculatedForTimestamp = MIN_VALUE;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultChunk() {
        this(null, null);
    }

    public DefaultChunk(final ActivationFormula activationFormula, final T represented) {
        this.activationFormula = activationFormula;
        this.represented = represented;
    }

    @Override
    public synchronized EstimatedActivationValue getEstimatedActivationValue(final long timestamp) {
        if (lastActivation != null && lastActivationCalculatedForTimestamp == timestamp) {
            return lastActivation;
        }
        lastActivation = activationFormula.estimateActivation(this, timestamp);
        lastActivationCalculatedForTimestamp = timestamp;
        return lastActivation;
    }

    @Override
    public T getRepresented() {
        return represented;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((represented == null) ? 0 : represented.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultChunk<?> other = (DefaultChunk<?>) obj;
        if (represented == null) {
            if (other.represented != null) {
                return false;
            }
        } else if (!represented.equals(other.represented)) {
            return false;
        }
        return true;
    }

}
