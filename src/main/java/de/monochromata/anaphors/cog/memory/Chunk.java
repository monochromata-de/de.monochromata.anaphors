package de.monochromata.anaphors.cog.memory;

import de.monochromata.anaphors.cog.activation.Activatable;

/**
 * A chunk in memory.
 * 
 * @param <T>
 *            The type of object that this chunk represents.
 * @since 1.1.0
 */
public interface Chunk<T> extends Activatable {

	T getRepresented();

}
