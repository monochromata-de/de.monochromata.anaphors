/**
 * Abstract activation-based memory.
 * 
 * @since 1.1.0
 */
package de.monochromata.anaphors.cog.memory;