package de.monochromata.anaphors.cog.memory;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.LinkedHashSet;

import de.monochromata.anaphors.cog.activation.ActivationFormula;

/**
 * TODO: The size of the list should be limited
 *
 * @param <T> The type of objects that the chunks in this memory can represents.
 * @since 1.1.0
 */
public class SortedListMemory<T> implements Memory<T>, ModifiableMemory<T> {

    private final ActivationFormula activationFormula;
    private final LinkedHashSet<Chunk<T>> chunks;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected SortedListMemory() {
        this(null, null);
    }

    /**
     * Create a sorted set memory that sorts values based on the activation at the
     * time when {@link #getChunks(long, int)} is invoked.
     *
     * @see ActivationFormula#comparatorForActivationAt(long)
     */
    public SortedListMemory(final ActivationFormula activationFormula) {
        this(activationFormula, new LinkedHashSet<>());
    }

    public SortedListMemory(final ActivationFormula activationFormula, final LinkedHashSet<Chunk<T>> chunks) {
        this.activationFormula = activationFormula;
        this.chunks = chunks;
    }

    @Override
    public void add(final long timestamp, final Chunk<T> chunk) {
        // compute initial activation
        chunk.getEstimatedActivationValue(timestamp);
        this.chunks.add(chunk);
    }

    @Override
    public void addAll(final long timestamp, final Collection<Chunk<T>> chunks) {
        chunks.forEach(chunk -> add(timestamp, chunk));
    }

    @Override
    public Collection<Chunk<T>> getChunks(final long timestamp, final int maximumCount) {
        return chunks.stream().sorted(activationFormula.comparatorForActivationAt(timestamp)).limit(maximumCount)
                .collect(toList());
    }

}
