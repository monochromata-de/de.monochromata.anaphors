package de.monochromata.anaphors.cog.memory;

import java.util.Collection;

/**
 * A memory to which chunks can be added.
 * <p>
 * Note that chunks cannot be removed from memory explicitly as they might
 * become available later after they have been re-activated.
 * 
 * @param <T>
 *            The type of objects that the chunks in this memory can represents.
 * @since 1.1.0
 */
public interface ModifiableMemory<T> extends Memory<T> {

	/**
	 * @param timestamp
	 *            the point in time at which the addition occurs (in the format
	 *            returned by {@link System#currentTimeMillis()}
	 */
	public void add(final long timestamp, final Chunk<T> chunk);

	/**
	 * @param timestamp
	 *            the point in time at which the addition occurs (in the format
	 *            returned by {@link System#currentTimeMillis()}
	 */
	public void addAll(final long timestamp, final Collection<Chunk<T>> chunks);

}
