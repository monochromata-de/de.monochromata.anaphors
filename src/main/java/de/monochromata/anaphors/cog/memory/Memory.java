package de.monochromata.anaphors.cog.memory;

import java.util.Collection;

/**
 * An activation-based memory.
 *
 * @param <T>
 *            The type of objects that the chunks in this memory can represents.
 * @since 1.1.0
 */
public interface Memory<T> {

	/**
	 * Returns a limited number of chunks from memory after sorting the chunks
	 * in descending order by activation.
	 * <p>
	 * TODO: Maybe also add an accessor that limits by a maximum activation.
	 *
	 * @param timestamp
	 *            the point in time at which the access occurs (in the format
	 *            returned by {@link System#currentTimeMillis()}
	 * @param maximumCount
	 *            The maximum number of chunks to return.
	 * @return The collection containing up to {@code maximumCount} chunks
	 *         sorted, from highest to lowest activation.
	 */
	Collection<Chunk<T>> getChunks(final long timestamp, final int maximumCount);

}
