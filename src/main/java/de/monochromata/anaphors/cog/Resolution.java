package de.monochromata.anaphors.cog;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.cog.transform.CheckResult;
import de.monochromata.anaphors.cog.transform.PreparatoryTransformation;

/**
 * Used to represent (potential) resolutions of anaphors based on a cognitive
 * model.
 * <p>
 * Anaphor resolution may require a preparatory AST transformation. Anaphor
 * resolution eventually happens by obtaining an AST-based
 * {@link ASTBasedAnaphora} relation that can be underspecified.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface Resolution<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> {

	/**
	 * The check result is obtained from the {@link PreparatoryTransformation} and
	 * can be used to actually perform the preparatory transformation.
	 *
	 * @return a check result with {@link CheckResult#canPerformTransformation()}
	 *         returning {@literal true}.
	 * @see PreparatoryTransformation#canPerform(de.monochromata.anaphors.cog.memory.Chunk,
	 *      Object, Object)
	 * @see PreparatoryTransformation#perform(CheckResult, ASTBasedAnaphora)
	 * @see #getPreparatoryTransformation()
	 */
	public CheckResult<N, E, S> getCheckResult();

	/**
	 * @return a preparatory AST transformation that yields the AST-based
	 *         {@link ASTBasedAnaphora} relation.
	 * @see #getPreliminaryAnaphora()
	 * @see PreparatoryTransformation#perform(CheckResult, ASTBasedAnaphora)
	 * @see #getCheckResult()
	 */
	public PreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> getPreparatoryTransformation();

	/**
	 * The preliminary anaphora might be used to signal to users which anaphora
	 * relations are available, e.g. in case of referential ambiguity.
	 *
	 * @return a preliminary anaphora relation comparable to the one that could be
	 *         re-resolved after the preparatory transformation has been applied.
	 * @see #getPreparatoryTransformation()
	 */
	public A getPreliminaryAnaphora();

}
