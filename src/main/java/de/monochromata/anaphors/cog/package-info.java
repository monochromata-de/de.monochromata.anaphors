/**
 * Resolution of anaphors based on a model from cognitive linguistics.
 * <p>
 * The results of the application of this model needs to be transferred into the
 * model implemented in {@link de.monochromata.anaphors.ast} in order to apply
 * it to source code.
 */
package de.monochromata.anaphors.cog;