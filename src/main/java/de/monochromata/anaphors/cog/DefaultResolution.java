package de.monochromata.anaphors.cog;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.cog.transform.CheckResult;
import de.monochromata.anaphors.cog.transform.PreparatoryTransformation;

/**
 * A default resolution implementation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class DefaultResolution<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        implements Resolution<N, E, T, B, TB, S, I, QI, R, A> {

    private final CheckResult<N, E, S> checkResult;
    private final PreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> preparatoryTransformation;
    private final A preliminaryAnaphora;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultResolution() {
        this(null, null, null);
    }

    public DefaultResolution(final CheckResult<N, E, S> checkResult,
            final PreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> preparatoryTransformation,
            final A preliminaryAnaphora) {
        this.checkResult = checkResult;
        this.preparatoryTransformation = preparatoryTransformation;
        this.preliminaryAnaphora = preliminaryAnaphora;
    }

    @Override
    public CheckResult<N, E, S> getCheckResult() {
        return checkResult;
    }

    @Override
    public PreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> getPreparatoryTransformation() {
        return preparatoryTransformation;
    }

    @Override
    public A getPreliminaryAnaphora() {
        return preliminaryAnaphora;
    }

}
