package de.monochromata.anaphors.cog.activation;

/**
 * An entity that can have an activation value calculated by an activation
 * formula.
 * 
 * <p>
 * The activation of an activatable can be computed for arbitary points in time,
 * as defined in {@link ActivationFormula}.
 * </p>
 * 
 * @since 1.0.146
 */
public interface Activatable {

	/**
	 * Returns the estimated activation value of this activatable object at the
	 * given timestamp.
	 * <p>
	 * If this method is invoked multiple times with the same timestamp, it
	 * might return the same {@link EstimatedActivationValue} or another
	 * instance that is equal to the previous ones (i.e.
	 * {@code newResult.compareTo(oldResult) == 0}).
	 * 
	 * @param timestamp
	 *            A timestamp as returned by {@link System#currentTimeMillis()}.
	 * @return The activation value at the given timestamp.
	 */
	public EstimatedActivationValue getEstimatedActivationValue(long timestamp);

}
