package de.monochromata.anaphors.cog.activation;

import static java.util.Comparator.comparing;

import java.util.Comparator;

/**
 * Computes activation values for activatable entites at arbitrary points in
 * time with an implementation-dependent precision.
 *
 * <p>
 * Limitations to the computation of activation values are
 * implementation-specific. Precision of activation might be reduced for
 * timestamps that are hard to calculate or estimate. Such cases include time
 * stamps that are very old or reach very far into the future.
 * </p>
 * <p>
 * E.g. activation values before the current model time might be cached. Because
 * only a limited number of activation values might be cached, a rough guess
 * might be returned for very old timestamps. Timestamps in the remote future
 * come with similar implications: uncertainty of the interpolations/forecasts
 * might be very high for very remote future timestamps.
 * </p>
 *
 * <p>
 * TODO: Maybe explicitly model the limits within which activation values can be
 * estimated.
 * </p>
 */
public interface ActivationFormula {

    public EstimatedActivationValue estimateActivation(final Activatable activatable, final long timestamp);

    /**
     * @return a negative integer, zero, or a positive integer as the activation of
     *         the first activatable at the given time is less than, equal to, or
     *         greater than the activation of the second activatable at that point
     *         in time.
     */
    default <T extends Activatable> Comparator<T> comparatorForActivationAt(final long timestamp) {
        return comparing(activatable -> activatable.getEstimatedActivationValue(timestamp));
    }

}
