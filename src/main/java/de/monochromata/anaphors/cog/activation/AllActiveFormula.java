package de.monochromata.anaphors.cog.activation;

/**
 * An activation formula in which all {@link Activatable}s are always active to
 * the same extent.
 * 
 * @since 1.0.146
 */
public class AllActiveFormula implements ActivationFormula {

	private static final Active ACTIVE = new Active();

	@Override
	public Active estimateActivation(final Activatable activatable, final long timestamp) {
		return ACTIVE;
	}

	public static class Active implements EstimatedActivationValue {

		@Override
		public int compareTo(final EstimatedActivationValue o) {
			return 0;
		}

		@Override
		public boolean equals(final Object obj) {
			return obj != null && obj.getClass() == getClass();
		}

		@Override
		public int hashCode() {
			return 0;
		}

	}
}
