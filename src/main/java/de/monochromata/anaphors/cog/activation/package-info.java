/**
 * A generic API for estimating activation values in humans with implementations
 * of simple activation formulas.
 */
package de.monochromata.anaphors.cog.activation;