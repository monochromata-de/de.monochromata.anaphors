package de.monochromata.anaphors.cog.activation;

/**
 * An abstract interface for activation values in humans estimated by a
 * cognitive model.
 * 
 * @since 1.0.146
 */
public interface EstimatedActivationValue extends Comparable<EstimatedActivationValue> {

}
