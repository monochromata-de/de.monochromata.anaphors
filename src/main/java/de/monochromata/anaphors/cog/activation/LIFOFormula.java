package de.monochromata.anaphors.cog.activation;

import static java.lang.Long.compare;

import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A simplistic memory-less activation that decreases activation for existing
 * elements whenever a new element is created and never increments activation.
 */
@Deprecated
public class LIFOFormula implements ActivationFormula {

    private final AtomicLong counter = new AtomicLong();
    private final WeakHashMap<Activatable, Long> positions = new WeakHashMap<>();

    @Override
    public LIFOActivation estimateActivation(final Activatable activatable, final long timestamp) {
        final Long existingPosition = positions.get(activatable);
        if (existingPosition == null) {
            return createAndSaveNewPosition(activatable);
        }
        return new LIFOActivation(existingPosition);
    }

    protected LIFOActivation createAndSaveNewPosition(final Activatable activatable) {
        final long newPosition = counter.getAndIncrement();
        positions.put(activatable, newPosition);
        return new LIFOActivation(newPosition);
    }

    public static class LIFOActivation implements EstimatedActivationValue {

        private final long position;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected LIFOActivation() {
            this(0l);
        }

        public LIFOActivation(final long position) {
            this.position = position;
        }

        @Override
        public int compareTo(final EstimatedActivationValue other) {
            if (other.getClass() != getClass()) {
                throw new IllegalArgumentException("Invalid argument type: " + other.getClass());
            }
            return -compare(position, ((LIFOActivation) other).position);
        }

        public long getPosition() {
            return position;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (int) (position ^ (position >>> 32));
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final LIFOActivation other = (LIFOActivation) obj;
            if (position != other.position) {
                return false;
            }
            return true;
        }

    }

}
