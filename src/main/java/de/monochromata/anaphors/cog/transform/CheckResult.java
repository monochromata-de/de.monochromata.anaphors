package de.monochromata.anaphors.cog.transform;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.cog.memory.Chunk;

/**
 * An interface for the results of preparatory transformations.
 * <p>
 * Implementations of
 * {@link PreparatoryTransformation#canPerform(de.monochromata.anaphors.cog.memory.Chunk, Object, Object)}
 * may return implementations that add information that can be re-used in
 * {@link PreparatoryTransformation#perform(CheckResult, ASTBasedAnaphora)} , e.g. when
 * the check includes an expensive search operation whose results are supplied
 * to {@link PreparatoryTransformation#perform(CheckResult, ASTBasedAnaphora)} .
 *
 * @param <N>
 *            The node type in the AST
 * @param <E>
 *            The expression type
 * @param <S>
 *            The scope type (optional)
 *
 * @see PreparatoryTransformation#canPerform(de.monochromata.anaphors.cog.memory.Chunk,
 *      Object, Object)
 * @see PreparatoryTransformation#perform(CheckResult, ASTBasedAnaphora)
 */
public interface CheckResult<N, E, S> {

	public Chunk<N> getChunk();

	public E getDefiniteExpression();

	public S getScope();

	public boolean canPerformTransformation();

}
