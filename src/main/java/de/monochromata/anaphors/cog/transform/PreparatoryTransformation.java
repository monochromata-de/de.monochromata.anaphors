package de.monochromata.anaphors.cog.transform;

import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.cog.memory.Chunk;

/**
 * A transformation used to prepare the introduction of an
 * {@link ASTBasedAnaphora} relation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface PreparatoryTransformation<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
		extends Strategy {

	/**
	 * Check whether the transformation can be performed.
	 * <p>
	 * Note that the applicability of preparatory transformations must be mutually
	 * exclusive: only one out of all preparatory transformations can be applicable
	 * to a combination of chunk, definite expression and scope.
	 *
	 * @param chunk              The chunk that contains the node that may function
	 *                           as related expression.
	 * @param definiteExpression The definite expression that can potentially
	 *                           function as anaphor.
	 * @param scope              The scope containing the definite expression. May
	 *                           be null if not required by the AST implementation
	 *                           configured via the service provider interfaces.
	 * @return the result of a check that might contain (hidden)
	 *         implementation-specific cached information to speed up
	 *         {@link #perform(CheckResult, ASTBasedAnaphora)}.
	 */
	public CheckResult<N, E, S> canPerform(final Chunk<N> chunk, final E definiteExpression, final S scope);

	/**
	 * Perform the preparatory transformation and return a potential Anaphora
	 * relation thereafter.
	 *
	 * @param checkResult         the result of an invocation of check that might
	 *                            contain (hidden) implementation-specific cached
	 *                            information to speed up
	 *                            {@link #perform(CheckResult, ASTBasedAnaphora)}
	 * @param preliminaryAnaphora a preliminary anaphora relation that will be used
	 *                            as a blue-print to create the potential anaphora.
	 *                            (The preliminary anaphora is based on the AST
	 *                            before the preparatory transformation - its
	 *                            related expression might be unreachable from its
	 *                            the definite expression.)
	 * @return a potential anaphora relation that in valid in the AST after the
	 *         preparatory transformation has been applied.
	 * @throws IllegalArgumentException If the given check result has not been
	 *                                  created by this strategy or if the given
	 *                                  check result's
	 *                                  {@link CheckResult#canPerformTransformation()}
	 *                                  returns false.
	 */
	public A perform(final CheckResult<N, E, S> checkResult, final A preliminaryAnaphora);

}
