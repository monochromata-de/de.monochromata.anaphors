package de.monochromata.anaphors.cog.transform;

import de.monochromata.anaphors.cog.memory.Chunk;

/**
 * @param <N> The node type in the AST
 * @param <E> The expression type
 * @param <S> The scope type (optional)
 */
public abstract class AbstractCheckResult<N, E, S> implements CheckResult<N, E, S> {

    private final Chunk<N> chunk;
    private final E definiteExpression;
    private final S scope;
    private final boolean canPerformTransformation;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractCheckResult() {
        this(null, null, null, false);
    }

    public AbstractCheckResult(final Chunk<N> chunk, final E definiteExpression, final S scope,
            final boolean canPerformTransformation) {
        this.chunk = chunk;
        this.definiteExpression = definiteExpression;
        this.scope = scope;
        this.canPerformTransformation = canPerformTransformation;
    }

    @Override
    public Chunk<N> getChunk() {
        return chunk;
    }

    @Override
    public E getDefiniteExpression() {
        return definiteExpression;
    }

    @Override
    public S getScope() {
        return scope;
    }

    @Override
    public boolean canPerformTransformation() {
        return canPerformTransformation;
    }

}
