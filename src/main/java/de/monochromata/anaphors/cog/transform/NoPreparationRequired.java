package de.monochromata.anaphors.cog.transform;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.TransformationsSpi;
import de.monochromata.anaphors.cog.memory.Chunk;

/**
 * A no-operation preparatory transformation that merely returns a given
 * {@link ASTBasedAnaphora} relation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class NoPreparationRequired<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractPreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected NoPreparationRequired() {
    }

    public NoPreparationRequired(final TransformationsSpi<N, E, T, B, TB, S, I, QI, R, A> transformationsSpi) {
        super(transformationsSpi);
    }

    @Override
    public String getKind() {
        return "NOP";
    }

    @Override
    public CheckResult<N, E, S> canPerform(final Chunk<N> chunk, final E definiteExpression, final S scope) {
        final boolean canPerformTransformation = transformationsSpi.partOfSameInvocable(chunk.getRepresented(),
                definiteExpression);
        return new NopCheckResult(chunk, definiteExpression, scope, canPerformTransformation);
    }

    @Override
    @SuppressWarnings("unchecked")
    public A perform(final CheckResult<N, E, S> result, final A preliminaryAnaphora) {
        requireSuccessfulCheck(result);
        requireInstanceOf(result, NopCheckResult.class);
        return preliminaryAnaphora;
    }

    private class NopCheckResult extends AbstractCheckResult<N, E, S> {

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected NopCheckResult() {
        }

        public NopCheckResult(final Chunk<N> chunk, final E definiteExpression, final S scope,
                final boolean canPerformTransformation) {
            super(chunk, definiteExpression, scope, canPerformTransformation);
        }

    }

}
