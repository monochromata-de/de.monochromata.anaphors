/**
 * Complex transformations to prepare resolution of activation-based anaphors.
 */
package de.monochromata.anaphors.cog.transform;