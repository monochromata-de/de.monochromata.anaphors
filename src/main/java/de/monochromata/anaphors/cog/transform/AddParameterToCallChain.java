package de.monochromata.anaphors.cog.transform;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.TransformationsSpi;
import de.monochromata.anaphors.ast.transform.ASTTransformation;
import de.monochromata.anaphors.cog.memory.Chunk;

/**
 * If the given definite expression is reachable via the call chain from the
 * invocable containing the potential related expression, add a parameter for it
 * to propagate it to the invocable containing the definite expression.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class AddParameterToCallChain<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractPreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AddParameterToCallChain() {
    }

    public AddParameterToCallChain(final TransformationsSpi<N, E, T, B, TB, S, I, QI, R, A> transformationsSpi) {
        super(transformationsSpi);
    }

    @Override
    public String getKind() {
        return "AddParam";
    }

    @Override
    public CheckResult<N, E, S> canPerform(final Chunk<N> chunk, final E definiteExpression, final S scope) {
        final ASTTransformation<N, E, T, B, TB, S, I, QI, R, A> astTransformation = transformationsSpi
                .passAlongCallChain(chunk.getRepresented(), definiteExpression, scope);
        return new AddParamCheckResult(chunk, definiteExpression, scope, astTransformation);
    }

    @Override
    @SuppressWarnings("unchecked")
    public A perform(final CheckResult<N, E, S> result, final A preliminaryAnaphora) {
        requireSuccessfulCheck(result);
        requireInstanceOf(result, AddParamCheckResult.class);
        return ((AddParamCheckResult) result).astTransformation.perform(preliminaryAnaphora);
    }

    private class AddParamCheckResult extends AbstractCheckResult<N, E, S> {

        private final ASTTransformation<N, E, T, B, TB, S, I, QI, R, A> astTransformation;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected AddParamCheckResult() {
            astTransformation = null;
        }

        public AddParamCheckResult(final Chunk<N> chunk, final E definiteExpression, final S scope,
                final ASTTransformation<N, E, T, B, TB, S, I, QI, R, A> astTransformation) {
            super(chunk, definiteExpression, scope, astTransformation != null);
            this.astTransformation = astTransformation;
        }

    }

}
