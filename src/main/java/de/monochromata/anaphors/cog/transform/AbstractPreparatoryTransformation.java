package de.monochromata.anaphors.cog.transform;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.spi.TransformationsSpi;

/**
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractPreparatoryTransformation<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        implements PreparatoryTransformation<N, E, T, B, TB, S, I, QI, R, A> {

    protected final TransformationsSpi<N, E, T, B, TB, S, I, QI, R, A> transformationsSpi;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractPreparatoryTransformation() {
        this(null);
    }

    public AbstractPreparatoryTransformation(
            final TransformationsSpi<N, E, T, B, TB, S, I, QI, R, A> transformationsSpi) {
        this.transformationsSpi = transformationsSpi;
    }

    protected void requireSuccessfulCheck(final CheckResult<N, E, S> result) {
        if (!result.canPerformTransformation()) {
            throw new IllegalArgumentException("Only results of successful checks may be passed");
        }
    }

    protected <C extends CheckResult<N, E, S>> void requireInstanceOf(final CheckResult<N, E, S> result,
            final Class<C> clazz) {
        if (!clazz.isInstance(result)) {
            throw new IllegalArgumentException("Instance of " + clazz.getName() + " required");
        }
    }

}
