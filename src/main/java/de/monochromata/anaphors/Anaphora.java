package de.monochromata.anaphors;

import java.io.Serializable;

import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;

public class Anaphora implements Serializable {

    private static final long serialVersionUID = -3898185327820196066L;
    public final String kindOfRelatedExpression;
    public final String kindOfAnaphorResolution;
    public final String kindOfReferentialization;
    public final String kind;
    public final String anaphor;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected Anaphora() {
        this(null, null, null, null);
    }

    public Anaphora(final RelatedExpressionPart<?, ?, ?, ?, ?, ?, ?, ?, ?> relatedExpressionPart,
            final AnaphorPart<?, ?, ?, ?, ?, ?, ?, ?, ?, ?> anaphorPart) {
        this(relatedExpressionPart.getRelatedExpressionStrategy().getKind(),
                anaphorPart.getAnaphorResolutionStrategy().getKind(),
                anaphorPart.getReferentializationStrategy().getKind(), anaphorPart.getAnaphor());
    }

    public Anaphora(final String kindOfRelatedExpression, final String kindOfAnaphorResolution,
            final String kindOfReferentialization, final String anaphor) {
        this.kindOfRelatedExpression = kindOfRelatedExpression;
        this.kindOfAnaphorResolution = kindOfAnaphorResolution;
        this.kindOfReferentialization = kindOfReferentialization;
        kind = kindOfRelatedExpression + "-" + kindOfAnaphorResolution + "-" + kindOfReferentialization;
        this.anaphor = anaphor;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anaphor == null) ? 0 : anaphor.hashCode());
        result = prime * result + ((kind == null) ? 0 : kind.hashCode());
        result = prime * result + ((kindOfAnaphorResolution == null) ? 0 : kindOfAnaphorResolution.hashCode());
        result = prime * result + ((kindOfReferentialization == null) ? 0 : kindOfReferentialization.hashCode());
        result = prime * result + ((kindOfRelatedExpression == null) ? 0 : kindOfRelatedExpression.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Anaphora other = (Anaphora) obj;
        if (anaphor == null) {
            if (other.anaphor != null) {
                return false;
            }
        } else if (!anaphor.equals(other.anaphor)) {
            return false;
        }
        if (kind == null) {
            if (other.kind != null) {
                return false;
            }
        } else if (!kind.equals(other.kind)) {
            return false;
        }
        if (kindOfAnaphorResolution == null) {
            if (other.kindOfAnaphorResolution != null) {
                return false;
            }
        } else if (!kindOfAnaphorResolution.equals(other.kindOfAnaphorResolution)) {
            return false;
        }
        if (kindOfReferentialization == null) {
            if (other.kindOfReferentialization != null) {
                return false;
            }
        } else if (!kindOfReferentialization.equals(other.kindOfReferentialization)) {
            return false;
        }
        if (kindOfRelatedExpression == null) {
            if (other.kindOfRelatedExpression != null) {
                return false;
            }
        } else if (!kindOfRelatedExpression.equals(other.kindOfRelatedExpression)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Anaphora [kind=" + kind + ", anaphor=" + anaphor + "]";
    }

}
