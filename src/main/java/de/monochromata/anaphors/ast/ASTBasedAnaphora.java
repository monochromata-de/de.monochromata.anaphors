package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

/**
 * The relation between anaphor and related expression. Because instances of
 * this type contain AST nodes, they should be short-lived and must not be
 * persisted.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface ASTBasedAnaphora<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
		extends RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R>, AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> {

	/**
	 * Returns an identifier of the kind of anaphora relationship in the format
	 * <code>&lt;relatedExpressionStrategy&gt;-&lt;anaphorResolutionStrategy&gt;-&lt;referentializationStrategy&gt;</code>
	 *
	 * @return An identifier of the kind of anaphora relationship
	 * @see RelatedExpressionStrategy#getKind()
	 * @see AnaphorResolutionStrategy#getKind()
	 * @see ReferentializationStrategy#getKind()
	 */
	default String getKind() {
		return KindComposition.getKind(this, this);
	}

	public RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> getRelatedExpressionPart();

	public AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> getAnaphorPart();

	@Override
	default R getRelatedExpression() {
		return getRelatedExpressionPart().getRelatedExpression();
	}

	@Override
	default RelatedExpressionStrategy<N, T, B, TB, S, QI, R> getRelatedExpressionStrategy() {
		return getRelatedExpressionPart().getRelatedExpressionStrategy();
	}

	@Override
	default String getAnaphor() {
		return getAnaphorPart().getAnaphor();
	}

	@Override
	default E getAnaphorExpression() {
		return getAnaphorPart().getAnaphorExpression();
	}

	@Override
	default Referent<TB, S, I, QI> getReferent() {
		return getAnaphorPart().getReferent();
	}

	@Override
	default AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> getAnaphorResolutionStrategy() {
		return getAnaphorPart().getAnaphorResolutionStrategy();
	}

	@Override
	default ReferentializationStrategy<E, TB, S, I, QI> getReferentializationStrategy() {
		return getAnaphorPart().getReferentializationStrategy();
	}

	@Override
	default B getBinding() {
		return getAnaphorPart().getBinding();
	}

	@Override
	default TB resolveType(final S scope) {
		return getAnaphorPart().resolveType(scope);
	}

	/**
	 * Returns a description of the anaphora relation, to enable users to
	 * distinguish multiple anaphora relations that are referentially ambiguous.
	 * <p>
	 * The reference description does not contain information about the anaphor, but
	 * only information on the related expression and the referent.
	 */
	public String getReferenceDescription();

	/**
	 * @return {@literal true} if this anaphora relation is underspecified, i.e. is
	 *         not explicated.
	 * @see #isExplicated()
	 * @deprecated The state of an anaphor should be represented internally in an
	 *             implementation-specific way. This method will be removed in the
	 *             future.
	 */
	@Deprecated
	public boolean isUnderspecified();

	/**
	 * @return {@literal true} if this anaphora relation is explicated, i.e. is not
	 *         underspecified.
	 * @see #isUnderspecified()
	 * @deprecated The state of an anaphor should be represented internally in an
	 *             implementation-specific way. This method will be removed in the
	 *             future.
	 */
	@Deprecated
	public boolean isExplicated();
}
