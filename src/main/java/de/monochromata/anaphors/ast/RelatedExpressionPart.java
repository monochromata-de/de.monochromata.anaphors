package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;

/**
 * Represents a related expression at the AST level. Because instances of this
 * type contain AST nodes, they should be short-lived and must not be persisted.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 */
public interface RelatedExpressionPart<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>> {

	/**
	 * Get the related expression of the anaphora relation.
	 *
	 * @return The related expression.
	 */
	public R getRelatedExpression();

	/**
	 * Get the related expression strategy that was used to find the related
	 * expression of this anaphora relation.
	 *
	 * @return The related expression strategy.
	 */
	public RelatedExpressionStrategy<N, T, B, TB, S, QI, R> getRelatedExpressionStrategy();

}
