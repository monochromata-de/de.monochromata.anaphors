package de.monochromata.anaphors.ast.strategy;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.anaphors.ast.DefaultIndirectAnaphora;
import de.monochromata.anaphors.ast.DefaultRelatedExpressionPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.reference.AbstractReferent;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.IA2FStrategy.IA2FReferent;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.anaphors.perspectivation.Perspectivation.Hide;
import de.monochromata.anaphors.perspectivation.Perspectivation.ToLowerCase;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An indirect-anaphora resolution strategy that anchors anaphors in a getter
 * method declared by the anchor.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class IA2MgStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractAnchoringStrategy<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>
        implements StoresAnchorInLocalTempVariable<N, E, T, B, TB, S, I, QI, R, A> {

    public static final List<Class<? extends RelatedExpressionStrategy>> IA2Mg_SUPPORTED_RELATED_EXPRESSION_STRATEGIES = asList(
            ClassInstanceCreationStrategy.class, ParameterDeclarationStrategy.class,
            LocalVariableDeclarationStrategy.class);

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected IA2MgStrategy() {
    }

    public IA2MgStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        super(IA2Mg_SUPPORTED_RELATED_EXPRESSION_STRATEGIES, anaphorsApi, relatedExpressionsApi, anaphoraResolutionApi);
    }

    @Override
    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final S scope, final R potentialRelatedExpression) {
        // TODO: Maybe use existing type binding, if it has been resolved
        // previously
        return createPotentialReferents(potentialRelatedExpression, scope,
                potentialRelatedExpression.resolveType(scope));
    }

    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final R relatedExpression, final S scope,
            final TB typeBinding) {
        final List<Referent<TB, S, I, QI>> potentialReferents = new ArrayList<>();
        final List<MB> methodBindings = anaphoraResolutionApi.getAccessibleGetterMethods(typeBinding, scope);
        for (final MB methodBinding : methodBindings) {
            final I methodName = anaphoraResolutionApi.getMethodName(methodBinding);
            final String methodDescription = anaphoraResolutionApi.getMethodDescription(methodBinding);
            potentialReferents.add(new IA2MgReferent<>(relatedExpression, methodBinding, methodName, methodDescription,
                    scope, relatedExpressionsApi, anaphoraResolutionApi));
        }
        return potentialReferents;
    }

    @Override
    public Referent<TB, S, I, QI> createReferent(final S scope, final R relatedExpression, final Object memento) {
        return createReferent(relatedExpression, scope, relatedExpression.resolveType(scope), memento);
    }

    protected Referent<TB, S, I, QI> createReferent(final R relatedExpression, final S scope, final TB typeBinding,
            final Object memento) {
        final List<MB> methodBindings = anaphoraResolutionApi.getAccessibleGetterMethods(typeBinding, scope);
        for (final MB methodBinding : methodBindings) {
            final I methodName = anaphoraResolutionApi.getMethodName(methodBinding);
            final String methodDescription = anaphoraResolutionApi.getMethodDescription(methodBinding);
            if (methodName.equals(memento)) {
                return new IA2MgReferent<>(relatedExpression, methodBinding, methodName, methodDescription, scope,
                        relatedExpressionsApi, anaphoraResolutionApi);
            }
        }
        throw new IllegalArgumentException("Failed to re-create referent");
    }

    @Override
    public A createAnaphora(final S scope, final String anaphor, final E anaphorExpression,
            final R potentialRelatedExpression, final Referent<TB, S, I, QI> potentialReferent,
            final ReferentializationStrategy<E, TB, S, I, QI> refStrategy) {
        final DefaultRelatedExpressionPart<N, E, T, B, VB, TB, S, I, QI, R> relatedExpressionPart = new DefaultRelatedExpressionPart<>(
                potentialRelatedExpression);
        final DefaultAnaphorPart<N, E, T, B, VB, TB, S, I, QI, R, A> anaphorPart = new DefaultAnaphorPart<>(anaphor,
                anaphorExpression, potentialReferent, this, refStrategy,
                ((IA2MgReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getMethodBinding());
        final String underspecifiedRelation = anaphoraResolutionApi.getIA2MgUnderspecifiedRelation(
                potentialRelatedExpression,
                ((IA2MgReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getMethodBinding(),
                scope);
        return new DefaultIndirectAnaphora(relatedExpressionPart, anaphorPart, underspecifiedRelation, false);
    }

    @Override
    public E realize(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final E replacee,
            final Optional<I> guessedTempName, final Object... support) {
        return anaphoraResolutionApi.realizeIA2Mg(relatedExpressionPart, anaphorPart, replacee, guessedTempName,
                support);
    }

    @Override
    public String getKind() {
        return "IA2Mg";
    }

    @Override
    public List<Perspectivation> underspecifyAnaphor(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final Referent<TB, S, I, QI> referent, final S scope) {
        return underspecifyAnaphorForIA2Mg(relatedExpression, anaphor, anaphorExpression, referent, scope);
    }

    protected List<Perspectivation> underspecifyAnaphorForIA2Mg(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final Referent<TB, S, I, QI> referent, final S scope) {
        final QI qualifier = getQualifierForIA(relatedExpression, anaphor, scope);
        final int qualifierLength = anaphorsApi.getLength(qualifier) + ".".length();
        final I methodName = referent.getMethodName();
        final int lengthOfMethodName = relatedExpressionsApi.getLength(methodName);
        return asList(new Hide(0, qualifierLength), new Hide(qualifierLength, "get".length()),
                new Hide(qualifierLength + lengthOfMethodName, 2),
                // Always add ToLowerCase because
                // AnaphoraResolutionSpi.getAccessibleGetterMethods(...) only returns methods
                // that have an upper-case character following the "get-" prefix.
                new ToLowerCase(qualifierLength + "get".length()));
    }

    @Override
    public String getAnaphorToBeRealized(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> allAnaphorPartsRelatedToTheRelatedExpression,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope) {
        final var referent = (IA2MgReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) anaphorPart
                .getReferent();
        final I methodName = referent.getMethodName();
        final String rawAnaphor = relatedExpressionsApi.identifierToString(methodName);
        return getAnaphorToBeRealized(rawAnaphor);
    }

    protected String getAnaphorToBeRealized(final String rawAnaphor) {
        if (!rawAnaphor.startsWith("get")) {
            throw new IllegalArgumentException("Missing get- prefix in method name " + rawAnaphor);
        }
        if (rawAnaphor.length() == 3) {
            throw new IllegalArgumentException("Method name contains get- prefix only");
        }
        if (!Character.isUpperCase(rawAnaphor.charAt(3))) {
            throw new IllegalArgumentException(
                    "Method name must continue in upper case after get- prefix: " + rawAnaphor);
        }
        final char firstChar = Character.toLowerCase(rawAnaphor.charAt("get".length()));
        final String suffix = rawAnaphor.length() >= ("get".length() + 1) ? rawAnaphor.substring("get".length() + 1)
                : "";
        return firstChar + suffix;
    }

    public static class IA2MgReferent<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
            extends AbstractReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, R, A> {

        final MB methodBinding;
        final I methodName;
        final S scope;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected IA2MgReferent() {
            methodBinding = null;
            methodName = null;
            scope = null;
        }

        IA2MgReferent(final R relatedExpression, final MB methodBinding, final I methodName,
                final String methodDescription, final S scope,
                final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
                final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi) {
            super(relatedExpression, methodDescription, relatedExpressionsApi, anaphorResolutionApi);
            this.methodBinding = methodBinding;
            this.methodName = methodName;
            this.scope = scope;
            // TODO: add features for the method name, parameters and return
            // types?
        }

        public MB getMethodBinding() {
            return methodBinding;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean canBeUsedInsteadOf(final Referent<TB, S, I, QI> other) {
            if (!(other instanceof IA2FStrategy.IA2FReferent)) {
                return false;
            } else {
                final IA2FStrategy.IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A> ia2fReferent = (IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) other;
                final FB fieldBinding = ia2fReferent.getFieldBinding();
                return anaphorResolutionApi.doesGetterMethodOnlyReturnValueOf(methodBinding, fieldBinding, scope);
            }
        }

        @Override
        public boolean hasName() {
            return true;
        }

        @Override
        public QI getName() {
            return anaphorResolutionApi.getReferentName(this.methodBinding);
        }

        @Override
        public boolean hasMethodName() {
            return true;
        }

        @Override
        public I getMethodName() {
            return this.methodName;
        }

        @Override
        public TB resolveType(final S scope) {
            return anaphorResolutionApi.resolveReturnType(this.methodBinding, scope);
        }

        @Override
        public Object getMemento() {
            return this.methodName;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = prime * result + ((this.methodBinding == null) ? 0 : this.methodBinding.hashCode());
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final IA2MgReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A> other = (IA2MgReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) obj;
            if (this.methodBinding == null) {
                if (other.methodBinding != null) {
                    return false;
                }
            } else if (!this.methodBinding.equals(other.methodBinding)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            // The method binding is not output because it can be quite long.
            return "IA2MgReferent [methodName=" + methodName + ", getDescription()=" + getDescription()
                    + ", getFeatures()=" + getFeatures() + "]";
        }

    }
}
