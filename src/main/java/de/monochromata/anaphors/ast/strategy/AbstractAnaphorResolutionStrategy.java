package de.monochromata.anaphors.ast.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import de.monochromata.AbstractStrategy;
import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An abstract base class for strategies used to for resolve or construct the
 * referents of anaphors.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractAnaphorResolutionStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractStrategy implements AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> {

    protected final List<Class<? extends RelatedExpressionStrategy>> supportedRelatedExpressionStrategies;
    protected final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi;
    protected final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi;
    protected final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractAnaphorResolutionStrategy() {
        this(null, null, null, null);
    }

    protected AbstractAnaphorResolutionStrategy(
            final List<Class<? extends RelatedExpressionStrategy>> supportedRelatedExpressionStrategies,
            final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        this.supportedRelatedExpressionStrategies = supportedRelatedExpressionStrategies;
        this.anaphorsApi = anaphorsApi;
        this.relatedExpressionsApi = relatedExpressionsApi;
        this.anaphoraResolutionApi = anaphoraResolutionApi;
    }

    @Override
    public boolean canRelateTo(
            final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> potentialRelatedExpressionStrategy) {
        return supportedRelatedExpressionStrategies.stream()
                .anyMatch(supportedType -> supportedType.isInstance(potentialRelatedExpressionStrategy));
    }

    @Override
    public List<A> generatePotentialAnaphora(final S scope, final String anaphor, final E definiteExpression,
            final List<R> potentialRelatedExpressions,
            final List<ReferentializationStrategy<E, TB, S, I, QI>> refStrategies) {
        final List<A> anaphoras = new ArrayList<>();
        return getIdForDefiniteExpression(definiteExpression)
                .map(idFromDefiniteExpression -> generatePotentialAnaphora(scope, anaphor, definiteExpression,
                        potentialRelatedExpressions, refStrategies, anaphoras, idFromDefiniteExpression))
                .orElseGet(Collections::emptyList);
    }

    protected List<A> generatePotentialAnaphora(final S scope, final String anaphor, final E definiteExpression,
            final List<R> potentialRelatedExpressions,
            final List<ReferentializationStrategy<E, TB, S, I, QI>> refStrategies, final List<A> anaphoras,
            final I idFromDefiniteExpression) {
        // Create anaphora relations for all related expressions
        // for which at least one referentialization strategy can be applied.
        for (final R potentialRelatedExpression : potentialRelatedExpressions) {
            if (canRelateTo(potentialRelatedExpression.getStrategy())) {
                final List<Referent<TB, S, I, QI>> potentialReferents = createPotentialReferents(scope,
                        potentialRelatedExpression);
                for (final Referent<TB, S, I, QI> potentialReferent : potentialReferents) {
                    for (final ReferentializationStrategy<E, TB, S, I, QI> refStrategy : refStrategies) {
                        if (refStrategy.canReferTo(idFromDefiniteExpression, potentialReferent, scope)) {
                            anaphoras.add(createAnaphora(scope, anaphor, definiteExpression, potentialRelatedExpression,
                                    potentialReferent, refStrategy));
                            break;
                        }
                    }
                }
            }
            // TODO: Assuming that there is no precedence / hiding
        }

        return anaphoras;
    }

    protected Optional<I> getIdForDefiniteExpression(final E definiteExpression) {
        if (anaphorsApi.isSimpleName(definiteExpression)) {
            // cannot resolve type again, since that would trigger anaphor
            // resolution, too
            // TODO: Maybe use another approach that also checks visibility etc.
            return Optional.of(anaphorsApi.getIdentifierOfSimpleName(definiteExpression));
        } else if (couldBeAPreviousRealization(definiteExpression)) {
            return Optional.of(getIdForPreviousRealization(definiteExpression));
        }
        return Optional.empty();
    }

    /**
     * @return {@literal true} if the given definite expression has been or could
     *         have been created by this or any other
     *         {@link AnaphorResolutionStrategy}.
     * @see #getIdForPreviousRealization(Object)
     */
    protected boolean couldBeAPreviousRealization(final E definiteExpression) {
        return anaphoraResolutionApi.couldBeAPreviousRealization(definiteExpression);
    }

    /**
     * Should only be invoked if {@link #couldBeAPreviousRealization(Object)}
     * returned {@literal true}.
     *
     * @see #couldBeAPreviousRealization(Object)
     */
    protected I getIdForPreviousRealization(final E definiteExpression) {
        return anaphoraResolutionApi.getIdFromPreviousRealization(definiteExpression);
    }

    protected abstract List<Referent<TB, S, I, QI>> createPotentialReferents(S scope, R potentialRelatedExpression);

    protected void requireThisAnaphorResolutionStrategy(final A anaphora) {
        if (anaphora.getAnaphorResolutionStrategy() != this) {
            throw new IllegalArgumentException("The given anaphora relation was not created by this strategy");
        }
    }
}
