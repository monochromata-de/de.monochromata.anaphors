package de.monochromata.anaphors.ast.strategy;

import java.util.List;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An abstract base class for strategies used to for resolve or construct the
 * referents of indirect anaphors.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractAnchoringStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractAnaphorResolutionStrategy<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>
        implements AnchoringStrategy<N, E, T, B, TB, S, I, QI, R, A> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractAnchoringStrategy() {
    }

    public AbstractAnchoringStrategy(
            final List<Class<? extends RelatedExpressionStrategy>> supportedRelatedExpressionStrategies,
            final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        super(supportedRelatedExpressionStrategies, anaphorsApi, relatedExpressionsApi, anaphoraResolutionApi);
    }

    protected QI getQualifierForIA(final R relatedExpression, final String anaphor, final S scope) {
        // TODO: More tests are required
        if (relatedExpression.shouldResolutionReplaceRelatedExpressionWithTempDeclaration()) {
            // the name obtained from the related expression is used as identifier of the
            // new temporary variable
            final I tempName = relatedExpressionsApi.guessTempName(relatedExpression, anaphor,
                    getLocalTempVariableContents(), scope);
            return relatedExpressionsApi.toQualifiedIdentifier(tempName);
        }
        // TODO: Add a test
        if (!relatedExpression.hasName()) {
            // TODO: Create a name
            throw new IllegalStateException(
                    "Related expression has no name: " + relatedExpression.getRelatedExpression());
        }
        return relatedExpression.getName();
    }

}
