package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy.MI_KIND;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.anaphors.ast.DefaultIndirectAnaphora;
import de.monochromata.anaphors.ast.DefaultRelatedExpressionPart;
import de.monochromata.anaphors.ast.KindComposition;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.reference.AbstractReferent;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An indirect-anaphora resolution strategy that anchors anaphors in the return
 * value of a method invocation that acts as the anchor.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class IA1MrStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractAnchoringStrategy<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>
        implements StoresReferentInLocalTempVariable<N, E, T, B, TB, S, I, QI, R, A> {

    public static final List<Class<? extends RelatedExpressionStrategy>> IA1Mr_SUPPORTED_RELATED_EXPRESSION_STRATEGIES = asList(
            MethodInvocationStrategy.class);

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected IA1MrStrategy() {
    }

    public IA1MrStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        super(IA1Mr_SUPPORTED_RELATED_EXPRESSION_STRATEGIES, anaphorsApi, relatedExpressionsApi, anaphoraResolutionApi);
    }

    @Override
    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final S scope, final R potentialRelatedExpression) {
        if (!MI_KIND.equals(potentialRelatedExpression.getStrategy().getKind())) {
            throw new IllegalArgumentException(
                    "Cannot apply " + getKind() + " to " + potentialRelatedExpression.getStrategy().getKind());
        }
        // TODO: Maybe use existing method binding, if it has been resolved previously
        final MB methodBinding = relatedExpressionsApi
                .resolveMethodInvocationBinding(potentialRelatedExpression.getRelatedExpression(), scope);
        if (methodBinding == null) {
            return emptyList();
        }
        // If the return type cannot be resolved, later processing will fail.
        if (null == potentialRelatedExpression.resolveType(scope)) {
            return emptyList();
        }
        return createPotentialReferents(potentialRelatedExpression, scope, methodBinding);
    }

    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final R relatedExpression, final S scope,
            final MB methodBinding) {
        final I methodName = anaphoraResolutionApi.getMethodName(methodBinding);
        final String methodDescription = anaphoraResolutionApi.getMethodDescription(methodBinding);
        return singletonList(new IA1MrReferent<>(relatedExpression, methodBinding, methodName, methodDescription, scope,
                relatedExpressionsApi, anaphoraResolutionApi));
    }

    @Override
    public Referent<TB, S, I, QI> createReferent(final S scope, final R relatedExpression, final Object memento) {
        final MB methodBinding = relatedExpressionsApi
                .resolveMethodInvocationBinding(relatedExpression.getRelatedExpression(), scope);
        return createReferent(relatedExpression, scope, methodBinding, memento);
    }

    protected Referent<TB, S, I, QI> createReferent(final R relatedExpression, final S scope, final MB methodBinding,
            final Object memento) {
        final I methodName = anaphoraResolutionApi.getMethodName(methodBinding);
        final String methodDescription = anaphoraResolutionApi.getMethodDescription(methodBinding);
        if (methodName.equals(memento)) {
            return new IA1MrReferent<>(relatedExpression, methodBinding, methodName, methodDescription, scope,
                    relatedExpressionsApi, anaphoraResolutionApi);
        }
        throw new IllegalArgumentException("Failed to re-create referent");
    }

    @Override
    public A createAnaphora(final S scope, final String anaphor, final E anaphorExpression,
            final R potentialRelatedExpression, final Referent<TB, S, I, QI> potentialReferent,
            final ReferentializationStrategy<E, TB, S, I, QI> refStrategy) {
        final DefaultRelatedExpressionPart<N, E, T, B, VB, TB, S, I, QI, R> relatedExpressionPart = new DefaultRelatedExpressionPart<>(
                potentialRelatedExpression);
        final DefaultAnaphorPart<N, E, T, B, VB, TB, S, I, QI, R, A> anaphorPart = new DefaultAnaphorPart<>(anaphor,
                anaphorExpression, potentialReferent, this, refStrategy,
                ((IA1MrReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getMethodBinding());
        final String underspecifiedRelation = anaphoraResolutionApi.getIA1MrUnderspecifiedRelation(
                potentialRelatedExpression,
                ((IA1MrReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getMethodBinding(),
                scope);
        final Supplier<String> descriptionSupplier = getShortenedReferenceDescription(potentialRelatedExpression,
                potentialReferent, scope, refStrategy);
        return new DefaultIndirectAnaphora(relatedExpressionPart, anaphorPart, underspecifiedRelation,
                descriptionSupplier, false);
    }

    protected Supplier<String> getShortenedReferenceDescription(final R relatedExpression,
            final Referent<TB, S, I, QI> referent, final S scope,
            final ReferentializationStrategy<E, TB, S, I, QI> refStrategy) {
        return () -> {
            final String returnType = relatedExpressionsApi
                    .identifierToString(relatedExpressionsApi.getIdentifier(referent.resolveType(scope)));
            final String kind = KindComposition.getKind(relatedExpression.getStrategy(), this, refStrategy);
            return relatedExpression.getRelatedExpression() + ":" + returnType + " at " + relatedExpression.getLine()
                    + ":" + relatedExpression.getColumn() + " (" + kind + ")";
        };
    }

    @Override
    public E realize(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final E replacee,
            final Optional<I> guessedTempName, final Object... support) {
        return anaphoraResolutionApi.realizeIA1Mr(relatedExpressionPart, anaphorPart, replacee, guessedTempName,
                support);
    }

    @Override
    public String getKind() {
        return "IA1Mr";
    }

    @Override
    public List<Perspectivation> underspecifyAnaphor(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final Referent<TB, S, I, QI> referent, final S scope) {
        // No perspectivation necessary
        return emptyList();
    }

    @Override
    public String getAnaphorToBeRealized(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> allAnaphorPartsRelatedToTheRelatedExpression,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope) {
        return AnaphorCreationForReferentInLocalTempVariable.getAnaphorToBeRealized(relatedExpressionPart,
                allAnaphorPartsRelatedToTheRelatedExpression, anaphorPart, scope, relatedExpressionsApi);
    }

    public static class IA1MrReferent<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
            extends AbstractReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, R, A> {

        final MB methodBinding;
        final I methodName;
        final S scope;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected IA1MrReferent() {
            methodBinding = null;
            methodName = null;
            scope = null;
        }

        IA1MrReferent(final R relatedExpression, final MB methodBinding, final I methodName,
                final String methodDescription, final S scope,
                final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
                final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi) {
            super(relatedExpression, methodDescription, relatedExpressionsApi, anaphorResolutionApi);
            this.methodBinding = methodBinding;
            this.methodName = methodName;
            this.scope = scope;
            // TODO: add features for the method name, parameters and return
            // types?
        }

        public MB getMethodBinding() {
            return this.methodBinding;
        }

        @Override
        public boolean canBeUsedInsteadOf(final Referent<TB, S, I, QI> other) {
            return false;
        }

        @Override
        public boolean hasName() {
            return true;
        }

        @Override
        public QI getName() {
            return anaphorResolutionApi.getReferentName(this.methodBinding);
        }

        @Override
        public boolean hasMethodName() {
            return true;
        }

        @Override
        public I getMethodName() {
            return methodName;
        }

        @Override
        public TB resolveType(final S scope) {
            return anaphorResolutionApi.resolveReturnType(this.methodBinding, scope);
        }

        @Override
        public Object getMemento() {
            return methodName;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = prime * result + ((methodBinding == null) ? 0 : methodBinding.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final IA1MrReferent other = (IA1MrReferent) obj;
            if (methodBinding == null) {
                if (other.methodBinding != null) {
                    return false;
                }
            } else if (!methodBinding.equals(other.methodBinding)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            // The method binding is not output because it can be quite long.
            return "IA1MrReferent [methodName=" + methodName + ", getDescription()=" + getDescription()
                    + ", getFeatures()=" + getFeatures() + "]";
        }
    }

}
