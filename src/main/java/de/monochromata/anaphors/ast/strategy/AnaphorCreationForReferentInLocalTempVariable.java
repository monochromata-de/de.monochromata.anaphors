package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy.CIC_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy.MI_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy.PD_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy.LVD_KIND;

import java.util.List;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

public interface AnaphorCreationForReferentInLocalTempVariable {

	static <N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> String getAnaphorToBeRealized(
			final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
			final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> allAnaphorPartsRelatedToTheRelatedExpression,
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope,
			final RelatedExpressionsSpi<N, E, T, B, MB, TB, S, I, QI, EV, PP, R> relatedExpressionsSpi) {
		final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> reStrategy = relatedExpressionPart
				.getRelatedExpressionStrategy();
		final String kindOfRelatedExpression = reStrategy.getKind();
		switch (kindOfRelatedExpression) {
		case CIC_KIND:
		case MI_KIND:
			final I identifierForTempVariable = ((LocalTempVariableIntroducingStrategy<N, E, T, B, TB, S, I, QI, R, A>) reStrategy)
					.getIdentifierForTempVariable(relatedExpressionPart, allAnaphorPartsRelatedToTheRelatedExpression,
							scope);
			return relatedExpressionsSpi.identifierToString(identifierForTempVariable);
		case PD_KIND:
		case LVD_KIND:
			final QI relatedExpressionName = relatedExpressionPart.getRelatedExpression().getName();
			return relatedExpressionsSpi.qualifiedIdentifierToString(relatedExpressionName);
		default:
			throw new IllegalArgumentException("Unknown related expression strategy: " + reStrategy.getKind());
		}
	}

}
