package de.monochromata.anaphors.ast.strategy;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.anaphors.ast.DefaultIndirectAnaphora;
import de.monochromata.anaphors.ast.DefaultRelatedExpressionPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.reference.AbstractReferent;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.anaphors.perspectivation.Perspectivation.Hide;
import de.monochromata.anaphors.perspectivation.Perspectivation.ToLowerCase;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An indirect-anaphora resolution strategy that anchors anaphors in a field
 * declared by the anchor.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class IA2FStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractAnchoringStrategy<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>
        implements StoresAnchorInLocalTempVariable<N, E, T, B, TB, S, I, QI, R, A> {

    public static final List<Class<? extends RelatedExpressionStrategy>> IA2F_SUPPORTED_RELATED_EXPRESSION_STRATEGIES = asList(
            ClassInstanceCreationStrategy.class, ParameterDeclarationStrategy.class,
            LocalVariableDeclarationStrategy.class);

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected IA2FStrategy() {
    }

    public IA2FStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        super(IA2F_SUPPORTED_RELATED_EXPRESSION_STRATEGIES, anaphorsApi, relatedExpressionsApi, anaphoraResolutionApi);
    }

    @Override
    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final S scope, final R potentialRelatedExpression) {
        // TODO: Maybe use existing type binding, if it has been resolved
        // previously
        return createPotentialReferents(potentialRelatedExpression, scope,
                potentialRelatedExpression.resolveType(scope));
    }

    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final R relatedExpression, final S scope,
            final TB typeBinding) {
        final List<Referent<TB, S, I, QI>> potentialReferents = new ArrayList<>();
        final List<FB> fieldBindings = anaphoraResolutionApi.getAccessibleFields(typeBinding, scope);
        for (final FB fieldBinding : fieldBindings) {
            final I fieldName = anaphoraResolutionApi.getFieldName(fieldBinding);
            final QI qualifiedFieldName = relatedExpressionsApi.toQualifiedIdentifier(fieldName);
            final String fieldDescription = anaphoraResolutionApi.getFieldDescription(fieldBinding);
            potentialReferents.add(new IA2FReferent<>(relatedExpression, fieldBinding, qualifiedFieldName,
                    fieldDescription, relatedExpressionsApi, anaphoraResolutionApi));
        }
        return potentialReferents;
    }

    @Override
    public Referent<TB, S, I, QI> createReferent(final S scope, final R relatedExpression, final Object memento) {
        return createReferent(relatedExpression, scope, relatedExpression.resolveType(scope), memento);
    }

    protected Referent<TB, S, I, QI> createReferent(final R relatedExpression, final S scope, final TB typeBinding,
            final Object memento) {
        final List<FB> fieldBindings = anaphoraResolutionApi.getAccessibleFields(typeBinding, scope);
        for (final FB fieldBinding : fieldBindings) {
            final I fieldName = anaphoraResolutionApi.getFieldName(fieldBinding);
            final QI qualifiedFieldName = relatedExpressionsApi.toQualifiedIdentifier(fieldName);
            final String fieldDescription = anaphoraResolutionApi.getFieldDescription(fieldBinding);
            if (fieldName.equals(memento)) {
                return new IA2FReferent<>(relatedExpression, fieldBinding, qualifiedFieldName, fieldDescription,
                        relatedExpressionsApi, anaphoraResolutionApi);
            }
        }
        throw new IllegalArgumentException("Could not re-create referent from memento " + memento);
    }

    @SuppressWarnings("unchecked")
    @Override
    public A createAnaphora(final S scope, final String anaphor, final E anaphorExpression,
            final R potentialRelatedExpression, final Referent<TB, S, I, QI> potentialReferent,
            final ReferentializationStrategy<E, TB, S, I, QI> refStrategy) {
        final DefaultRelatedExpressionPart<N, E, T, B, VB, TB, S, I, QI, R> relatedExpressionPart = new DefaultRelatedExpressionPart<>(
                potentialRelatedExpression);
        final DefaultAnaphorPart<N, E, T, B, VB, TB, S, I, QI, R, A> anaphorPart = new DefaultAnaphorPart<>(anaphor,
                anaphorExpression, potentialReferent, this, refStrategy,
                ((IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getFieldBinding());
        final String underspecifiedRelation = anaphoraResolutionApi.getIA2FUnderspecifiedRelation(
                potentialRelatedExpression,
                ((IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) potentialReferent)
                        .getFieldBinding(),
                scope);
        return new DefaultIndirectAnaphora(relatedExpressionPart, anaphorPart, underspecifiedRelation, false);
    }

    @Override
    public E realize(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final E replacee,
            final Optional<I> guessedTempName, final Object... support) {
        return anaphoraResolutionApi.realizeIA2F(relatedExpressionPart, anaphorPart, replacee, guessedTempName,
                support);
    }

    @Override
    public String getKind() {
        return "IA2F";
    }

    @Override
    public List<Perspectivation> underspecifyAnaphor(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final Referent<TB, S, I, QI> referent, final S scope) {
        return underspecifyAnaphorForIA2F(relatedExpression, anaphor, anaphorExpression,
                (IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) referent, scope);
    }

    protected List<Perspectivation> underspecifyAnaphorForIA2F(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A> referent,
            final S scope) {
        final QI qualifier = getQualifierForIA(relatedExpression, anaphor, scope);
        final int qualifierLength = anaphorsApi.getLength(qualifier) + ".".length();
        final I fieldName = anaphoraResolutionApi.getFieldName(referent.getFieldBinding());
        final boolean fieldNameStartsUpperCase = anaphoraResolutionApi.identifierStartsUpperCase(fieldName);
        return underspecifyAnaphorForIA2F(qualifierLength, fieldNameStartsUpperCase);
    }

    /**
     * Only public to ease testing. Should be moved to a mixin interface and be
     * hidden/separated using Java modules.
     */
    public List<Perspectivation> underspecifyAnaphorForIA2F(final int qualifierLength,
            final boolean fieldNameStartsUpperCase) {
        final List<Perspectivation> perspectivations = new ArrayList<>();
        perspectivations.add(new Hide(0, qualifierLength));
        if (fieldNameStartsUpperCase) {
            perspectivations.add(new ToLowerCase(qualifierLength));
        }
        return perspectivations;
    }

    @Override
    public String getAnaphorToBeRealized(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> allAnaphorPartsRelatedToTheRelatedExpression,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope) {
        final IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A> referent = ((IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) anaphorPart
                .getReferent());
        final I fieldName = anaphoraResolutionApi.getFieldName(referent.getFieldBinding());
        return getAnaphorToBeRealized(fieldName);
    }

    protected String getAnaphorToBeRealized(final I fieldName) {
        final String rawAnaphor = relatedExpressionsApi.identifierToString(fieldName);
        return getAnaphorToBeRealized(rawAnaphor);
    }

    protected String getAnaphorToBeRealized(final String rawAnaphor) {
        if (rawAnaphor.isEmpty()) {
            return rawAnaphor;
        }
        final String prefix = Character.toLowerCase(rawAnaphor.charAt(0)) + "";
        final String suffix = rawAnaphor.length() > 1 ? rawAnaphor.substring(1) : "";
        return prefix + suffix;
    }

    public static class IA2FReferent<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
            extends AbstractReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, R, A> {

        final FB fieldBinding;
        final QI fieldName;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected IA2FReferent() {
            fieldBinding = null;
            fieldName = null;
        }

        IA2FReferent(final R relatedExpression, final FB fieldBinding, final QI fieldName,
                final String fieldDescription,
                final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
                final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi) {
            super(relatedExpression, fieldDescription, relatedExpressionsApi, anaphorResolutionApi);
            this.fieldBinding = fieldBinding;
            this.fieldName = fieldName;
            // TODO: Add field name or binding as feature?
        }

        public FB getFieldBinding() {
            return this.fieldBinding;
        }

        @Override
        public boolean canBeUsedInsteadOf(final Referent<TB, S, I, QI> other) {
            // TODO: implement additional alias analysis
            return false;
        }

        @Override
        public boolean hasName() {
            return true;
        }

        @Override
        public QI getName() {
            return this.fieldName;
        }

        @Override
        public boolean hasMethodName() {
            return false;
        }

        @Override
        public I getMethodName() {
            throw new UnsupportedOperationException("IA2FReferent has no method name");
        }

        @Override
        public TB resolveType(final S scope) {
            return anaphorResolutionApi.resolveType(fieldBinding, scope);
        }

        @Override
        public Object getMemento() {
            return this.fieldName;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = prime * result + ((this.fieldBinding == null) ? 0 : this.fieldBinding.hashCode());
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A> other = (IA2FReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>) obj;
            if (this.fieldBinding == null) {
                if (other.fieldBinding != null) {
                    return false;
                }
            } else if (!this.fieldBinding.equals(other.fieldBinding)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            // The field binding is not output because it can be quite long.
            return "IA2FReferent [fieldName=" + fieldName + ", getDescription()=" + getDescription()
                    + ", getFeatures()=" + getFeatures() + "]";
        }

    }

}
