package de.monochromata.anaphors.ast.strategy;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Optional;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.DefaultAnaphorPart;
import de.monochromata.anaphors.ast.DefaultDirectAnaphora;
import de.monochromata.anaphors.ast.DefaultRelatedExpressionPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.reference.AbstractReferent;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.AnaphorsApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * A simple direct anaphora resolution strategy that is based on the various
 * forms of recurrence that can be implemented using the available
 * {@link ReferentializationStrategy} implementations.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class DA1ReStrategy<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractAnaphorResolutionStrategy<N, E, T, B, VB, FB, MB, TB, S, I, QI, EV, PP, R, A>
        implements StoresReferentInLocalTempVariable<N, E, T, B, TB, S, I, QI, R, A> {

    public static final String DA1Re_KIND = "DA1Re";
    public static final List<Class<? extends RelatedExpressionStrategy>> DA1Re_SUPPORTED_RELATED_EXPRESSION_STRATEGIES = asList(
            ClassInstanceCreationStrategy.class, ParameterDeclarationStrategy.class,
            LocalVariableDeclarationStrategy.class);

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DA1ReStrategy() {
    }

    public DA1ReStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphoraResolutionApi) {
        super(DA1Re_SUPPORTED_RELATED_EXPRESSION_STRATEGIES, anaphorsApi, relatedExpressionsApi, anaphoraResolutionApi);
    }

    @Override
    protected List<Referent<TB, S, I, QI>> createPotentialReferents(final S scope, final R potentialRelatedExpression) {
        return singletonList(createReferent(potentialRelatedExpression));
    }

    @Override
    public Referent<TB, S, I, QI> createReferent(final S scope, final R relatedExpression, final Object memento) {
        return createReferent(relatedExpression);
    }

    protected Referent<TB, S, I, QI> createReferent(final R relatedExpression) {
        return new DA1Referent<>(relatedExpression, relatedExpressionsApi, anaphoraResolutionApi);
    }

    @Override
    public A createAnaphora(final S scope, final String anaphor, final E anaphorExpression,
            final R potentialRelatedExpression, final Referent<TB, S, I, QI> potentialReferent,
            final ReferentializationStrategy<E, TB, S, I, QI> refStrategy) {
        final DefaultRelatedExpressionPart<N, E, T, B, VB, TB, S, I, QI, R> relatedExpressionPart = new DefaultRelatedExpressionPart<>(
                potentialRelatedExpression);
        final DefaultAnaphorPart<N, E, T, B, VB, TB, S, I, QI, R, A> anaphorPart = new DefaultAnaphorPart<>(anaphor,
                anaphorExpression, potentialReferent, this, refStrategy,
                potentialRelatedExpression.resolveNameBinding(scope));
        return new DefaultDirectAnaphora(relatedExpressionPart, anaphorPart, false);
    }

    @Override
    public E realize(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final E replacee,
            final Optional<I> guessedTempName, final Object... support) {
        return anaphoraResolutionApi.realizeDA1Re(relatedExpressionPart, anaphorPart, replacee, guessedTempName,
                support);
    }

    @Override
    public String getKind() {
        return DA1Re_KIND;
    }

    @Override
    public List<Perspectivation> underspecifyAnaphor(final R relatedExpression, final String anaphor,
            final E anaphorExpression, final Referent<TB, S, I, QI> referent, final S scope) {
        // No perspectivation necessary
        return emptyList();
    }

    @Override
    public String getAnaphorToBeRealized(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> allAnaphorPartsRelatedToTheRelatedExpression,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final S scope) {
        return AnaphorCreationForReferentInLocalTempVariable.getAnaphorToBeRealized(relatedExpressionPart,
                allAnaphorPartsRelatedToTheRelatedExpression, anaphorPart, scope, relatedExpressionsApi);
    }

    public static class DA1Referent<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
            extends AbstractReferent<N, E, T, B, VB, FB, MB, TB, S, I, QI, R, A> {

        private QI name;
        private TB typeBinding;

        /**
         * Used in contract testing.
         */
        @SuppressWarnings("unused")
        protected DA1Referent() {
        }

        public DA1Referent(final R relatedExpression,
                final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
                final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi) {
            super(relatedExpression, relatedExpression.getDescription(), relatedExpressionsApi, anaphorResolutionApi);
        }

        @Override
        public boolean canBeUsedInsteadOf(final Referent<TB, S, I, QI> other) {
            // TODO: improve alias analysis, maybe use
            // org.eclipse.jdt.internal.corext.refactoring.code.flow
            return false;
        }

        @Override
        public boolean hasName() {
            return getRelatedExpression().hasName();
        }

        @Override
        public QI getName() {
            return getRelatedExpression().getName();
        }

        @Override
        public boolean hasMethodName() {
            return false;
        }

        @Override
        public I getMethodName() {
            throw new UnsupportedOperationException("DA1Referent has no method name");
        }

        @Override
        public TB resolveType(final S scope) {
            if (this.typeBinding == null) {
                this.typeBinding = getRelatedExpression().resolveType(scope);
            }
            return this.typeBinding;
        }

        @Override
        public Object getMemento() {
            return "";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            result = prime * result + ((typeBinding == null) ? 0 : typeBinding.hashCode());
            return result;
        }

        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (!super.equals(obj)) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final DA1Referent other = (DA1Referent) obj;
            if (name == null) {
                if (other.name != null) {
                    return false;
                }
            } else if (!name.equals(other.name)) {
                return false;
            }
            if (typeBinding == null) {
                if (other.typeBinding != null) {
                    return false;
                }
            } else if (!typeBinding.equals(other.typeBinding)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            // The type binding is not output because it can be quite long.
            return "DA1Referent [name=" + name + ", getDescription()=" + getDescription() + ", getFeatures()="
                    + getFeatures() + "]";
        }

    }
}
