package de.monochromata.anaphors.ast.strategy;

import static de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence.Rn_KIND;
import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents.REFERENT;
import static de.monochromata.anaphors.ast.strategy.DA1ReStrategy.DA1Re_KIND;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;

/**
 * This is a {@link AnaphorResolutionStrategy} that has a related expression
 * that is or yields the referent. It uses {@link DA1ReStrategy} and
 * {@link NameRecurrence} for realization, because this strategy relates to the
 * referent which is stored in the introduced local temp variable, if such a
 * variable is introduced (depends on whether the
 * {@link RelatedExpressionStrategy} is a
 * {@link LocalTempVariableIntroducingStrategy}).
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface StoresReferentInLocalTempVariable<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
		extends AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> {

	@Override
	default LocalTempVariableContents getLocalTempVariableContents() {
		return REFERENT;
	}

	@Override
	default String getKindOfAnaphorResolutionStrategyToBeRealized(
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart) {
		return DA1Re_KIND;
	}

	/**
	 * Always returns {@link NameRecurrence#Rn_KIND} because re-resolution triggered
	 * after realization will always be name-based as it is based on the name of the
	 * local variable that was introduced to store the referent.
	 */
	@Override
	default String getKindOfReferentializationStrategyToBeRealized(
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart) {
		return Rn_KIND;
	}

}
