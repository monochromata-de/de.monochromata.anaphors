package de.monochromata.anaphors.ast.feature;

/**
 * An interface for a single feature.
 * <p>
 * It is assumed that features are discrete. Features may occur in conceptual
 * schemata (TODO: define) or in lexicon entries (TODO: define). They might be
 * derived from textual representations, but they do not occur in texts. Each
 * feature has an optional name that can be identical or similar to one of
 * potentially multiple textual representations of the feature. There may be
 * features that cannot be identified by name.
 * </p>
 * 
 * <p>
 * Each feature has an activation that might be computed in an
 * implementation-specific and theory-dependent manner.
 * </p>
 * 
 * <p>
 * TODO: Maybe use un-qualified identifiers?
 * </p>
 * 
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public interface Feature<QI> {

	public QI getName();
}
