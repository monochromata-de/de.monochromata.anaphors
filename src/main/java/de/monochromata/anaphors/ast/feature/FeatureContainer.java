package de.monochromata.anaphors.ast.feature;

import java.util.Set;

/**
 * A collection of features.
 * <p>
 * It is assumed that features can be enumerated, have no order or precedence
 * and that each feature container has a finite number of features.
 * </p>
 *
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public interface FeatureContainer<QI> {

	/**
	 * Provides access to the features of this container.
	 *
	 * <p>
	 * TODO: Maybe return a sorted set?
	 * </p>
	 *
	 * @return The set of features in the container. The set is not modifiable.
	 */
	public Set<Feature<QI>> getFeatures();

	/**
	 * Returns true, if this container contains no features.
	 *
	 * @return {@literal} true}, if {@link #getFeatures()} returns an empty set,
	 *         {@literal false} otherwise.
	 */
	public boolean isEmpty();

	/**
	 * Compares the features in this container to the features in the given
	 * container.
	 *
	 * @param other
	 *            another container whose features are to be compared to the
	 *            features in this container.
	 * @return {literal true}, if this container contains all features contained
	 *         in the given container, {@literal false} otherwise.
	 */
	public boolean containsFeaturesOf(FeatureContainer<QI> other);

}
