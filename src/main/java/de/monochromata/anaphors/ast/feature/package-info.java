/**
 * Interfaces and generic classes for feature-based semantics.
 */
package de.monochromata.anaphors.ast.feature;