package de.monochromata.anaphors.ast.feature;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public class DefaultFeatureContainer<QI> implements FeatureContainer<QI> {

	private final Set<Feature<QI>> features = new HashSet<Feature<QI>>();

	protected void add(final Feature<QI> feature) {
		features.add(feature);
	}

	protected void addAll(final Set<Feature<QI>> features) {
		this.features.addAll(features);
	}

	protected void remove(final Feature<QI> feature) {
		features.remove(feature);
	}

	protected void removeAll(final Set<Feature<QI>> features) {
		this.features.removeAll(features);
	}

	@Override
	public Set<Feature<QI>> getFeatures() {
		return Collections.unmodifiableSet(features);
	}

	@Override
	public boolean isEmpty() {
		return features.isEmpty();
	}

	@Override
	public boolean containsFeaturesOf(final FeatureContainer<QI> other) {
		return features.containsAll(other.getFeatures());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((features == null) ? 0 : features.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final DefaultFeatureContainer other = (DefaultFeatureContainer) obj;
		if (features == null) {
			if (other.features != null) {
				return false;
			}
		} else if (!features.equals(other.features)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AbstractFeatureContainer [features=" + features + "]";
	}

}
