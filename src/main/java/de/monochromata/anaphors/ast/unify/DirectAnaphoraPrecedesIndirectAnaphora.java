package de.monochromata.anaphors.ast.unify;

import java.util.ArrayList;
import java.util.List;

import de.monochromata.anaphors.ast.DirectAnaphora;

public interface DirectAnaphoraPrecedesIndirectAnaphora {

	static <A> List<A> preferDirectOverIndirectAnaphora(final List<A> potentialAnaphoraRelations) {
		final List<A> directAnaphoraRelations = new ArrayList<>();
		for (final A anaphora : potentialAnaphoraRelations) {
			if (anaphora instanceof DirectAnaphora) {
				directAnaphoraRelations.add(anaphora);
			}
		}
		return directAnaphoraRelations.isEmpty() ? potentialAnaphoraRelations : directAnaphoraRelations;
	}

}
