package de.monochromata.anaphors.ast.unify;

import static de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy.Hy_KIND;
import static de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence.Rt_KIND;
import static de.monochromata.anaphors.ast.unify.Unification.haveEqualReferent;
import static de.monochromata.anaphors.ast.unify.Unification.unifyListElements;

import java.util.List;
import java.util.function.BiPredicate;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

public interface TypeRecurrencePrecedesHyponymy {

	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> List<A> preferTypeRecurrenceOverHynonymy(
			final List<A> potentialAnaphoraRelations) {
		return unifyListElements(potentialAnaphoraRelations, preferRtOverHy());
	}

	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> BiPredicate<A, A> preferRtOverHy() {
		return (anaphora1, anaphora2) -> haveRtAndHy(anaphora1, anaphora2) && haveEqualReferent(anaphora1, anaphora2);
	}

	private static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> boolean haveRtAndHy(
			final A anaphora1, final A anaphora2) {
		return anaphora1.getReferentializationStrategy().getKind().equals(Rt_KIND)
				&& anaphora2.getReferentializationStrategy().getKind().equals(Hy_KIND);
	}

}
