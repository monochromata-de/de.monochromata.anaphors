package de.monochromata.anaphors.ast.unify;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

public interface Unification {

	static <N, T, B, TB extends B, S, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>> List<R> unifyCoReferentialRelatedExpressions(
			final List<R> potentialRelatedExpressions) {
		return unifyListElements(potentialRelatedExpressions, (re1, re2) -> re1.canBeUsedInsteadOf(re2));
	}

	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> List<A> unifyCoReferentialReferents(
			final List<A> potentialAnaphoraRelations) {
		return unifyListElements(potentialAnaphoraRelations,
				(anaphora1, anaphora2) -> anaphora1.getReferent().canBeUsedInsteadOf(anaphora2.getReferent()));
	}

	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> boolean haveEqualReferent(
			final A anaphora1, final A anaphora2) {
		return anaphora1.getReferent().equals(anaphora2.getReferent());
	}

	static <T> List<T> unifyListElements(final List<T> elements, final BiPredicate<T, T> check) {
		final List<T> retainedElements = new ArrayList<>();
		final List<T> omittedElements = new ArrayList<>();
		for (final T toCheck : elements) {
			if (!omittedElements.contains(toCheck)) {
				boolean retainedInsteadOfAnotherOne = false;
				for (final T other : elements) {
					if (toCheck != other && check.test(toCheck, other)) {
						if (!retainedInsteadOfAnotherOne) {
							retainedElements.add(toCheck);
							retainedInsteadOfAnotherOne = true;
						}
						System.err.println("Omitting " + other + " in favour of " + toCheck);
						omittedElements.add(other);
						retainedElements.remove(other);
					}
				}
				if (!retainedInsteadOfAnotherOne) {
					retainedElements.add(toCheck);
				}
			}
		}
		return retainedElements;
	}

}
