package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * A default implementation of a direct anaphora relation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class DefaultDirectAnaphora<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>
        implements DirectAnaphora<N, E, T, B, TB, S, I, QI, R, A> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultDirectAnaphora() {
    }

    public DefaultDirectAnaphora(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final boolean isUnderspecified) {
        super(relatedExpressionPart, anaphorPart, isUnderspecified);
    }

    @Override
    public String getReferenceDescription() {
        return getRelatedExpression().getDescription() + " at " + getRelatedExpression().getLine() + ":"
                + getRelatedExpression().getColumn() + " (" + getKind() + ")";
    }

    @Override
    public String toString() {
        return getKind() + " [getRelatedExpression()=" + getRelatedExpression() + ", getAnaphor()=" + getAnaphor()
                + ", getAnaphorExpression()=" + getAnaphorExpression() + ", getReferent()=" + getReferent()
                + ", getRelatedExpressionStrategy()=" + getRelatedExpressionStrategy()
                + ", getAnaphorResolutionStrategy()=" + getAnaphorResolutionStrategy()
                + ", getReferentializationStrategy()=" + getReferentializationStrategy() + ", getBinding()="
                + getBinding() + ", isUnderspecified()=" + isUnderspecified() + ", isExplicated()=" + isExplicated()
                + "]";
    }

}
