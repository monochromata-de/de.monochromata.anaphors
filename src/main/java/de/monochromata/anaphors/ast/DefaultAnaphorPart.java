package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

/**
 * An default implementation of the {@link AnaphorPart} interface.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class DefaultAnaphorPart<N, E, T, B, VB extends B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        implements AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> {

    private final String anaphor;
    private final E anaphorExpression;
    private final Referent<TB, S, I, QI> referent;
    private final AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> anaphorResolutionStrategy;
    private final ReferentializationStrategy<E, TB, S, I, QI> referentializationStrategy;
    private final B binding;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultAnaphorPart() {
        this(null, null, null, null, null, null);
    }

    public DefaultAnaphorPart(final String anaphor, final E anaphorExpression, final Referent<TB, S, I, QI> referent,
            final AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> anaphorResolutionStrategy,
            final ReferentializationStrategy<E, TB, S, I, QI> referentializationStrategy, final B binding) {
        this.anaphor = anaphor;
        this.anaphorExpression = anaphorExpression;
        this.referent = referent;
        this.anaphorResolutionStrategy = anaphorResolutionStrategy;
        this.referentializationStrategy = referentializationStrategy;
        this.binding = binding;
    }

    @Override
    public String getAnaphor() {
        return anaphor;
    }

    @Override
    public E getAnaphorExpression() {
        return this.anaphorExpression;
    }

    @Override
    public Referent<TB, S, I, QI> getReferent() {
        return this.referent;
    }

    @Override
    public AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> getAnaphorResolutionStrategy() {
        return this.anaphorResolutionStrategy;
    }

    @Override
    public ReferentializationStrategy<E, TB, S, I, QI> getReferentializationStrategy() {
        return this.referentializationStrategy;
    }

    @Override
    public B getBinding() {
        return this.binding;
    }

    @Override
    public TB resolveType(final S scope) {
        return this.referent.resolveType(scope);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anaphor == null) ? 0 : anaphor.hashCode());
        result = prime * result + ((anaphorExpression == null) ? 0 : anaphorExpression.hashCode());
        result = prime * result + ((anaphorResolutionStrategy == null) ? 0 : anaphorResolutionStrategy.hashCode());
        result = prime * result + ((binding == null) ? 0 : binding.hashCode());
        result = prime * result + ((referent == null) ? 0 : referent.hashCode());
        result = prime * result + ((referentializationStrategy == null) ? 0 : referentializationStrategy.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultAnaphorPart other = (DefaultAnaphorPart) obj;
        if (anaphor == null) {
            if (other.anaphor != null) {
                return false;
            }
        } else if (!anaphor.equals(other.anaphor)) {
            return false;
        }
        if (anaphorExpression == null) {
            if (other.anaphorExpression != null) {
                return false;
            }
        } else if (!anaphorExpression.equals(other.anaphorExpression)) {
            return false;
        }
        if (anaphorResolutionStrategy == null) {
            if (other.anaphorResolutionStrategy != null) {
                return false;
            }
        } else if (!anaphorResolutionStrategy.equals(other.anaphorResolutionStrategy)) {
            return false;
        }
        if (binding == null) {
            if (other.binding != null) {
                return false;
            }
        } else if (!binding.equals(other.binding)) {
            return false;
        }
        if (referent == null) {
            if (other.referent != null) {
                return false;
            }
        } else if (!referent.equals(other.referent)) {
            return false;
        }
        if (referentializationStrategy == null) {
            if (other.referentializationStrategy != null) {
                return false;
            }
        } else if (!referentializationStrategy.equals(other.referentializationStrategy)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        // The binding is not output because it can be quite long.
        return "DefaultAnaphorPart [anaphor=" + anaphor + ", anaphorExpression=" + anaphorExpression + ", referent="
                + referent + ", anaphorResolutionStrategy=" + anaphorResolutionStrategy
                + ", referentializationStrategy=" + referentializationStrategy + "]";
    }

}
