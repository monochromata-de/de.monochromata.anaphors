package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategy;
import de.monochromata.ast.AnaphorsApi;

/**
 * An abstract base class for strategies used to obtain referents based on
 * information referring to the conceptual type (TODO:?) of the referent.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public abstract class AbstractConceptReferentializationStrategy<N, E, TB, S, I, QI, EV, PP>
        extends AbstractReferentializationStrategy<N, E, TB, S, I, QI, EV, PP>
        implements ConceptReferentializationStrategy<E, TB, S, I, QI> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractConceptReferentializationStrategy() {
    }

    public AbstractConceptReferentializationStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi) {
        super(anaphorsApi);
    }

}
