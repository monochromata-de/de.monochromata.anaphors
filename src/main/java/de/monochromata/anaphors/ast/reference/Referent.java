package de.monochromata.anaphors.ast.reference;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

/**
 * The representation of a referent. A referent may provide a number of
 * features.
 *
 * @param <TB>
 *            The type binding type
 * @param <S>
 *            The scope type (optional)
 * @param <I>
 *            The type used to represent identifiers
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public interface Referent<TB, S, I, QI> extends FeatureContainer<QI> {

    /**
     * Used to resolve ambiguity between referents. Cases of resolvable ambiguity
     * include e.g. a referent that represents the invocation of a getter method
     * that returns the value of the field that is represented by another referent.
     *
     * @param other
     *            Another referent.
     * @return True, if this referent represents the same referent as {@code other}
     *         and this related expression should be used instead of {@code other}
     *         in order to eliminate the ambiguity between the two. False is
     *         returned otherwise.
     */
    public boolean canBeUsedInsteadOf(Referent<TB, S, I, QI> other);

    public boolean hasName();

    /**
     * Returns the name of this referent.
     *
     * If {@link #hasMethodName()} returns true and the method name is the only name
     * of this referent, this method returns a name constructed from the method
     * name. That name is constructed by removing any getter or setter prefix to the
     * method name and by turning the first character of the remaining string into
     * lower case.
     *
     * @return The name of this referent.
     * @throws UnsupportedOperationException
     *             If {@link #hasName()} returns false.
     */
    public QI getName();

    /**
     * Returns true, if this referent is created by a method invocation.
     *
     * TODO: Maybe replace by a system of thematic roles
     *
     * @return True, if the referent is created by a method invocation, false
     *         otherwise.
     * @see #getMethodName()
     */
    public boolean hasMethodName();

    /**
     * Returns the name of the method invocation which creates this referent, if
     * {@link #hasMethodName()} returns true.
     *
     * TODO: Maybe replace by a system of thematic roles
     *
     * @return The method name.
     * @throws UnsupportedOperationException
     *             If {@link #hasMethodName()} returns false.
     */
    public I getMethodName();

    /**
     * The type of the referent - is identical to the return type, if the referent
     * is a method.
     *
     * @param scope
     *            The scope containing the referent if used by the compiler-specific
     *            implementation. Implementations of this method must not access the
     *            scope but merely pass it on to SPI's they invoke.
     * @return The type binding of the referent.
     */
    public TB resolveType(S scope);

    /**
     * Return an object whose {@link Object#toString()} method returns a
     * representation of the internal state of the referent that can be passed to
     * {@link AnaphorResolutionStrategy#createReferent(Object, de.monochromata.anaphors.ast.relatedexp.RelatedExpression, Object)}
     *
     * TODO: Use {@link RelatedExpressionsSpi} methods to convert the memento to a
     * String and back, or create a ReferentSpi.
     *
     * @return A memento object.
     */
    public Object getMemento();

    public String getDescription();
}
