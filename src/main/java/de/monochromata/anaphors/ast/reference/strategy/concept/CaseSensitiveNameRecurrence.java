package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.spi.AnaphorsSpi;

/**
 * Referentialization based on case-sensitive name recurrence, like in
 * variables.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public class CaseSensitiveNameRecurrence<N, E, TB, S, I, QI, EV, PP>
        extends NameRecurrence<N, E, TB, S, I, QI, EV, PP> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected CaseSensitiveNameRecurrence() {
    }

    public CaseSensitiveNameRecurrence(final AnaphorsSpi<N, E, TB, S, I, QI, EV, PP> anaphorsSpi) {
        super(anaphorsSpi);
    }

    /**
     * Returns true
     */
    @Override
    protected boolean isCaseSensitive() {
        return true;
    }

    @Override
    public String getKind() {
        return super.getKind() + "CS";
    }

}
