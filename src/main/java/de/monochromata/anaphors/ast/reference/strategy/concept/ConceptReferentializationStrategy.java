package de.monochromata.anaphors.ast.reference.strategy.concept;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.feature.FeatureReferentializationStrategy;

/**
 * A referentialization strategy that interprets identifiers from right to left
 * and refers to the conceptual type(TODO: ?), assuming that the conceptual type
 * is not just another feature (TODO: find literature reference).
 *
 * <p>
 * A number of cases can be distinguished:
 * <ol>
 * <li>Neither the entire definite expression nor a suffix of it refer to the
 * conceptual type of the potential referent. In this case
 * {@link #canReferToUsingConceptualType(Object, Referent, Object)} returns
 * false and
 * {@link #getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(Object, Referent, Object)}
 * returns {@code null}.</li>
 * <li>The entire definite expression refers to the conceptual type of the
 * potential referent. In this case
 * {@link #canReferToUsingConceptualType(Object, Referent, Object)} returns true
 * and
 * {@link #getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(Object, Referent, Object)}
 * returns an empty {@link FeatureContainer}.</li>
 * <li>A suffix of the definite expression refers to the conceptual type of the
 * potential referent. In this case
 * {@link #canReferToUsingConceptualType(Object, Referent, Object)} returns true
 * and
 * {@link #getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(Object, Referent, Object)}
 * returns a non-empty {@link FeatureContainer}.</li>
 * </ol>
 *
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 *
 * @see FeatureContainer#isEmpty()
 */
public interface ConceptReferentializationStrategy<E, TB, S, I, QI>
        extends ReferentializationStrategy<E, TB, S, I, QI> {

    /**
     * Checks whether the given definite expression can, using this
     * referentialization strategy, refer to the given potential referent in the
     * given scope. Unlike {@link #canReferTo(Object, Referent, Object)}, this
     * method requires only conceptual-type information in a suffix of the definite
     * expression to be matched by the information of the given potential referent
     * for the method to return true. Matching is performed in a way that is
     * specific to the referentialization strategy.
     *
     * @param idFromDefiniteExpression the ID from the definite expression that
     *                                 shall refer to the given potential referent
     * @param potentialReferent        the potential referent to refer to
     * @param scope                    the scope in which the definite expression
     *                                 occurs
     * @return {@code true} if the conceptual-type information provided by a suffix
     *         of the definite expression matches the information provided by the
     *         potential referent, where matching depends on the implementation of
     *         this method, {@code false} otherwise.
     */
    public boolean canReferToUsingConceptualType(I idFromDefiniteExpression, Referent<TB, S, I, QI> potentialReferent,
            S scope);

    /**
     * If {@link #canReferToUsingConceptualType(Object, Referent, Object)} returns
     * {@code true} for the given arguments, there is a suffix in the given definite
     * expression that represents a conceptual type and can be used to refer to the
     * given potential referent. In this case, this method returns a
     * {@link FeatureContainer} with all features that can be extracted from the
     * remainder of the given definite expression after the suffix has been removed.
     * If the remainder is empty, the {@link FeatureContainer} is empty, too.
     *
     * @param idFromDefiniteExpression The ID from the definite expression whose
     *                                 features are to be extracted.
     * @param potentialReferent        The potential referent of the definite
     *                                 expression.
     * @param scope                    An optional scope object specific to the AST
     *                                 implementation.
     * @return {@code null}, if
     *         {@link #canReferToUsingConceptualType(Object, Referent, Object)}
     *         returns false, a (possibly) empty {@link FeatureContainer} if
     *         {@link #canReferToUsingConceptualType(Object, Referent, Object)}
     *         returns true.
     * @see FeatureReferentializationStrategy
     */
    public FeatureContainer<QI> getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(
            I idFromDefiniteExpression, Referent<TB, S, I, QI> potentialReferent, S scope);

}
