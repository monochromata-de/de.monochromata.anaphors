/**
 * <p>
 * Generic interfaces and standard implementations of referents and
 * referentialization strategies.
 * </p>
 */
package de.monochromata.anaphors.ast.reference;