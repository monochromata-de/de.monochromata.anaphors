/**
 * Concept-based referentialization strategies
 */
package de.monochromata.anaphors.ast.reference.strategy.concept;