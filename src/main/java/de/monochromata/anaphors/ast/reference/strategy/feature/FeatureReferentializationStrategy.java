package de.monochromata.anaphors.ast.reference.strategy.feature;

import de.monochromata.anaphors.ast.ASTBasedAnaphorResolution;
import de.monochromata.anaphors.ast.feature.Feature;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.concept.ConceptReferentializationStrategy;

/**
 * A referentialization strategy that interprets those parts of an identifier
 * not consumed by a {@link ConceptReferentializationStrategy} that this
 * strategy delegates to. This referentialization strategy interprets the
 * remaining parts of the identifier as verbalizations of {@link Feature}s.
 * 
 * <p>
 * Note that {@link ASTBasedAnaphorResolution} should not contain the delegates
 * returned by {@link #getDelegate()} because the
 * {@link FeatureReferentializationStrategy} will also apply to cases when the
 * delegate consumes all information in the definite expression. If both the
 * feature referentialization strategy and its delegate would be activated
 * during anaphor resolution, such cases would result in (technical) referential
 * ambiguity.
 * </p>
 *
 * @param <E>
 *            The expression type
 * @param <TB>
 *            The type binding type
 * @param <S>
 *            The scope type (optional)
 * @param <I>
 *            The type used to represent identifiers
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public interface FeatureReferentializationStrategy<E, TB, S, I, QI>
		extends ReferentializationStrategy<E, TB, S, I, QI> {

	/**
	 * Provides access to the {@link ConceptReferentializationStrategy} that
	 * this feature referentialization strategy delegates to.
	 * 
	 * @return the delegate
	 */
	public ConceptReferentializationStrategy<E, TB, S, I, QI> getDelegate();

}
