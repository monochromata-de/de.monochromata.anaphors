package de.monochromata.anaphors.ast.reference.strategy.concept;

import java.util.function.Predicate;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.ast.AnaphorsApi;

/**
 * Referentialization based on case-insensitive exact type recurrence.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public class TypeRecurrence<N, E, TB, S, I, QI, EV, PP>
        extends AbstractConceptReferentializationStrategy<N, E, TB, S, I, QI, EV, PP> {

    public static final String Rt_KIND = "Rt";

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected TypeRecurrence() {
    }

    public TypeRecurrence(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi) {
        super(anaphorsApi);
    }

    private boolean canReferToInternal(final Referent<TB, S, I, QI> potentialReferent, final S scope,
            final Predicate<TB> comparison) {
        final TB typeOfPotentialReferent = potentialReferent.resolveType(scope);
        // TODO: should cache type binding / use resolved type
        // TODO: Need to test cases in which multiple types with identical
        // simple name are available in the compilation unit
        return typeOfPotentialReferent != null && comparison.test(typeOfPotentialReferent);
    }

    /**
     * Returns true, if the given definite expression is a simple name, and the
     * simple name is equal to the simple name of the type of the referent.
     *
     * @param idFromDefiniteExpression the ID from the definite expression that
     *                                 shall refer to the given potential referent
     * @param potentialReferent        the potential referent of the definite
     *                                 expression
     * @param scope                    the scope in which the definite expression
     *                                 occurs
     * @return {@code true}, if the definite expression is a simple name that is
     *         equal to the simple name of the type of the referent, {@code false}
     *         otherwise.
     * @see #isCaseSensitive()
     * @see #canReferToUsingConceptualType(Object, Referent, Object)
     */
    @Override
    public boolean canReferTo(final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent,
            final S scope) {
        return canReferToInternal(potentialReferent, scope,
                typeOfPotentialReferent -> anaphorsApi.nameOfIdentifierEqualsSimpleNameOfTypeBinding(
                        idFromDefiniteExpression, typeOfPotentialReferent, isCaseSensitive()));
    }

    /**
     * Returns true, if the given definite expression is a simple name, and the
     * conceptual type expressed in simple name is equal to the simple name of the
     * type of the referent.
     *
     * @param idFromDefiniteExpression the ID from the definite expression that
     *                                 shall refer
     * @param potentialReferent        the potential referent of the definite
     *                                 expression
     * @param scope                    the scope in which the definite expression
     *                                 occurs
     * @return {@code true}, if the definite expression is a simple name that is
     *         equal to the simple name of the type of the referent, {@code false}
     *         otherwise.
     * @see #isCaseSensitive()
     * @see #canReferTo(Object, Referent, Object)
     */
    @Override
    public boolean canReferToUsingConceptualType(final I idFromDefiniteExpression,
            final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        return canReferToInternal(potentialReferent, scope,
                typeOfPotentialReferent -> anaphorsApi.conceptualTypeInIdentifierEqualsSimpleNameOfType(
                        idFromDefiniteExpression, typeOfPotentialReferent, isCaseSensitive()));
    }

    @Override
    public FeatureContainer<QI> getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(
            final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        // TODO: This implementation actually performs a number of checks twice
        // in canReferToUsingConceptualType and
        // spi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName
        // . It should be replaced by one that implements all checks in
        // spi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName
        // so that canReferToUsingConceptualType does not need to be invoked.
        if (canReferToUsingConceptualType(idFromDefiniteExpression, potentialReferent, scope)) {
            final TB typeOfPotentialReferent = potentialReferent.resolveType(scope);
            return anaphorsApi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentType(
                    idFromDefiniteExpression, typeOfPotentialReferent, isCaseSensitive());
        }
        return null;
    }

    /**
     * Whether or not type names are matched in a case-sensitive way.
     *
     * @return false
     */
    protected boolean isCaseSensitive() {
        return false;
    }

    @Override
    public String getKind() {
        return Rt_KIND;
    }
}
