package de.monochromata.anaphors.ast.reference;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.feature.DefaultFeatureContainer;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.ast.AnaphoraResolutionApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An abstract base class for referents.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <FB> The field binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractReferent<N, E, T, B, VB extends B, FB extends B, MB extends B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends DefaultFeatureContainer<QI> implements Referent<TB, S, I, QI> {

    private final R relatedExpression;
    private final String description;
    protected final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractReferent() {
        relatedExpression = null;
        description = null;
        anaphorResolutionApi = null;
    }

    public <EV, PP> AbstractReferent(final R relatedExpression, final String description,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final AnaphoraResolutionApi<N, E, T, B, VB, FB, MB, TB, S, I, QI> anaphorResolutionApi) {
        this.relatedExpression = relatedExpression;
        this.description = description;
        this.anaphorResolutionApi = anaphorResolutionApi;
        addAll(relatedExpressionsApi.getFeatures(relatedExpression.getRelatedExpression()));
    }

    protected R getRelatedExpression() {
        return relatedExpression;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((relatedExpression == null) ? 0 : relatedExpression.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractReferent other = (AbstractReferent) obj;
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (relatedExpression == null) {
            if (other.relatedExpression != null) {
                return false;
            }
        } else if (!relatedExpression.equals(other.relatedExpression)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractReferent [relatedExpression=" + relatedExpression + ", description=" + description
                + ", getFeatures()=" + getFeatures() + "]";
    }

}
