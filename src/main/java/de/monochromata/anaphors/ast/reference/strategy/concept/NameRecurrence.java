package de.monochromata.anaphors.ast.reference.strategy.concept;

import java.util.function.Supplier;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.ast.AnaphorsApi;

/**
 * Referentialization based on case-insensitive name recurrence.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public class NameRecurrence<N, E, TB, S, I, QI, EV, PP>
        extends AbstractConceptReferentializationStrategy<N, E, TB, S, I, QI, EV, PP> {

    public static final String Rn_KIND = "Rn";

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected NameRecurrence() {
    }

    public NameRecurrence(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi) {
        super(anaphorsApi);
    }

    private boolean canReferToInternal(final Referent<TB, S, I, QI> potentialReferent, final S scope,
            final Supplier<Boolean> comparison) {
        return potentialReferent.hasName() && comparison.get();
    }

    /**
     * Returns true, if the given definite expression is a simple name, the given
     * potential referent has a name, and the simple name is equal to the name of
     * the referent.
     *
     * @param idFromDefiniteExpression the ID created from the definite expression
     *                                 that shall refer
     * @param potentialReferent        the potential referent of the definite
     *                                 expression
     * @param scope                    the scope in which the definite expression
     *                                 occurs
     * @return {@code true}, if the definite expression is a simple name that is
     *         equal to the (available) name of the referent, {@code false}
     *         otherwise.
     * @see #isCaseSensitive()
     * @see #canReferToUsingConceptualType(Object, Referent, Object)
     */
    @Override
    public boolean canReferTo(final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent,
            final S scope) {
        return canReferToInternal(potentialReferent, scope, () -> anaphorsApi
                .nameOfReferentEqualsIdentifier(potentialReferent, idFromDefiniteExpression, isCaseSensitive()));
    }

    /**
     * Returns true, if the given definite expression is a simple name, the given
     * potential referent has a name, and the conceptual type expressed in the
     * simple name is equal to the conceptual type expressed in the name of the
     * referent.
     *
     * <p>
     * Note that during case-sensitive matching the first character of the
     * conceptual type expressed in the referent name is matched case-insensitively
     * to the character at that position in the definite expression because camel
     * case is used in the definite expression, if there is a prefix to the
     * conceptual type in the definite expression.
     * </p>
     *
     * @param idFromDefiniteExpression the ID from the definite expression that
     *                                 shall refer
     * @param potentialReferent        the potential referent of the definite
     *                                 expression
     * @param scope                    the scope in which the definite expression
     *                                 occurs
     * @return {@code true}, if the definite expression is a simple name that is
     *         identical to the (available) name of the referent, {@code false}
     *         otherwise.
     * @see #isCaseSensitive()
     * @see #canReferTo(Object, Referent, Object)
     */
    @Override
    public boolean canReferToUsingConceptualType(final I idFromDefiniteExpression,
            final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        return canReferToInternal(potentialReferent, scope,
                () -> anaphorsApi.nameOfReferentMatchesConceptualTypeOfIdentifier(potentialReferent,
                        idFromDefiniteExpression, isCaseSensitive()));
    }

    @Override
    public FeatureContainer<QI> getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(
            final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        // TODO: This implementation actually performs a number of checks twice
        // in canReferToUsingConceptualType and
        // spi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName
        // . It should be replaced by one that implements all checks in
        // spi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName
        // so that canReferToUsingConceptualType does not need to be invoked.
        if (canReferToUsingConceptualType(idFromDefiniteExpression, potentialReferent, scope)) {
            if (potentialReferent.hasName()) {
                return anaphorsApi.getFeaturesRemainingInIdentifierBesidesConceptualTypeOfReferentName(
                        idFromDefiniteExpression, potentialReferent, isCaseSensitive());
            }
        }
        return null;
    }

    /**
     * Whether to match case
     *
     * @return false
     */
    protected boolean isCaseSensitive() {
        return false;
    }

    @Override
    public String getKind() {
        return Rn_KIND;
    }
}
