package de.monochromata.anaphors.ast.reference.strategy.feature;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.AbstractReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.concept.ConceptReferentializationStrategy;
import de.monochromata.ast.AnaphorsApi;

/**
 * Referentialization based on feature recurrence (i.e. matching referents whose
 * type is equal to a suffix of the simple name that acts as definite expression
 * and that declare a feature in the constructor invocation that recurs in the
 * non-empty prefix to the suffix of the simple name).
 *
 * <p>
 * Note
 * </p>
 *
 * TODO: Ensure non-empty prefix to the type name in the simple name
 *
 * TODO: Make sure all features from the simple name are found in the referent
 *
 * TODO: Collect features not only from the constructor, but also from other
 * downstream accesses
 *
 * TODO: Implement matching of multiple features and flexible length of type
 * names ...
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <CI> The class instance creation expression node type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public class FeatureRecurrence<N, E, CI extends N, B, TB extends B, S, I, QI, EV, PP>
        extends AbstractReferentializationStrategy<N, E, TB, S, I, QI, EV, PP>
        implements FeatureReferentializationStrategy<E, TB, S, I, QI> {

    public static final String Rf_KIND_PREFIX = "Rf";
    private final ConceptReferentializationStrategy<E, TB, S, I, QI> delegate;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected FeatureRecurrence() {
        delegate = null;
    }

    public FeatureRecurrence(final ConceptReferentializationStrategy<E, TB, S, I, QI> delegate,
            final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi) {
        super(anaphorsApi);
        this.delegate = delegate;
    }

    @Override
    public boolean canReferTo(final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent,
            final S scope) {
        final FeatureContainer<QI> featuresInIdentifier = delegate
                .getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(idFromDefiniteExpression,
                        potentialReferent, scope);
        return featuresInIdentifier != null && potentialReferent.containsFeaturesOf(featuresInIdentifier);
    }

    @Override
    public ConceptReferentializationStrategy<E, TB, S, I, QI> getDelegate() {
        return delegate;
    }

    @Override
    public String getKind() {
        return Rf_KIND_PREFIX + delegate.getKind();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((delegate == null) ? 0 : delegate.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeatureRecurrence other = (FeatureRecurrence) obj;
        if (delegate == null) {
            if (other.delegate != null) {
                return false;
            }
        } else if (!delegate.equals(other.delegate)) {
            return false;
        }
        return true;
    }

}
