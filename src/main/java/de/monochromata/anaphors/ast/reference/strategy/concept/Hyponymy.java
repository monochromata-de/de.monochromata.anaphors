package de.monochromata.anaphors.ast.reference.strategy.concept;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

import de.monochromata.anaphors.ast.feature.FeatureContainer;
import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.ast.AnaphorsApi;

/**
 * Referentialization based on case-insensitive hyponymy (i.e. matching
 * referents whose type is a - potentially transitive - super-class or a -
 * potentially transitively - implemented interface of the type of the definite
 * expression).
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public class Hyponymy<N, E, TB, S, I, QI, EV, PP>
        extends AbstractConceptReferentializationStrategy<N, E, TB, S, I, QI, EV, PP> {

    public static final String Hy_KIND = "Hy";

    private final Function<TB, Optional<TB>> getSuperClass;
    private final Function<TB, List<TB>> getImplementedInterfaces;
    private final Function<I, Function<TB, Boolean>> nameOfIdentifierEqualsSimpleNameOfTypeBinding;
    private final Function<I, Function<TB, Boolean>> conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected Hyponymy() {
        this(null, null, null, null);
    }

    public Hyponymy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi,
            final Function<TB, Optional<TB>> getSuperClass, final Function<TB, List<TB>> getImplementedInterfaces) {
        this(getSuperClass, getImplementedInterfaces,
                id -> type -> anaphorsApi.nameOfIdentifierEqualsSimpleNameOfTypeBinding(id, type, false),
                id -> type -> anaphorsApi.conceptualTypeInIdentifierEqualsSimpleNameOfType(id, type, false));
    }

    public Hyponymy(final Function<TB, Optional<TB>> getSuperClass,
            final Function<TB, List<TB>> getImplementedInterfaces,
            final Function<I, Function<TB, Boolean>> nameOfIdentifierEqualsSimpleNameOfTypeBinding,
            final Function<I, Function<TB, Boolean>> conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding) {
        super(null);
        this.getSuperClass = getSuperClass;
        this.getImplementedInterfaces = getImplementedInterfaces;
        this.nameOfIdentifierEqualsSimpleNameOfTypeBinding = nameOfIdentifierEqualsSimpleNameOfTypeBinding;
        this.conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding = conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding;
    }

    @Override
    public boolean canReferTo(final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent,
            final S scope) {
        return canReferToInternal(potentialReferent, scope,
                nameOfIdentifierEqualsSimpleNameOfTypeBinding.apply(idFromDefiniteExpression));
    }

    private boolean canReferToInternal(final Referent<TB, S, I, QI> potentialReferent, final S scope,
            final Function<TB, Boolean> comparison) {
        return canReferToInternal(potentialReferent, scope, (Predicate<TB>) type -> comparison.apply(type));
    }

    private boolean canReferToInternal(final Referent<TB, S, I, QI> potentialReferent, final S scope,
            final Predicate<TB> comparison) {
        final var canReferToAnImplementedInterface = canReferToAnImplementedInterface(comparison);
        final var type = potentialReferent.resolveType(scope);
        final var optionalSuperClass = getSuperClass.apply(type);
        return canReferToASuperclass(optionalSuperClass, comparison, canReferToAnImplementedInterface)
                || canReferToAnImplementedInterface.test(type);
    }

    private boolean canReferToASuperclass(final Optional<TB> optionalSuperClass,
            final Predicate<TB> canReferUsingSimpleNameOfTypeBinding,
            final Predicate<TB> canReferToAnImplementedInterface) {
        if (optionalSuperClass.isEmpty()) {
            return false;
        }
        final var superClass = optionalSuperClass.get();
        final var canReferToSuperClass = canReferUsingSimpleNameOfTypeBinding.test(superClass);
        if (canReferToSuperClass) {
            return true;
        } else if (canReferToAnImplementedInterface.test(superClass)) {
            return true;
        }
        // Recurse
        return canReferToASuperclass(getSuperClass.apply(superClass), canReferUsingSimpleNameOfTypeBinding,
                canReferToAnImplementedInterface);
    }

    private Predicate<TB> canReferToAnImplementedInterface(final Predicate<TB> canReferUsingSimpleNameOfTypeBinding) {
        return type -> {
            final var implementedInterfaces = getImplementedInterfaces.apply(type);
            final var canReferToImplementedInterface = implementedInterfaces.stream()
                    .anyMatch(canReferUsingSimpleNameOfTypeBinding);
            if (canReferToImplementedInterface) {
                return true;
            }
            // Recurse
            return implementedInterfaces.stream()
                    .anyMatch(canReferToAnImplementedInterface(canReferUsingSimpleNameOfTypeBinding));
        };
    }

    @Override
    public boolean canReferToUsingConceptualType(final I idFromDefiniteExpression,
            final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        return canReferToInternal(potentialReferent, scope,
                conceptualTypeInIdentifierEqualsSimpleNameOfTypeBinding.apply(idFromDefiniteExpression));
    }

    @Override
    public FeatureContainer<QI> getFeaturesRemainingInIdentifierIfItCanReferUsingConceptualType(
            final I idFromDefiniteExpression, final Referent<TB, S, I, QI> potentialReferent, final S scope) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getKind() {
        return Hy_KIND;
    }

}
