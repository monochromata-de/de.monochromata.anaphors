package de.monochromata.anaphors.ast.reference.strategy;

import de.monochromata.AbstractStrategy;
import de.monochromata.ast.AnaphorsApi;

/**
 * An abstract base class for strategies used to obtain referents.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 */
public abstract class AbstractReferentializationStrategy<N, E, TB, S, I, QI, EV, PP> extends AbstractStrategy
        implements ReferentializationStrategy<E, TB, S, I, QI> {

    protected final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractReferentializationStrategy() {
        this(null);
    }

    public AbstractReferentializationStrategy(final AnaphorsApi<N, E, TB, S, I, QI, EV, PP> anaphorsApi) {
        this.anaphorsApi = anaphorsApi;
    }

}
