package de.monochromata.anaphors.ast.reference.strategy;

import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.reference.Referent;

/**
 * A strategy used to obtain referents
 *
 * @param <E>
 *            The expression type
 * @param <TB>
 *            The type binding type
 * @param <S>
 *            The scope type (optional)
 * @param <I>
 *            The type used to represent identifiers
 * @param <QI>
 *            The type used to represent qualified identifiers
 */
public interface ReferentializationStrategy<E, TB, S, I, QI> extends Strategy {

    /**
     * Checks whether the given definite expression can, using this
     * referentialization strategy, refer to the given potential referent in the
     * given scope. All information in the definite expression needs to be matched
     * by the given potential referent for the method to return true. Matching is
     * performed in a way that is specific to the referentialization strategy.
     *
     * @param idFromDefiniteExpression
     *            the ID from the definite expression that shall refer to the given
     *            potential referent
     * @param potentialReferent
     *            the potential referent to refer to
     * @param scope
     *            the scope in which the definite expression occurs
     * @return {@code true} if all information provided by the definite expression
     *         matches information provided by the potential referent, where
     *         matching depends on the implementation of this method, {@code false}
     *         otherwise.
     */
    public boolean canReferTo(I idFromDefiniteExpression, Referent<TB, S, I, QI> potentialReferent, S scope);

}
