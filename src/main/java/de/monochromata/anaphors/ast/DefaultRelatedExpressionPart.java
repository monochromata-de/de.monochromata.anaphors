package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;

/**
 * An default implementation of the {@link RelatedExpressionPart} interface.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <VB> The variable binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 */
public class DefaultRelatedExpressionPart<N, E, T, B, VB extends B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>>
        implements RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> {

    private final R relatedExpression;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultRelatedExpressionPart() {
        this(null);
    }

    public DefaultRelatedExpressionPart(final R relatedExpression) {
        this.relatedExpression = relatedExpression;
    }

    @Override
    public R getRelatedExpression() {
        return relatedExpression;
    }

    @Override
    public RelatedExpressionStrategy<N, T, B, TB, S, QI, R> getRelatedExpressionStrategy() {
        return relatedExpression.getStrategy();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((relatedExpression == null) ? 0 : relatedExpression.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultRelatedExpressionPart other = (DefaultRelatedExpressionPart) obj;
        if (relatedExpression == null) {
            if (other.relatedExpression != null) {
                return false;
            }
        } else if (!relatedExpression.equals(other.relatedExpression)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DefaultRelatedExpressionPart [relatedExpression=" + relatedExpression + "]";
    }

}
