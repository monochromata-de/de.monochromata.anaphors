package de.monochromata.anaphors.ast;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableContents;

public interface AnaphorPartsStreaming {

	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> List<Pair<LocalTempVariableContents, String>> toVariableContentsAndAnaphors(
			final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> anaphorParts) {
		final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = anaphorParts.stream()
				.map(anaphorPart -> new ImmutablePair<>(
						anaphorPart.getAnaphorResolutionStrategy().getLocalTempVariableContents(),
						anaphorPart.getAnaphor()))
				.collect(toList());
		return variableContentsAndAnaphors;
	}

}
