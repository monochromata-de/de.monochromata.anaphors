package de.monochromata.anaphors.ast.transform;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * An AST transformation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface ASTTransformation<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> {

	/**
	 * Perform the transformation and convert the given preliminary anaphora (that
	 * is based on the AST before the transformation) into a potential anaphora that
	 * is based on the AST after the transformation (which might contain new nodes).
	 * <p>
	 * The relationship between the transformation and preliminary anaphora depends
	 * on the kind of transformation. I.e. a certain transformation might introduce
	 * a node that exchanges the related expression from the preliminary anaphora by
	 * a new node that is eventually used in the potential anaphora while another
	 * transformation might replace other nodes.
	 * <p>
	 * The potential anaphor uses the same strategies as the preliminary anaphora,
	 * though.
	 * <p>
	 * TODO: error handling: what shall happen if the transformation fails?
	 *
	 * @param preliminaryAnaphora the preliminary anaphora
	 * @return a potential anaphora relation
	 */
	public A perform(final A preliminaryAnaphora);

}
