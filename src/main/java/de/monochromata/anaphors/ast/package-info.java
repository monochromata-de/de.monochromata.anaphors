/**
 * <p>
 * A generic framework for AST-based anaphora resolution and standard anaphor
 * resolution strategies.
 * </p>
 *
 * <p>
 * The interfaces
 * {@link de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy}
 * , {@link de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy} and
 * {@link de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy}
 * interact in the following way. All available
 * {@link de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy}
 * instances are used to provide potential related expressions to all available
 * {@link de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy}
 * instances that uses all available
 * {@link de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy}
 * instances to find potential referents.
 * </p>
 *
 * <p>
 * TODO: Also mention the difference between anaphors and anaphora
 * </p>
 *
 * <p>
 * TODO: Add caption / refer to the image:<br>
 * <img src=
 * "../../../resources/de/monochromata/anaphors/doc-files/Model_Index_Anaphora_Relations.PNG"
 * alt= "Interfaces and classes implementing anaphora relations">
 * </p>
 *
 * <p>
 * TODO: Add caption / refer to the image:<br>
 * <img src=
 * "../../../resources/de/monochromata/anaphors/doc-files/Model_Index_Anaphor_Resolution_Strategies.PNG"
 * alt="Interfaces and classes implementing anaphor resolution strategies">
 * </p>
 *
 * <p>
 * TODO: Add caption / refer to the image:<br>
 * <img src=
 * "../../../resources/de/monochromata/anaphors/doc-files/Model_Index_SPI_Access.PNG"
 * alt= "Accessing the Service-Provider Interfaces (SPIs)">
 * </p>
 *
 * <p>
 * Note that this package supports anaphora resolution on different AST
 * implementations. See the package {@link de.monochromata.anaphors.ast.spi} for
 * a service-provider interface for specific AST implementations.
 * </p>
 */
package de.monochromata.anaphors.ast;
