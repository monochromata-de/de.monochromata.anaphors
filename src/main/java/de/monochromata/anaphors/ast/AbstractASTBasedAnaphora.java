package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * An abstract implementation of the anaphora interface.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractASTBasedAnaphora<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        implements ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A> {

    private final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart;
    private final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart;

    private final boolean isUnderspecified;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractASTBasedAnaphora() {
        this(null, null, false);
    }

    public AbstractASTBasedAnaphora(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final boolean isUnderspecified) {
        this.relatedExpressionPart = relatedExpressionPart;
        this.anaphorPart = anaphorPart;
        this.isUnderspecified = isUnderspecified;
    }

    @Override
    public RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> getRelatedExpressionPart() {
        return relatedExpressionPart;
    }

    @Override
    public AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> getAnaphorPart() {
        return anaphorPart;
    }

    @Override
    public boolean isUnderspecified() {
        return isUnderspecified;
    }

    @Override
    public boolean isExplicated() {
        return !isUnderspecified();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anaphorPart == null) ? 0 : anaphorPart.hashCode());
        result = prime * result + (isUnderspecified ? 1231 : 1237);
        result = prime * result + ((relatedExpressionPart == null) ? 0 : relatedExpressionPart.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbstractASTBasedAnaphora other = (AbstractASTBasedAnaphora) obj;
        if (anaphorPart == null) {
            if (other.anaphorPart != null) {
                return false;
            }
        } else if (!anaphorPart.equals(other.anaphorPart)) {
            return false;
        }
        if (isUnderspecified != other.isUnderspecified) {
            return false;
        }
        if (relatedExpressionPart == null) {
            if (other.relatedExpressionPart != null) {
                return false;
            }
        } else if (!relatedExpressionPart.equals(other.relatedExpressionPart)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AbstractASTBasedAnaphora [relatedExpressionPart=" + relatedExpressionPart + ", anaphorPart="
                + anaphorPart + ", isUnderspecified=" + isUnderspecified + "]";
    }

}
