package de.monochromata.anaphors.ast.relatedexp.strategy;

import static java.util.Collections.emptyList;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.ast.PreferencesApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * Local variable declarations functioning as related expression.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 */
public class LocalVariableDeclarationStrategy<N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>>
        extends AbstractRelatedExpressionStrategy<N, E, T, B, MB, TB, S, I, QI, EV, PP, R> {

    public static final String LVD_KIND = "LVD";

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected LocalVariableDeclarationStrategy() {
    }

    public LocalVariableDeclarationStrategy(
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final PreferencesApi preferencesApi) {
        super(relatedExpressionsApi, preferencesApi);
    }

    @Override
    public String getKind() {
        return LVD_KIND;
    }

    @Override
    public List<Perspectivation> underspecifyRelatedExpression(final R relatedExpression,
            final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors, final S scope) {
        // No perspectivation necessary
        return emptyList();
    }

    protected boolean refersToSameAstNode(final R thisInstance, final R otherInstance) {
        return thisInstance.equals(otherInstance)
                && thisInstance.getRelatedExpression() == otherInstance.getRelatedExpression();
    }

    protected boolean isOnlyFragment(final R thisInstance, final R otherInstance,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsSpi) {
        return relatedExpressionsSpi.isOnlyFragmentOfMultiVariable(thisInstance.getRelatedExpression(),
                otherInstance.getRelatedExpression());
    }

    protected boolean isInitializer(final R thisInstance, final R otherInstance,
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsSpi) {
        return otherInstance.getStrategy() instanceof ClassInstanceCreationStrategy && relatedExpressionsSpi
                .hasInitializer(thisInstance.getRelatedExpression(), otherInstance.getRelatedExpression());
    }
}
