package de.monochromata.anaphors.ast.relatedexp.strategy;

import java.util.List;

import de.monochromata.AbstractStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.ast.PreferencesApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * An abstract base class for strategies used to generate (potential) related
 * expressions from AST nodes.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 */
public abstract class AbstractRelatedExpressionStrategy<N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>>
        extends AbstractStrategy implements RelatedExpressionStrategy<N, T, B, TB, S, QI, R> {

    private List<N> collection;
    protected final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi;
    protected final PreferencesApi preferencesApi;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractRelatedExpressionStrategy() {
        this(null, null);
    }

    public AbstractRelatedExpressionStrategy(
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final PreferencesApi preferencesApi) {
        this.relatedExpressionsApi = relatedExpressionsApi;
        this.preferencesApi = preferencesApi;
    }

    @Override
    public void collectTo(final List<N> potentialRelatedExpressions) {
        this.collection = potentialRelatedExpressions;
    }

    @Override
    public void stopCollection() {
        this.collection = null;
    }

    /**
     * Add the given potential related expression to the current collection.
     *
     * @param potentialRelatedExpression Adds the given potential related expression
     *                                   to the collection of potential related
     *                                   expressions.
     * @see #collectTo(List)
     * @see #stopCollection()
     */
    protected void addToCollection(final N potentialRelatedExpression) {
        if (this.collection == null) {
            throw new IllegalStateException("No collection set, need to invoke collectTo(List<ASTNode>)");
        }
        this.collection.add(potentialRelatedExpression);
    }
}
