package de.monochromata.anaphors.ast.relatedexp.strategy;

public enum LocalTempVariableContents {
    ANCHOR, REFERENT
}