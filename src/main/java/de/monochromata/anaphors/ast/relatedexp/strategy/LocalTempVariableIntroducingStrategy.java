package de.monochromata.anaphors.ast.relatedexp.strategy;

import java.util.List;
import java.util.function.Function;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.strategy.AnchoringStrategy;

/**
 * A related expression strategy that introduces a local temporary variable
 * during anaphor resolution.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface LocalTempVariableIntroducingStrategy<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends RelatedExpressionStrategy<N, T, B, TB, S, QI, R> {

    int getLengthOfTypeForTempVariable(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final S scope);

    /**
     * @param importRewrite To turn a type binding into a type that can be used in
     *                      the AST
     */
    @Deprecated
    T getTypeForTempVariable(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final S scope, Function<TB, T> importRewrite);

    /**
     * @return The identifier to be used for the temp variable: for anaphora with
     *         {@link AnchoringStrategy}, a name is derived from the related
     *         expression, for other anaphora, the anaphor is used.
     */
    I getIdentifierForTempVariable(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> anaphorParts, final S scope);

}
