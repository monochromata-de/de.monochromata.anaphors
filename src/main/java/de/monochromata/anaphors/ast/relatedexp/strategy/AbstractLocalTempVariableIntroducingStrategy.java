package de.monochromata.anaphors.ast.relatedexp.strategy;

import static de.monochromata.anaphors.ast.AnaphorPartsStreaming.toVariableContentsAndAnaphors;
import static java.util.Arrays.asList;

import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.perspectivation.Perspectivation;
import de.monochromata.ast.PreferencesApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * Abstract base class for related expressions whose realization introduces
 * local temporary variables.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public abstract class AbstractLocalTempVariableIntroducingStrategy<N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractRelatedExpressionStrategy<N, E, T, B, MB, TB, S, I, QI, EV, PP, R>
        implements LocalTempVariableIntroducingStrategy<N, E, T, B, TB, S, I, QI, R, A> {

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected AbstractLocalTempVariableIntroducingStrategy() {
    }

    public AbstractLocalTempVariableIntroducingStrategy(
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final PreferencesApi preferencesApi) {
        super(relatedExpressionsApi, preferencesApi);
    }

    @Override
    public List<Perspectivation> underspecifyRelatedExpression(final R relatedExpression,
            final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors, final S scope) {
        return underspecifyRelatedExpressionForLocalTempVariable(relatedExpression, variableContentsAndAnaphors, scope);
    }

    public List<Perspectivation> underspecifyRelatedExpressionForLocalTempVariable(final R relatedExpression,
            final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors, final S scope) {
        final String optionalFinalModifierAndSpace = preferencesApi.getAddFinalModifierToCreatedTemporaryVariables()
                ? "final "
                : "";
        final int lengthOfTypeName = getLengthOfTypeForTempVariable(relatedExpression, scope);
        final I tempName = relatedExpressionsApi.guessTempName(relatedExpression, variableContentsAndAnaphors, scope);
        final int lengthOfTempName = relatedExpressionsApi.getLength(tempName);
        final int lengthToHide = optionalFinalModifierAndSpace.length() + lengthOfTypeName + " ".length()
                + lengthOfTempName + " = ".length();
        return asList(new Perspectivation.Hide(0, lengthToHide));
    }

    @Override
    public int getLengthOfTypeForTempVariable(
            final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart, final S scope) {
        final R relatedExpression = relatedExpressionPart.getRelatedExpression();
        return getLengthOfTypeForTempVariable(relatedExpression, scope);
    }

    public int getLengthOfTypeForTempVariable(final R relatedExpression, final S scope) {
        if (useLocalVariableTypeInference(scope)) {
            final T type = relatedExpressionsApi.getReservedTypeVar(scope);
            return relatedExpressionsApi.getLengthOfSimpleNameOfType(type);
        }
        return relatedExpression.getLengthOfTypeForTempVar(scope);
    }

    @Override
    public T getTypeForTempVariable(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final S scope, final Function<TB, T> importRewrite) {
        final R relatedExpression = relatedExpressionPart.getRelatedExpression();
        return getTypeForTempVariable(relatedExpression, scope, importRewrite);
    }

    public T getTypeForTempVariable(final R relatedExpression, final S scope, final Function<TB, T> importRewrite) {
        if (useLocalVariableTypeInference(scope)) {
            return relatedExpressionsApi.getReservedTypeVar(scope);
        }
        return relatedExpression.getTypeForTempVar(scope, importRewrite);
    }

    protected boolean useLocalVariableTypeInference(final S scope) {
        return preferencesApi.getUseLocalVariableTypeInference()
                && relatedExpressionsApi.supportsLocalVariableTypeInference(scope);
    }

    @Override
    public I getIdentifierForTempVariable(
            final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final List<AnaphorPart<N, E, T, B, TB, S, I, QI, R, A>> anaphorParts, final S scope) {
        final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors = toVariableContentsAndAnaphors(
                anaphorParts);
        return relatedExpressionsApi.guessTempName(relatedExpressionPart.getRelatedExpression(),
                variableContentsAndAnaphors, scope);
    }

}
