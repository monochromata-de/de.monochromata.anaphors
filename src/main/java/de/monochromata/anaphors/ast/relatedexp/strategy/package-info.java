/**
 * Strategies for identifying and handling related expressions
 */
package de.monochromata.anaphors.ast.relatedexp.strategy;