package de.monochromata.anaphors.ast.relatedexp.strategy;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.ast.PreferencesApi;
import de.monochromata.ast.RelatedExpressionsApi;

/**
 * Method invocation expressions functioning as related expression.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class MethodInvocationStrategy<N, E, T, B, MB extends B, TB extends B, S, I, QI, EV, PP, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractLocalTempVariableIntroducingStrategy<N, E, T, B, MB, TB, S, I, QI, EV, PP, R, A> {

    public static final String MI_KIND = "MI";

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected MethodInvocationStrategy() {
    }

    public MethodInvocationStrategy(
            final RelatedExpressionsApi<N, E, T, B, MB, TB, S, I, QI, EV, PP> relatedExpressionsApi,
            final PreferencesApi preferencesApi) {
        super(relatedExpressionsApi, preferencesApi);
    }

    @Override
    public String getKind() {
        return MI_KIND;
    }
}
