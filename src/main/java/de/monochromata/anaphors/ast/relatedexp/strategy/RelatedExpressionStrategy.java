package de.monochromata.anaphors.ast.relatedexp.strategy;

import static de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy.LVD_KIND;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.Strategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.perspectivation.Perspectivation;

/**
 * A strategy for identifying and handling related expressions.
 *
 * @param <N>  The node type in the AST
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 */
public interface RelatedExpressionStrategy<N, T, B, TB extends B, S, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>>
        extends Strategy {

    public void collectTo(List<N> potentialRelatedExpressions);

    public void stopCollection();

    /**
     * Create a perspectivations for the related expression
     */
    public List<Perspectivation> underspecifyRelatedExpression(final R relatedExpression,
            final List<Pair<LocalTempVariableContents, String>> variableContentsAndAnaphors, final S scope);

    /**
     * To be invoked when a related expression resolved with this strategy is
     * realized to obtain the kind of related expression that applies after
     * realization. All related expression strategies either represent parameters,
     * local variables or introduce local variables. Hence, this method typically
     * returns {@link LocalVariableDeclarationStrategy#LVD_KIND}.
     * <p>
     * That realized kind will also apply when the anaphora is re-resolved. Thus
     * setting the realized kind in the beginning will avoid a faux change being
     * detected during re-resolution of the unmodified anaphora relation.
     */
    default String getKindOfRelatedExpressionToBeRealized() {
        return LVD_KIND;
    }
}
