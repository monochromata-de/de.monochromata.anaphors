package de.monochromata.anaphors.ast.relatedexp;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Supplier;

import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.spi.RelatedExpressionsSpi;

/**
 * An abstract base class for implementing related expressions.
 *
 * @param <N>  The node type in the AST
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <MB> The method binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <QI> The type used to represent qualified identifiers
 * @param <EV> The type of the event contained in the condition that is
 *             evaluated to check when the perspectivations shall be applied.
 * @param <PP> The type used for positions that carry perspectivations
 * @param <R>  The sub-type of related expression to use
 *
 * @deprecated {@link RelatedExpression} will be turned into a POJO instead
 */
@Deprecated
public class DefaultRelatedExpression<N, T, B, MB extends B, TB extends B, S, QI, EV, PP, R extends DefaultRelatedExpression<N, T, B, MB, TB, S, QI, EV, PP, R>>
        implements RelatedExpression<N, T, B, TB, S, QI, R> {

    private final boolean isEffectivelyFinal;
    private final Supplier<QI> nameSupplier;
    private N relatedExpression;
    private final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> strategy;
    protected final RelatedExpressionsSpi<N, ?, T, B, MB, TB, S, ?, QI, EV, PP, R> relatedExpressionsSpi;
    private final Function<S, B> nameBindingResolver;
    private final Function<S, Integer> lengthOfTypeForTempVarFunction;
    private final BiFunction<S, Function<TB, T>, T> typeForTypeVarFunction;
    private final Function<S, TB> typeResolver;
    private final BiPredicate<? super DefaultRelatedExpression<N, T, B, MB, TB, S, QI, EV, PP, R>, R> canBeUsedInsteadOfBiPredicate;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultRelatedExpression() {
        this(false, null, null, null, null, null, null, null, null, null);
    }

    public DefaultRelatedExpression(final boolean isEffectivelyFinal, final Supplier<QI> nameSupplier,
            final N relatedExpression, final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> strategy,
            final RelatedExpressionsSpi<N, ?, T, B, MB, TB, S, ?, QI, EV, PP, R> relatedExpressionsSpi,
            final Function<S, B> nameBindingResolver, final Function<S, Integer> lengthOfTypeForTempVarFunction,
            final BiFunction<S, Function<TB, T>, T> typeForTypeVarFunction, final Function<S, TB> typeResolver,
            final BiPredicate<? super DefaultRelatedExpression<N, T, B, MB, TB, S, QI, EV, PP, R>, R> canBeUsedInsteadOfBiPredicate) {
        this.isEffectivelyFinal = isEffectivelyFinal;
        this.nameSupplier = nameSupplier;
        this.relatedExpression = relatedExpression;
        this.strategy = strategy;
        this.relatedExpressionsSpi = relatedExpressionsSpi;
        this.nameBindingResolver = nameBindingResolver;
        this.lengthOfTypeForTempVarFunction = lengthOfTypeForTempVarFunction;
        this.typeForTypeVarFunction = typeForTypeVarFunction;
        this.typeResolver = typeResolver;
        this.canBeUsedInsteadOfBiPredicate = canBeUsedInsteadOfBiPredicate;
    }

    @Override
    public boolean isEffectivelyFinal() {
        return isEffectivelyFinal;
    }

    @Override
    public QI getName() {
        return nameSupplier.get();
    }

    @Override
    public N getRelatedExpression() {
        return relatedExpression;
    }

    @Override
    public void setRelatedExpression(final N relatedExpression) {
        this.relatedExpression = relatedExpression;
    }

    @Override
    public RelatedExpressionStrategy<N, T, B, TB, S, QI, R> getStrategy() {
        return strategy;
    }

    @Override
    public B resolveNameBinding(final S scope) {
        return nameBindingResolver.apply(scope);
    }

    @Override
    public int getLengthOfTypeForTempVar(final S scope) {
        return lengthOfTypeForTempVarFunction.apply(scope);
    }

    @Override
    public T getTypeForTempVar(final S scope, final Function<TB, T> importRewrite) {
        return typeForTypeVarFunction.apply(scope, importRewrite);
    }

    @Override
    public TB resolveType(final S scope) {
        return typeResolver.apply(scope);
    }

    @Override
    public List<QI> getContainedNamesFromSurface() {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public List<QI> getPartNames() {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public List<QI> getAssociateNames() {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public List<TB> resolveContainedTypesFromSurface(final S scope) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public List<TB> resolvePartTypes(final S scope) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public List<TB> resolveAssociateTypes(final S scope) {
        throw new RuntimeException("Not yet implemented");
    }

    @Override
    public boolean canBeUsedInsteadOf(final R other) {
        return canBeUsedInsteadOfBiPredicate.test(this, other);
    }

    @Override
    public String getDescription() {
        return relatedExpressionsSpi.getDescription(relatedExpression);
    }

    @Override
    public int getLine() {
        return relatedExpressionsSpi.getLine(relatedExpression);
    }

    @Override
    public int getColumn() {
        return relatedExpressionsSpi.getColumn(relatedExpression);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.relatedExpression == null) ? 0 : this.relatedExpression.hashCode());
        result = prime * result + ((this.strategy == null) ? 0 : this.strategy.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        final DefaultRelatedExpression<N, T, B, MB, TB, S, QI, EV, PP, R> other = (DefaultRelatedExpression<N, T, B, MB, TB, S, QI, EV, PP, R>) obj;
        if (this.relatedExpression == null) {
            if (other.relatedExpression != null) {
                return false;
            }
        } else if (!relatedExpressionsSpi.compare(this.relatedExpression, other.relatedExpression)) {
            return false;
        }
        if (this.strategy == null) {
            if (other.strategy != null) {
                return false;
            }
        } else if (!this.strategy.equals(other.strategy)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RelatedExpression [name=" + this.getName() + ", relatedExpression=" + this.relatedExpression
                + ", strategy=" + this.strategy + "]";
    }

}
