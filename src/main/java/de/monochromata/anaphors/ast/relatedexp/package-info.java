/**
 * <p>
 * Generic interfaces for and standard implementations of expressions that
 * anaphors are related to (related expressions).
 * </p>
 */
package de.monochromata.anaphors.ast.relatedexp;