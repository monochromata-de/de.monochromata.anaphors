package de.monochromata.anaphors.ast.relatedexp;

import java.util.List;
import java.util.function.Function;

import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;

/**
 * Represents the function of an expression, or statement as related expression.
 *
 * @param <N>  The node type in the AST
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 */
public interface RelatedExpression<N, T, B, TB extends B, S, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>> {

    /**
     * If the related expression is effectively final, its value is identical at
     * run-time, regardless of whether the related expression occurs in its original
     * position, or would be moved or copied to the position of the anaphor.
     *
     * @return {@literal true}, if the related expression is effectively final.
     */
    public boolean isEffectivelyFinal();

    /**
     * @return {@literal true}, if anaphor resolution should replace the related
     *         expression with a declaration of a temporary local variable. This is
     *         required whenever the related expression is not effectively final and
     *         is not already used to initialize an effectively final local variable
     *         that can be re-used.
     * @see #isEffectivelyFinal()
     */
    default boolean shouldResolutionReplaceRelatedExpressionWithTempDeclaration() {
        return !isEffectivelyFinal();
    }

    public N getRelatedExpression();

    /**
     * Replaces the current related expression with the given one.
     *
     * @param relatedExpression The new related expression
     * @see #getRelatedExpression()
     * @see #getStrategy()
     * @deprecated
     */
    @Deprecated
    public void setRelatedExpression(N relatedExpression);

    public RelatedExpressionStrategy<N, T, B, TB, S, QI, R> getStrategy();

    /**
     * Used to resolve referential ambiguity between related expressions.
     *
     * @param other Another related expression.
     * @return True, if this related expression refers to the same referent as
     *         <code>other</code> and this related expression should be used instead
     *         of <code>other</code> in order to eliminate the ambiguity between the
     *         two. False is returned otherwise.
     */
    public boolean canBeUsedInsteadOf(R other);

    /**
     * Whether or not this related expression declares a name.
     *
     * @return True, if a name is declared, false otherwise.
     */
    default boolean hasName() {
        return null != getName();
    }

    /**
     * Return the name used as identifier in the related expression, if the related
     * expression declares a name. Null is returned, if {@link #hasName()} returns
     * false.
     *
     * @see #hasName()
     * @return Null, if {@link #hasName()} returns false.
     */
    public QI getName();

    public List<QI> getPartNames();

    public List<QI> getAssociateNames();

    public List<QI> getContainedNamesFromSurface();

    /**
     * Resolves the binding of the name declared in the related expression. Returns
     * {@literal null}, if {@link #hasName()} returns {@literal false}.
     *
     * @param scope The scope containing the related expression if used by the
     *              compiler-specific implementation. Implementations of this method
     *              must not access the scope but merely pass it on to SPI's they
     *              invoke.
     * @return The resolved binding.
     */
    public B resolveNameBinding(S scope);

    public int getLengthOfTypeForTempVar(S scope);

    /**
     * Returns the type of the related expression for use in declaring a local
     * temporary variable. This method needs to add import declarations if necessary
     * to make the type useable for the temp var.
     *
     * @param importRewrite To turn a type binding into a type that can be used in
     *                      the AST
     */
    @Deprecated
    public T getTypeForTempVar(S scope, Function<TB, T> importRewrite);

    /**
     * Resolve the type of the related expression
     *
     * @param scope The scope containing the related expression if used by the
     *              compiler-specific implementation. Implementations of this method
     *              must not access the scope but merely pass it on to SPI's they
     *              invoke.
     * @return The resolved type.
     */
    public TB resolveType(S scope);

    public List<TB> resolvePartTypes(S scope);

    public List<TB> resolveAssociateTypes(S scope);

    public List<TB> resolveContainedTypesFromSurface(S scope);

    public String getDescription();

    public int getLine();

    public int getColumn();

}
