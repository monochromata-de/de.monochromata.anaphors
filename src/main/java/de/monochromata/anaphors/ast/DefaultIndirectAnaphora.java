package de.monochromata.anaphors.ast;

import static java.util.Optional.empty;

import java.util.Optional;
import java.util.function.Supplier;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * A default implementation of an indirect anaphora relation.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class DefaultIndirectAnaphora<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
        extends AbstractASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>
        implements IndirectAnaphora<N, E, T, B, TB, S, I, QI, R, A> {

    private final String underspecifiedRelation;
    private final Optional<Supplier<String>> customReferenceDescriptionSupplier;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected DefaultIndirectAnaphora() {
        underspecifiedRelation = null;
        customReferenceDescriptionSupplier = null;
    }

    public DefaultIndirectAnaphora(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final String underspecifiedRelation,
            final boolean isUnderspecified) {
        this(relatedExpressionPart, anaphorPart, underspecifiedRelation, empty(), isUnderspecified);
    }

    public DefaultIndirectAnaphora(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final String underspecifiedRelation,
            final Supplier<String> customReferenceDescriptionSupplier, final boolean isUnderspecified) {
        this(relatedExpressionPart, anaphorPart, underspecifiedRelation,
                Optional.of(customReferenceDescriptionSupplier), isUnderspecified);
    }

    protected DefaultIndirectAnaphora(final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final String underspecifiedRelation,
            final Optional<Supplier<String>> customReferenceDescriptionSupplier, final boolean isUnderspecified) {
        super(relatedExpressionPart, anaphorPart, isUnderspecified);
        this.underspecifiedRelation = underspecifiedRelation;
        this.customReferenceDescriptionSupplier = customReferenceDescriptionSupplier;
    }

    /*
     * (non-Javadoc)
     *
     * @see de.monochromata.anaphors.IndirectAnaphora#getUnderspecifiedRelation()
     */
    @Override
    public String getUnderspecifiedRelation() {
        return underspecifiedRelation;
    }

    @Override
    public String getReferenceDescription() {
        return customReferenceDescriptionSupplier.map(Supplier::get).orElseGet(this::getDefaultReferenceDescription);
    }

    protected String getDefaultReferenceDescription() {
        return getReferent().getDescription() + " of " + getRelatedExpression().getRelatedExpression() + " at "
                + getRelatedExpression().getLine() + ":" + getRelatedExpression().getColumn() + " (" + getKind() + ")";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((underspecifiedRelation == null) ? 0 : underspecifiedRelation.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DefaultIndirectAnaphora other = (DefaultIndirectAnaphora) obj;
        if (underspecifiedRelation == null) {
            if (other.underspecifiedRelation != null) {
                return false;
            }
        } else if (!underspecifiedRelation.equals(other.underspecifiedRelation)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getKind() + " [getUnderspecifiedRelation()=" + underspecifiedRelation + ", getRelatedExpression()="
                + getRelatedExpression() + ", getAnaphor()=" + getAnaphor() + ", getAnaphorExpression()="
                + getAnaphorExpression() + ", getReferent()=" + getReferent() + ", getRelatedExpressionStrategy()="
                + getRelatedExpressionStrategy() + ", getAnaphorResolutionStrategy()=" + getAnaphorResolutionStrategy()
                + ", getReferentializationStrategy()=" + getReferentializationStrategy() + ", getBinding()="
                + getBinding() + ", isUnderspecified()=" + isUnderspecified() + ", isExplicated()=" + isExplicated()
                + "]";
    }

}
