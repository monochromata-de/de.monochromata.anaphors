package de.monochromata.anaphors.ast.chain;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

public interface ChainTraversal {

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 * @throws IllegalStateException If no anaphor element could be found.
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> getAnaphorElementsForRelatedExpressionElement(
			final C relatedExpressionElement) {
		final Builder<C> builder = Stream.builder();
		collectAnaphorElements(relatedExpressionElement.next, builder, next -> next.anaphor != null,
				next -> next.relatedExpression == null);
		final List<C> anaphorElements = builder.build().collect(toList());
		if (anaphorElements.isEmpty()) {
			throw new IllegalStateException("No anaphor elements found");
		}
		return anaphorElements;
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 * @throws IllegalStateException If no related expression element could be
	 *                               found.
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> C getRelatedExpressionElementForAnaphorElement(
			final C anaphorElement) {
		C current = anaphorElement;
		while (current != null) {
			if (current.relatedExpression != null) {
				return current;
			}
			current = current.previous;
		}
		throw new IllegalStateException("No related expression element found");
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> Stream<C> getRelatedExpressionElements(
			final C root) {
		return getElements(root, next -> next.relatedExpression != null);
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> Stream<C> getAnaphorElements(
			final C root) {
		return getElements(root, next -> next.anaphor != null);
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> Stream<C> getElements(
			final C root, final Predicate<C> predicate) {
		final Builder<C> builder = Stream.builder();
		collectAnaphorElements(singletonList(root), builder, predicate);
		return builder.build();
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> void collectAnaphorElements(
			final List<C> nextElements, final Builder<C> builder, final Predicate<C> predicate) {
		collectAnaphorElements(nextElements, builder, predicate, unused -> true);
	}

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> void collectAnaphorElements(
			final List<C> nextElements, final Builder<C> builder, final Predicate<C> shouldAddElement,
			final Predicate<C> shouldContinue) {
		for (final C next : nextElements) {
			if (shouldAddElement.test(next)) {
				builder.add(next);
			}
			if (shouldContinue.test(next)) {
				collectAnaphorElements(next.next, builder, shouldAddElement);
			}
		}
	}

}
