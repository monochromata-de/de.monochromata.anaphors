package de.monochromata.anaphors.ast.chain;

import java.util.List;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <AT> The type used for attachments to chain elements that have an
 *             anaphor part. The attachment may be used to stored e.g. an old
 *             anaphora relation.
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 * @param <C>  The sub-type of chain element to use
 */
public interface ChainElementFactory<N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> {

	C create(C previous, List<C> next, RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpression,
			AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphor, AT anaphorAttachment);

}
