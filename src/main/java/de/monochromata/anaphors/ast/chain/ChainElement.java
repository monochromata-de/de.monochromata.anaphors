package de.monochromata.anaphors.ast.chain;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

import java.util.List;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * Used to form chains of anaphors. A chain element can have a single
 * predecessor and multiple successors. I.e. an anaphor can have a single
 * related expression and a related expression can have multiple related
 * expressions.
 * <p>
 * E.g.
 *
 * <pre>
 * {@code new String("hello"); println(helloString); println(helloString);}
 * </pre>
 *
 * would be realized as three {@link ChainElement}s:
 *
 * <pre>
 * {@code ChainElement(RelatedExpressionPart)
 * ├ ChainElement(AnaphorPart #1)
 * └ ChainElement(AnaphorPart #2)}
 * </pre>
 * <p>
 * This 1:N relationship between related expressions and anaphors reflects their
 * realization via local variables. From a theoretical, linguistic point it
 * might also suitable to consider the second anaphor to be related to the first
 * anaphor instead of being related to the (initial) related expression. From a
 * cognitive point in three-level semantics, the related expression and the two
 * anaphors would refer to a single node in memory and order would occur
 * temporally only in the order of reading.
 * <p>
 * While an {@link ASTBasedAnaphora} combines both a
 * {@link RelatedExpressionPart} and a {@link AnaphorPart}, an anaphoric chain
 * represents them as two {@link ChainElement}s. That way, a two
 * {@link AnaphorPart}s that are related to the same
 * {@link RelatedExpressionPart} can be represented as three
 * {@link ChainElement}s.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <AT> The type used for attachments to chain elements that have an
 *             anaphor part. The attachment may be used to stored e.g. an old
 *             anaphora relation.
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 * @param <C>  The sub-type of chain element to use
 */
public class ChainElement<N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> {

    public final C previous;
    public final List<C> next;

    public final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpression;
    public final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphor;

    public final AT anaphorAttachment;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected ChainElement() {
        this(null, null, null, null, null);
    }

    public ChainElement(final C previous, final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphor,
            final AT anaphorAttachment) {
        this(previous, emptyList(), null, anaphor, anaphorAttachment);
    }

    public ChainElement(final C next, final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpression) {
        this(null, singletonList(next), relatedExpression, null, null);
    }

    public ChainElement(final List<C> next,
            final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpression) {
        this(null, next, relatedExpression, null, null);
    }

    public ChainElement(final C previous, final List<C> next,
            final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpression,
            final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphor, final AT anaphorAttachment) {
        this.previous = previous;
        this.next = next;
        this.relatedExpression = relatedExpression;
        this.anaphor = anaphor;
        this.anaphorAttachment = anaphorAttachment;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anaphor == null) ? 0 : anaphor.hashCode());
        result = prime * result + ((anaphorAttachment == null) ? 0 : anaphorAttachment.hashCode());
        result = prime * result + ((relatedExpression == null) ? 0 : relatedExpression.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChainElement other = (ChainElement) obj;
        if (anaphor == null) {
            if (other.anaphor != null) {
                return false;
            }
        } else if (!anaphor.equals(other.anaphor)) {
            return false;
        }
        if (anaphorAttachment == null) {
            if (other.anaphorAttachment != null) {
                return false;
            }
        } else if (!anaphorAttachment.equals(other.anaphorAttachment)) {
            return false;
        }
        if (relatedExpression == null) {
            if (other.relatedExpression != null) {
                return false;
            }
        } else if (!relatedExpression.equals(other.relatedExpression)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ChainElement [relatedExpression=" + relatedExpression + ", anaphor=" + anaphor + ", attachment="
                + anaphorAttachment + "]";
    }

}
