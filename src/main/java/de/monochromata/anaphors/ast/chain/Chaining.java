package de.monochromata.anaphors.ast.chain;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import de.monochromata.anaphors.ast.ASTBasedAnaphora;
import de.monochromata.anaphors.ast.AnaphorPart;
import de.monochromata.anaphors.ast.RelatedExpressionPart;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 * Used to maintain a list of anaphoric chains.
 *
 * {@link ChainElement}
 */
public interface Chaining {

	/**
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <AT> The type used for attachments to chain elements that have an
	 *             anaphor part. The attachment may be used to stored e.g. an old
	 *             anaphora relation.
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 * @param <C>  The sub-type of chain element to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> emptyListOfChains() {
		return emptyList();
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> singletonListOfChains(
			final A anaphora, final AT anaphorAttachment,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		return singletonList(toChain(anaphora, anaphorAttachment, factory));
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> C toChain(
			final A anaphora, final AT anaphorAttachment,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		return toChain(anaphora.getRelatedExpressionPart(), anaphora.getAnaphorPart(), anaphorAttachment, factory);
	}

	static <TB extends B, PP, E, T, I, B, QI, EV, N, S, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> C toChain(
			final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart, final AT anaphorAttachment,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		final List<C> nextElements = new ArrayList<>();
		final C relatedExpressionElement = factory.create(null, nextElements, relatedExpressionPart, null, null);
		final C nextElement = factory.create(relatedExpressionElement, new ArrayList<>(), null, anaphorPart,
				anaphorAttachment);
		nextElements.add(nextElement);
		return relatedExpressionElement;
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> merge(
			final List<C> existingChains, final A anaphora, final AT anaphorAttachment,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		return merge(existingChains, toChain(anaphora, anaphorAttachment, factory), factory);
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> merge(
			final List<C> existingChains, final List<C> newChains,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		List<C> result = existingChains;
		for (final C newChain : newChains) {
			result = merge(result, newChain, factory);
		}
		return result;
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> merge(
			final List<C> existingChains, final C newChain,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		final Pair<Boolean, List<C>> mergedAndResult = mergeForEqualRelatedExpression(existingChains, newChain,
				factory);
		if (mergedAndResult.getLeft()) {
			return mergedAndResult.getRight();
		}
		return addChain(existingChains, newChain);
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> Pair<Boolean, List<C>> mergeForEqualRelatedExpression(
			final List<C> existingChains, final C newChain,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		final AtomicBoolean ruleApplied = new AtomicBoolean(false);
		final List<C> mappedChains = existingChains.stream()
				.map(existingChainElement -> mergeForEqualRelatedExpression(existingChainElement, newChain, ruleApplied,
						factory))
				.collect(toList());
		return new ImmutablePair<>(ruleApplied.get(), mappedChains);
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> C mergeForEqualRelatedExpression(
			final C existingRoot, final C newRoot, final AtomicBoolean ruleApplied,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		if (relatedExpressionsAreEqual(existingRoot, newRoot)) {
			final C mergedChain = mergeByRelatedExpression(existingRoot, newRoot, factory);
			ruleApplied.set(true);
			return mergedChain;
		}
		return existingRoot;
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> boolean relatedExpressionsAreEqual(
			final C existingRoot, final C newRoot) {
		return existingRoot.relatedExpression.getRelatedExpression().getRelatedExpression()
				.equals(newRoot.relatedExpression.getRelatedExpression().getRelatedExpression());
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> C mergeByRelatedExpression(
			final C existingRoot, final C newRoot,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		final List<C> next = new ArrayList<>();
		next.addAll(existingRoot.next);
		next.addAll(linkToPrevious(newRoot.next, existingRoot, factory));
		return factory.create(null, next, existingRoot.relatedExpression, null, null);
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> linkToPrevious(
			final List<C> nextElements, final C previous,
			final ChainElementFactory<N, E, T, B, TB, S, I, QI, AT, R, A, C> factory) {
		return nextElements.stream().map(next -> factory.create(previous, linkToPrevious(next.next, next, factory),
				next.relatedExpression, next.anaphor, next.anaphorAttachment)).collect(Collectors.toList());
	}

	static <N, E, T, B, TB extends B, S, I, QI, AT, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>, C extends ChainElement<N, E, T, B, TB, S, I, QI, AT, R, A, C>> List<C> addChain(
			final List<C> existingChains, final C newChain) {
		return Stream.concat(existingChains.stream(), Stream.of(newChain)).collect(toList());
	}

}
