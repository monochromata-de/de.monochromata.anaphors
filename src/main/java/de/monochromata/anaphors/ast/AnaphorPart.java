package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.reference.Referent;
import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

/**
 * Represents an anaphor at the AST level. Because instances of this type
 * contain AST nodes, they should be short-lived and must not be persisted.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface AnaphorPart<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> {

	/**
	 * @return A {@link String} representation of the anaphor initially entered by
	 *         the user or chosen algorithmically. This will be identical to the
	 *         {@link String} representation of the anaphor expression returned for
	 *         underspecified anaphora relations but will differ to explicated
	 *         anaphora relations.
	 * @see #getAnaphorExpression()
	 */
	public String getAnaphor();

	/**
	 * Get the expression that acts as anaphor.
	 *
	 * TODO: Replace by getTargetExpression()? But should the Anaphora interface be
	 * renamed, too, in this case?
	 *
	 * @return The expression that acts as anaphor.
	 */
	public E getAnaphorExpression();

	/**
	 * Get the referent of the anaphor.
	 *
	 * TODO: Should this method be in an Anaphor or TargetExpression interface?
	 * TODO: Should other referents (e.g. the one of the related expression) be
	 * available, too?
	 *
	 * @return The referent of the anaphor.
	 */
	public Referent<TB, S, I, QI> getReferent();

	/**
	 * Get the anaphor resolution strategy that was used to find the referent of the
	 * anaphor involved in this anaphora relation.
	 *
	 * @return The anaphor resolution strategy.
	 */
	public AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> getAnaphorResolutionStrategy();

	public ReferentializationStrategy<E, TB, S, I, QI> getReferentializationStrategy();

	public B getBinding();

	public TB resolveType(S scope);

}
