package de.monochromata.anaphors.ast;

import static de.monochromata.anaphors.ast.unify.DirectAnaphoraPrecedesIndirectAnaphora.preferDirectOverIndirectAnaphora;
import static de.monochromata.anaphors.ast.unify.HyponymyPrecedesFauxHyponymy.preferHynonymyOverFauxHyponymy;
import static de.monochromata.anaphors.ast.unify.TypeRecurrencePrecedesHyponymy.preferTypeRecurrenceOverHynonymy;
import static java.util.Collections.singletonList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiPredicate;

import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.reference.strategy.concept.FauxHyponymy;
import de.monochromata.anaphors.ast.reference.strategy.concept.Hyponymy;
import de.monochromata.anaphors.ast.reference.strategy.concept.NameRecurrence;
import de.monochromata.anaphors.ast.reference.strategy.concept.TypeRecurrence;
import de.monochromata.anaphors.ast.reference.strategy.feature.FeatureRecurrence;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.ClassInstanceCreationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalTempVariableIntroducingStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.LocalVariableDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.MethodInvocationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.ParameterDeclarationStrategy;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;
import de.monochromata.anaphors.ast.strategy.DA1ReStrategy;
import de.monochromata.anaphors.ast.strategy.IA1MrStrategy;
import de.monochromata.anaphors.ast.strategy.IA2FStrategy;
import de.monochromata.anaphors.ast.strategy.IA2MgStrategy;
import de.monochromata.ast.Apis;

/**
 * A generic interface to anaphora resolution. Note that invocations of the
 * interface need to be aware of a specific AST implementation to correctly
 * invoke the interface.
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public class ASTBasedAnaphorResolution<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> {

    private final Apis apis;
    private final List<RelatedExpressionStrategy<N, T, B, TB, S, QI, R>> relatedExpressionStrategies;
    private final List<AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A>> resolutionStrategies;
    private final List<ReferentializationStrategy<E, TB, S, I, QI>> referentializationStrategies;

    /**
     * Used in contract testing.
     */
    @SuppressWarnings("unused")
    protected ASTBasedAnaphorResolution() {
        apis = null;
        relatedExpressionStrategies = null;
        resolutionStrategies = null;
        referentializationStrategies = null;
    }

    public ASTBasedAnaphorResolution(final Apis apis) {
        this.apis = apis;
        this.relatedExpressionStrategies = List.of(
                new ClassInstanceCreationStrategy<>(apis.relatedExpressionsApi, apis.preferencesApi),
                new LocalVariableDeclarationStrategy<>(apis.relatedExpressionsApi, apis.preferencesApi),
                new MethodInvocationStrategy<>(apis.relatedExpressionsApi, apis.preferencesApi),
                new ParameterDeclarationStrategy<>(apis.relatedExpressionsApi, apis.preferencesApi));
        this.resolutionStrategies = resolutionStrategies = List.of(
                new DA1ReStrategy<>(apis.anaphorsApi, apis.relatedExpressionsApi, apis.anaphoraResolutionApi),
                new IA1MrStrategy<>(apis.anaphorsApi, apis.relatedExpressionsApi, apis.anaphoraResolutionApi),
                new IA2FStrategy<>(apis.anaphorsApi, apis.relatedExpressionsApi, apis.anaphoraResolutionApi),
                new IA2MgStrategy<>(apis.anaphorsApi, apis.relatedExpressionsApi, apis.anaphoraResolutionApi));
        this.referentializationStrategies = validate(
                List.of(new NameRecurrence<>(apis.anaphorsApi), new TypeRecurrence<>(apis.anaphorsApi),
                        new Hyponymy<>(apis.anaphorsApi /* TODO: Move to APIs: null, null */),
                        new FauxHyponymy<>(apis.anaphorsApi),
                        new FeatureRecurrence<>(new NameRecurrence<>(apis.anaphorsApi), apis.anaphorsApi),
                        new FeatureRecurrence<>(new TypeRecurrence<>(apis.anaphorsApi), apis.anaphorsApi),
                        new FeatureRecurrence<>(new Hyponymy<>(apis.anaphorsApi), apis.anaphorsApi)
                /* , FeatureRecurrence + FauxHyponymy */));
    }

    protected List<ReferentializationStrategy<E, TB, S, I, QI>> validate(
            final List<ReferentializationStrategy<E, TB, S, I, QI>> referentializationStrategies) {
        if (referentializationStrategies.isEmpty()) {
            throw new IllegalArgumentException("No referentialization strategies provided. At least "
                    + NameRecurrence.class.getSimpleName() + " needs to be provided because "
                    + LocalTempVariableIntroducingStrategy.class.getSimpleName()
                    + " instances use it for re-resolution.");
        } else if (!(referentializationStrategies.get(0) instanceof NameRecurrence)) {
            throw new IllegalArgumentException("No referentialization strategies provided. At least "
                    + NameRecurrence.class.getSimpleName() + " needs to be provided because "
                    + LocalTempVariableIntroducingStrategy.class.getSimpleName()
                    + " instances use it for re-resolution.");
        }
        return referentializationStrategies;
    }

    /**
     * Perform anaphora resolution on the AST implementation configured via the
     * service provider interfaces.
     *
     * @param anaphor            The anaphor that is to be (re-)resolved.
     * @param definiteExpression The expression that may function as anaphor in the
     *                           anaphora relation to be generated by this method.
     *                           If the anaphora relation is to be re-resolved, this
     *                           can be a non-trivial expression. If the anaphora
     *                           relation is to be resolved for the first time, this
     *                           is typically a simple name and might as well be
     *                           called a definite expression at this point.
     * @param scope              May be null if not required by the AST
     *                           implementation configured via the service provider
     *                           interfaces.
     * @return A list of anaphors that the given definite expression can function
     *         as. The list is empty is the anaphor could not be resolved and will
     *         contain more than one element if the anaphor is ambiguous.
     */
    public List<A> resolveAnaphor(final String anaphor, final E definiteExpression, final S scope) {

        // Get potential related expressions
        final var reCollector = apis.relatedExpressionsCollectorSupplier.get();
        final List<R> potentialRelatedExpressions = reCollector.traverse(definiteExpression, scope)
                .map(typeAndExpression -> /* TODO: */ createRelatedExpression(typeAndExpression));

        return resolveAnaphor(potentialRelatedExpressions, anaphor, definiteExpression, scope);
    }

    public List<A> resolveAnaphor(final R potentialRelatedExpression, final String anaphor, final E definiteExpression,
            final S scope) {
        return resolveAnaphor(singletonList(potentialRelatedExpression), anaphor, definiteExpression, scope);
    }

    public List<A> resolveAnaphor(final List<R> potentialRelatedExpressions, final String anaphor,
            final E definiteExpression, final S scope) {
        // Unify co-referential related expressions
        final List<R> unifiedPotentialRelatedExpressions = unifyCoReferentialRelatedExpressions(
                potentialRelatedExpressions);

        // Get potential anaphora relations
        List<A> potentialAnaphoraRelations = new ArrayList<>();
        for (final AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> resolutionStrat : this.resolutionStrategies) {
            potentialAnaphoraRelations.addAll(resolutionStrat.generatePotentialAnaphora(scope, anaphor,
                    definiteExpression, unifiedPotentialRelatedExpressions, this.referentializationStrategies));
        }

        // Unify co-referential referents
        potentialAnaphoraRelations = unifyCoReferentialReferents(potentialAnaphoraRelations);

        // Apply precedence rule
        potentialAnaphoraRelations = preferTypeRecurrenceOverHynonymy(potentialAnaphoraRelations);
        potentialAnaphoraRelations = preferHynonymyOverFauxHyponymy(potentialAnaphoraRelations);
        potentialAnaphoraRelations = preferDirectOverIndirectAnaphora(potentialAnaphoraRelations);

        return potentialAnaphoraRelations;
    }

    /**
     * @return a potential {@link RelatedExpression} representing the given node, or
     *         {@literal null}, if the node cannot function as related expression.
     */
    public R getRelatedExpression(final N node) {
        throw new UnsupportedOperationException("Creation of related expressions should be moved to d.m.e.a entirely");
        /*
         * for (final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> strategy :
         * relatedExpressionStrategies) { if
         * (strategy.isPotentialRelatedExpression(node)) { return
         * strategy.createRelatedExpression(node); } } return null;
         */
    }

    protected List<R> unifyCoReferentialRelatedExpressions(final List<R> potentialRelatedExpressions) {
        return unifyListElements(potentialRelatedExpressions, (re1, re2) -> re1.canBeUsedInsteadOf(re2));
    }

    protected List<A> unifyCoReferentialReferents(final List<A> potentialAnaphoraRelations) {
        return unifyListElements(potentialAnaphoraRelations,
                (anaphora1, anaphora2) -> anaphora1.getReferent().canBeUsedInsteadOf(anaphora2.getReferent()));
    }

    protected static <T> List<T> unifyListElements(final List<T> elements, final BiPredicate<T, T> check) {
        final List<T> retainedElements = new ArrayList<>();
        final List<T> omittedElements = new ArrayList<>();
        for (final T toCheck : elements) {
            if (!omittedElements.contains(toCheck)) {
                boolean retainedInsteadOfAnotherOne = false;
                for (final T other : elements) {
                    if (toCheck != other && check.test(toCheck, other)) {
                        if (!retainedInsteadOfAnotherOne) {
                            retainedElements.add(toCheck);
                            retainedInsteadOfAnotherOne = true;
                        }
                        System.err.println("Omitting " + other + " in favour of " + toCheck);
                        omittedElements.add(other);
                        retainedElements.remove(other);
                    }
                }
                if (!retainedInsteadOfAnotherOne) {
                    retainedElements.add(toCheck);
                }
            }
        }
        return retainedElements;
    }
}
