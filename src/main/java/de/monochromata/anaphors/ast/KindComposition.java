package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.reference.strategy.ReferentializationStrategy;
import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;
import de.monochromata.anaphors.ast.relatedexp.strategy.RelatedExpressionStrategy;
import de.monochromata.anaphors.ast.strategy.AnaphorResolutionStrategy;

public interface KindComposition {

	/**
	 * Returns a kind composed of the kinds provided by the
	 * {@link RelatedExpressionPart} and the {@link AnaphorPart}.
	 *
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 */
	static <N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> String getKind(
			final RelatedExpressionPart<N, E, T, B, TB, S, I, QI, R> relatedExpressionPart,
			final AnaphorPart<N, E, T, B, TB, S, I, QI, R, A> anaphorPart) {
		return getKind(relatedExpressionPart.getRelatedExpression().getStrategy(),
				anaphorPart.getAnaphorResolutionStrategy(), anaphorPart.getReferentializationStrategy());
	}

	/**
	 * Returns a kind composed of the kinds of the provided strategies.
	 *
	 * @param <N>  The node type in the AST
	 * @param <E>  The expression type
	 * @param <T>  The type type
	 * @param <B>  The binding type
	 * @param <TB> The type binding type
	 * @param <S>  The scope type (optional)
	 * @param <I>  The type used to represent identifiers
	 * @param <QI> The type used to represent qualified identifiers
	 * @param <R>  The sub-type of related expression to use
	 * @param <A>  The sub-type of AST-based anaphora to use
	 */
	static <N, T, B, TB extends B, S, QI, E, I, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>> String getKind(
			final RelatedExpressionStrategy<N, T, B, TB, S, QI, R> relatedExpressionStrategy,
			final AnaphorResolutionStrategy<N, E, T, B, TB, S, I, QI, R, A> anaphorResolutionStrategy,
			final ReferentializationStrategy<E, TB, S, I, QI> referentializationStrategy) {
		return relatedExpressionStrategy.getKind() + "-" + anaphorResolutionStrategy.getKind() + "-"
				+ referentializationStrategy.getKind();
	}

}
