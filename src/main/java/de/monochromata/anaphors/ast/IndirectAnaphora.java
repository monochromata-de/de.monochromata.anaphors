package de.monochromata.anaphors.ast;

import de.monochromata.anaphors.ast.relatedexp.RelatedExpression;

/**
 *
 * @param <N>  The node type in the AST
 * @param <E>  The expression type
 * @param <T>  The type type
 * @param <B>  The binding type
 * @param <TB> The type binding type
 * @param <S>  The scope type (optional)
 * @param <I>  The type used to represent identifiers
 * @param <QI> The type used to represent qualified identifiers
 * @param <R>  The sub-type of related expression to use
 * @param <A>  The sub-type of AST-based anaphora to use
 */
public interface IndirectAnaphora<N, E, T, B, TB extends B, S, I, QI, R extends RelatedExpression<N, T, B, TB, S, QI, R>, A extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A>>
		extends ASTBasedAnaphora<N, E, T, B, TB, S, I, QI, R, A> {

	/**
	 * An alias for {@link #getRelatedExpression()}.
	 */
	default R getAnchor() {
		return getRelatedExpression();
	}

	public abstract String getUnderspecifiedRelation();

}