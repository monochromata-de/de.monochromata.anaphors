package de.monochromata;

/**
 * A generic interface for strategies.
 */
public interface Strategy {

	/**
	 * Returns a short mnemonic string that uniquely identifies this strategy
	 * among the type of strategies it belongs to.
	 *
	 * @return the kind of strategy
	 */
	public String getKind();
}
