package de.monochromata;

public abstract class AbstractStrategy implements Strategy {

	@Override
	public boolean equals(Object other) {
		return this == other || (other != null && other.getClass().equals(this.getClass()));
	}

	@Override
	public int hashCode() {
		return this.getClass().getName().hashCode();
	}

	@Override
	public String toString() {
		return this.getClass().getName();
	}

}
