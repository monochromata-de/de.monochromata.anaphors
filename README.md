# A framework for anaphors in Java source code

[![pipeline status](https://gitlab.com/monochromata-de/de.monochromata.anaphors/badges/master/pipeline.svg)](https://gitlab.com/monochromata-de/de.monochromata.anaphors/commits/master)
[![Pact status](https://monochromata.pactflow.io/pacts/provider/anaphors/consumer/eclipse-anaphors/latest/badge.svg)](https://monochromata.pactflow.io/pacts/provider/anaphors/consumer/eclipse-anaphors/latest)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.monochromata.anaphors/api/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.monochromata.anaphors/api)

## Links

The project is used by
[de.monochromata.eclipse.anaphors](https://gitlab.com/monochromata-de/de.monochromata.eclipse.anaphors/).

## License

MIT
